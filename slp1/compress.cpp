#include <iostream>
#include <fstream>
#include "suffix_tree.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v2.h"
#include "slp2_v3.h"
#include "compress.h"
#include "huffman.h"

using namespace std;

namespace compress_n {

int compress_chunk_slp1(vector<int> &data, ofstream &out_file);
int compress_chunk_slp2(vector<int> &data, ofstream &out_file);
int compress_chunk_slp2_v3(vector<int> &data, ofstream &out_file);

int compress(string in_filename, string out_filename, int chunk_size, int method) {
    
    ifstream fl(in_filename);
	ofstream out_file(out_filename, ios::out | ios::binary);

    fl.seekg( 0, ios::end );  

    size_t len = fl.tellg();
    char *ret = new char[chunk_size];  
    fl.seekg(0, ios::beg);
    // cout<<len<<"\n";

    int read = 0;

    do {

     int to_read = min(chunk_size, int(len-read));
     read += to_read;

     vector<int> vec;
     fl.read(ret, to_read);

     for(int i=0; i<to_read; ++i)
    	vec.push_back(ret[i]);
    	
     if(method == 0)
     	compress_chunk_slp1(vec, out_file);
     else if(method == 1)
     	compress_chunk_slp2(vec, out_file);
     else
     	compress_chunk_slp2_v3(vec, out_file);

     cout<<"Read: "<<read<<"\n"; 
	} while(read != len);
    delete[] ret;
    fl.close();

    // cout<<vec.size()<<"\n";
    return 0;
}

int compress_chunk_slp1(vector<int> &data, ofstream &out_file) {
	slp1 slp1_compress;
	slp1_compress.compress(data);
	// slp1_compress.compress2(data);
	vector<unsigned int> compressed_grammar_uint = slp1_compress.get_grammar();
	// vector<unsigned int> compressed_grammar_uint;
	// cout<<compressed_grammar.size()<<"\n";
	// compressed_grammar_uint.resize(compressed_grammar.size());
	// for(int i=0; i<compressed_grammar.size(); ++i) {
	// 	compressed_grammar_uint[i] = compressed_grammar[i];
	// 	// compressed_grammar_uint.push_back(compressed_grammar[i]);
	// }
	// cout<<"kk\n";
	map<unsigned int, string> dict;
	vector<bool> out;
	huffman_encode(compressed_grammar_uint, dict, out);

	// cout<<"Dict:\n";
	// for(auto it = dict.begin(); it != dict.end(); ++it) {
	// 	cout<<it->first<<" "<<it->second<<"\n";
	// }
	// cout<<"\n";
	cout<<compressed_grammar_uint.size()<<" "<<out.size()/8<<"\n";
}

int compress_chunk_slp2(vector<int> &data, ofstream &out_file) {
	slp2 slp2_compress;
	slp2_compress.compress(data);
	// slp1_compress.compress2(data);
	vector<unsigned int> compressed_grammar_uint = slp2_compress.get_grammar();
	// slp2_compress.get_compressed_grammar();
	// vector<unsigned int> compressed_grammar_uint;
	// cout<<compressed_grammar.size()<<"\n";
	// compressed_grammar_uint.resize(compressed_grammar.size());
	// for(int i=0; i<compressed_grammar.size(); ++i) {
	// 	compressed_grammar_uint[i] = compressed_grammar[i];
	// 	// compressed_grammar_uint.push_back(compressed_grammar[i]);
	// }
	// cout<<"kk\n";
	map<unsigned int, string> dict;
	vector<bool> out;
	huffman_encode(compressed_grammar_uint, dict, out);

	// cout<<"Dict:\n";
	// for(auto it = dict.begin(); it != dict.end(); ++it) {
	// 	cout<<it->first<<" "<<it->second<<"\n";
	// }
	// cout<<"\n";
	cout<<compressed_grammar_uint.size()<<" "<<out.size()/8<<"\n";
	exit(0);
}

int compress_chunk_slp2_v3(vector<int> &data, ofstream &out_file) {
	slpv3::slp2_v3 slp_compress(data, 2.0f);
	// slp_compress.compress(data);
}

}