#include "slp1.h"
#include "suffix_tree.h"
#include <map>
#include <iostream>

void slp1::split(int i, vector<int> &start, vector<int> &end) {
	if (start[i] != -1 && end[i]) {
		start[i] = -1;
		end[i] = false;
	} else
	if (start[i] != -1) {
		if(end[i+1]) {
			start[i] = -1;
			end[i+1] = false;
		} else {
			start[i+1] = start[i] + 1;
			start[i] = -1;
		}
	} else
	if (end[i]) {
		if (start[i-1] != -1) {
			end[i] = false;
			start[i-1] = -1;
		} else {
			end[i-1] = true;
			end[i] = false;
		}
	}
}

void slp1::preproc(int len, vector<int> &start, vector<int> &end) {
	start[0] = -1;
	end[0] = false;
	for(int i=1; i<len; ++i) {
		if (start[i] == i-1) {
			if(i+1<len)
				if (start[i+1] == -1 && end[i] == false)
					start[i+1] = i-1;

			split(i, start, end);
		}

		if(start[i] != -1 && end[i]) //split one letter factors
			split(i, start, end);
	}
}

void slp1::pairing(int len, vector<int> &start, vector<int> &end, vector<int> &pair) {

	for(int i=0; i<pair.size(); ++i)
		pair[i] = -1;

	start[0] = -1;
	end[0] = false;

	for(int i=1; i < len; ++i) {
		if(start[i] != -1) {
			// cout<<"kk\n";
			// cout<<i<<" "<<start[i]<<"\n";
			// cout<<i<<" "<<start[i]<<" "<<pair[start[i]]<<"\n";
			if( (pair[start[i]] == -1 && pair[i-1] == -1) || pair[start[i]] == 1) {
				// cout<<i<<" "<<"spl\n";
				split(i, start, end);
			} else {
				// cout<<i<<" "<<"nspl\n";
				// cout<<i<<"\n";
				int j = start[i];
				do {
					pair[i] = pair[j];
					i++; j++;
				} while(!end[i-1]);
				i--;
				if(pair[i] == 0) {
					split(i, start, end);
					pair[i] = -1;
				}
			}
		} else
		if(start[i] == -1)
		{
			// cout<<i<<" _\n";
			if (pair[i-1] == -1) {
				split(i-1, start, end);
				pair[i-1] = 0;
				pair[i] = 1;
			} else {
				pair[i] = -1;
			}
			// cout<<"aa\n";
		}
		// if(i == 3) break;
	}
}

void slp1::pairing2(int len, vector<int> &txt, vector<int> &start, vector<int> &end, vector<int> &pair) {

	map<std::pair<int, int>, int> cnt_pairs;

	for(int i=0; i<pair.size(); ++i)
		pair[i] = -1;

	start[0] = -1;
	end[0] = false;

	for(int i=1; i < len; ++i) {
		if(start[i] != -1) {
			// cout<<"kk\n";
			// cout<<i<<" "<<start[i]<<"\n";
			// cout<<i<<" "<<start[i]<<" "<<pair[start[i]]<<"\n";
			if( (pair[start[i]] == -1 && pair[i-1] == -1) || pair[start[i]] == 1) {
				// cout<<i<<" "<<"spl\n";
				split(i, start, end);
			} else {
				// cout<<i<<" "<<"nspl\n";
				// cout<<i<<"\n";
				int j = start[i];
				do {
					pair[i] = pair[j];
					if(pair[i] == 1) {
						cnt_pairs[make_pair(txt[i-1], txt[i])]++;
					}
					i++; j++;
				} while(!end[i-1]);
				i--;
				if(pair[i] == 0) {
					split(i, start, end);
					pair[i] = -1;
				}
			}
		} else
		if(start[i] == -1)
		{
			// cout<<i<<" _\n";
			if (pair[i-1] == -1) {
				split(i-1, start, end);
				pair[i-1] = 0;
				pair[i] = 1;
				cnt_pairs[make_pair(txt[i-1], txt[i])]++;
			} else {
				pair[i] = -1;
			}
			// cout<<"aa\n";
		}
		// if(i == 3) break;
	}

	for(int i=1; i < len; ++i) {
		if(pair[i] == 1 && cnt_pairs[make_pair(txt[i-1], txt[i])] < 2) {
			pair[i-1] = -1;
			pair[i] = -1;
			// cout<<txt[i-1]<<" "<<txt[i]<<"\n";
		}
	}
}

void slp1::print_state(int len, vector<int> &txt, vector<int> &start, vector<int> &end) {
	bool fac = 0;
	for(int i=0; i<len; ++i) {
		if(start[i] != -1) {
			cout<<"|_";
			fac = 1;
		}

		if(!fac)
			cout<<"|"<<txt[i]<<"|";
		else
			cout<<txt[i];
		if(end[i]) {
			fac = 0;
			cout<<"_|";
		}
		cout<<" ";
	}
	cout<<"\n";
}

void slp1::replacement(int &len, vector<int> &txt, vector<int> &start, vector<int> &end, vector<int> &pair) {

	int old_len = len;
	vector<int> newpos;
	// cout << len << "\n";
	newpos.resize(len);

	for(int i=0; i<len; ++i) newpos[i] = i;

	int i = 0, ip = 0;
	while(i < len) {
		// cout << i<<"\n";
		if(start[i] == -1) {
			if(pair[i] == -1) {
				txt[ip] = txt[i];
				start[ip] = start[i];
				end[ip] = end[i]; // !!! 
				// cout<<start[ip]<<" "<<end[i]<<"\n";
				newpos[i] = ip;
				i++; ip++;
			} else {
			// cout<<"P: "<< i << "\n";
				newpos[i] = ip;
				newpos[i+1] = ip;

				std::pair<int, int> pp = make_pair(txt[i], txt[i+1]);

				if(nonterms.find(pp) != nonterms.end() )
					txt[ip] = nonterms[pp];
				else {
					txt[ip] = next_nonterm++;
					nonterms[pp] = txt[ip];
				}
				// cout<<"Adding nonterm: " <<pp.first<<" "<<pp.second<<" "<<nonterms[pp]<<" "<<ip<<"\n";
				nonterms_count[pp]++;

				end[ip] = end[i+1];
				start[ip] = start[i];
				i+=2; ip++;
			}
		} else {
			// cout<<ip<<" "<<i<<" "<<pair[i]<<"\n";
			int o_ip = ip;
			int newpos_start_i = newpos[start[i]];
			// cout<<start[i]<<" ss\n";
			start[ip] = newpos[start[i]];
			int j = newpos[start[i]];
			// cout<<ip<<" "<<i<<" "<<start[i]<<" "<<j<<" <<< \n";
			do{
				txt[ip] = txt[j];
				end[ip] = false;
				start[ip] = -1;
				newpos[i] = ip;
				if(pair[i] == 0) {
					newpos[i+1] = ip;
					i++;
				}
				i++;
				ip++; j++;
				// cout<<end[i-1]<<"\n";
			} while(!end[i-1]);
			start[o_ip] = newpos_start_i;
			end[i-1] = false;
			end[ip-1] = true;
			// cout<<ip<<" "<<i<<" "<<end[ip-1]<<" "<<end[ip-2]<<"/\n";
		}
	}
	len = ip;
}

void slp1::compress(vector<int> txt, int compress_mode) {
	suffix_tree st;
	vector<unsigned int> factors;
	vector<unsigned int> factor_starts;
	st.lz77(txt, factors, factor_starts);
	// cout<<"lz77 done\n";
	vector<int> start, end, pair;
	start.resize(txt.size(), 0);
	end.resize(txt.size(), 0);
	pair.resize(txt.size());

	// cout<<str[0]<<"|";
	start[0] = 0;
	end[0] = 0;
	// cout<<start[0]<<",";
	for(int i=1; i<factors.size(); ++i) {
		for(int j=factors[i-1]; j<factors[i]; ++j) {
			// start[j] = factors[i-1];
			// end[j] = factors[i]-1;
			start[j] = -1;
			end[j] = 0;
			// cout<<str[j];
		}
		start[factors[i-1]] = factor_starts[i];
		end[factors[i]-1] = 1;
		// cout<<start[factors[i-1]]<<",";
		// cout<<"|";
	}
	// cout<<"__\n";

	for(int i=0; i<txt.size(); ++i) {
		next_nonterm = max(next_nonterm, txt[i] + 1);		
		// cout<<start[i]<<"|"<<end[i]<<" ";
	}
	// cout<<"\n";
	len = txt.size();
	preproc(len, start, end);
	// return;

	// cout<<"Length: "<<len<<"\n";
	// cout<<"State: \n"; print_state(len, txt, start, end);

	// split(5, start, end);
	// cout<<"State: \n"; print_state(len, txt, start, end);

	// return;
	// int compress_mode = 0;
	if(compress_mode == 0)
	while(len > 1) {
		// cout<<"State: \n";print_state(len, txt, start, end);
		
		preproc(len, start, end);

		// for(int i=0; i<txt.size(); ++i) {	
		// cout<<start[i]<<"|"<<end[i]<<" ";
		// }
		// cout<<"\n";
		// cout<<"State: \n"; print_state(len, txt, start, end);
		// cout<<"\\state\n";
		pairing(len, start, end, pair);
		// cout<<len<<endl;
		// cout<<"StatePair: \n"; print_state(len, txt, start, end);
		// cout<<"\\statePar\n";
		// cout<<"preporecd\n";
		// cout<<"State: \n";print_state(len, txt, start, end);
		// for(int i=0; i<len; ++i) {
		// 	cout<<pair[i]<<" ";
		// }
		// cout<<"\n";
		// pair[len-1] = pair[len-2] = -1;
		replacement(len, txt, start, end, pair);
		// cout<<"State2: \n"; print_state(len, txt, start, end);
		// return;
		// cout<<"nLen: "<<len<<"\n";
		// cout<<len<<"\n";
		// cout<<"State: \n"; print_state(len, txt, start, end);
		// break;
	}

	if(compress_mode == 1) {
		int old_len = -1;
		while(len != old_len) {
		
			old_len = len;
			// cout<<len<<"\n";
			preproc(len, start, end);
		// cout<<"State: \n"; print_state(len, txt, start, end);
		// cout<<"\\state\n";
			pairing2(len, txt, start, end, pair);
		// cout<<"StatePair: \n"; print_state(len, txt, start, end);
		// cout<<"\\statePar\n";
		// cout<<"preporecd\n";
		// cout<<"State: \n"; print_state(len, txt, start, end);
		// for(int i=0; i<len; ++i) {
		// 	cout<<pair[i]<<" ";
		// }
		// cout<<"\n";
		// pair[len-1] = pair[len-2] = -1;
			replacement(len, txt, start, end, pair);
		// cout<<"State2: \n"; print_state(len, txt, start, end);
		// return;
		// cout<<"nLen: "<<len<<"\n";
		// cout<<len<<"\n";
		// cout<<"State: \n"; print_state(len, txt, start, end);
		// break;
		}

		// cout<<"Approx: "<<factors.size()<<" "<<nonterms.size()*2<<"+"<<len<<"\n";
		// return;
	}
	
	lz77_size = factors.size();
	result_txt = txt;
	// cout<<"\n";

	// cout<<"Approx: "<<factors.size()<<" "<<nonterms.size()*2<<"\n";
	// return;

	// vector<unsigned int> grammar;

	// for(auto it = nonterms.begin(); it != nonterms.end(); ++it) {
	// 	grammar.push_back(it->first.first);
	// 	grammar.push_back(it->first.second);
	// 	grammar.push_back(it->second);	
	// }
	// cout<<grammar.size()<<"\n"
	// vector<int> temp_text = iteration(txt, start, end);
}

// void slp1::

// void slp1::remove_single(vector<int> &res_txt){

// }
//recomputation of lz77 in each step
void slp1::compress2(vector<int> txt) {
	suffix_tree st;
	vector<unsigned int> factors;
	vector<unsigned int> factor_starts;
	st.lz77(txt, factors, factor_starts);
	// cout<<"lz77 done\n";
	vector<int> start, end, pair;
	start.resize(txt.size());
	end.resize(txt.size());
	pair.resize(txt.size());

	// cout<<str[0]<<"|";

	start[0] = 0;
	end[0] = 0;
	// cout<<start[0]<<",";
	for(int i=1; i<factors.size(); ++i) {
		for(int j=factors[i-1]; j<factors[i]; ++j) {
			// start[j] = factors[i-1];
			// end[j] = factors[i]-1;
			start[j] = -1;
			end[j] = 0;
			// cout<<str[j];
		}
		start[factors[i-1]] = factor_starts[i];
		end[factors[i]-1] = 1;
		// cout<<start[factors[i-1]]<<",";
		// cout<<"|";
	}
	// cout<<"__\n";

	for(int i=0; i<txt.size(); ++i) {
		next_nonterm = max(next_nonterm, txt[i] + 1);		
	}

	int len = txt.size();
	preproc(len, start, end);

	// cout<<"Length: "<<len<<"\n";
	// cout<<"State: \n"; print_state(len, txt, start, end);

	// split(5, start, end);
	// cout<<"State: \n"; print_state(len, txt, start, end);

	// return;
	while(len > 1) {
		preproc(len, start, end);
		pairing(len, start, end, pair);
		replacement(len, txt, start, end, pair);

		// cout<<"State: \n"; print_state(len, txt, start, end);

		vector<int> txt2;
		for(int i=0; i<len; ++i) {
			txt2.push_back(txt[i]);
		}

		st.lz77(txt2, factors, factor_starts);
		start[0] = 0;
		end[0] = 0;
		// cout<<start[0]<<",";
		for(int i=1; i<factors.size(); ++i) {
			for(int j=factors[i-1]; j<factors[i]; ++j) {
				// start[j] = factors[i-1];
				// end[j] = factors[i]-1;
				start[j] = -1;
				end[j] = 0;
				// cout<<str[j];
			}
			start[factors[i-1]] = factor_starts[i];
			end[factors[i]-1] = 1;
			// cout<<start[factors[i-1]]<<",";
			// cout<<"|";
		}

	}
	// cout<<"\n";

	cout<<"Approx2	: "<<factors.size()<<" "<<nonterms.size()*2<<"\n";
	return;

	// vector<unsigned int> grammar;

	// for(auto it = nonterms.begin(); it != nonterms.end(); ++it) {
	// 	grammar.push_back(it->first.first);
	// 	grammar.push_back(it->first.second);
	// 	grammar.push_back(it->second);	
	// 	r_nonterms[it->second] = it->first;
	// }
	// cout<<grammar.size()<<"\n"
	// vector<int> temp_text = iteration(txt, start, end);
}


vector<int> slp1::decompress() {
	int mx = 0;
	auto it = nonterms.begin();
	map<int, pair<int, int>> r_grammar;
	for(;it != nonterms.end(); ++it){
		mx = max(mx, it->second);
		r_grammar[it->second] = it->first;
		// cout<<it->second<<" "<<it->first.first<<" "<<it->first.second<<" |\n";
	}

	vector<int> res;
	// int current = first;
	// while(current != -1) {
	// 	flatten(text[current].letter, r_grammar, res);
	// 	current = text[current].next;
	// }
	// cout<<len<<" :: \n";
	for(int i=0; i<len; ++i) {
		// cout<<result_txt[i]<<" [[]]\n";
		// res.push_back(result_txt[i]);
		flatten(result_txt[i], r_grammar, res);
	}
	return res;
}

void slp1::flatten(int nonterm, map<int, pair<int, int>>  &r_grammar, vector<int> &res){

	if(r_grammar.find(nonterm) == r_grammar.end()){
		// res.push_back(nonterm);
		// vector<int> res;
		// res.push_back(nonterm);
		// cout<<"leaf: "<<nonterm<<"\n";
		// return res;
		// cout<<"kk\n";

		res.push_back(nonterm);
		// leaf_cnt++;
		// if(leaf_cnt % 1000 == 0)
			// cout<<"leaf: "<<leaf_cnt<<"\n";
	} else {
		pair<int, int> pp = r_grammar[nonterm];
		// vector<int> left = flatten(pp.first, r_grammar);
		// vector<int> right = flatten(pp.second, r_grammar);

		// left.insert(left.end(), right.begin(), right.end());
		// return left;
		flatten(pp.first, r_grammar, res);
		flatten(pp.second, r_grammar, res);

	}

}

vector<unsigned int> slp1::get_grammar() {
	cout<<"Nonterms: "<<nonterms.size()<<"\n";
	vector<unsigned int> grammar;
	for(auto it = nonterms.begin(); it != nonterms.end(); ++it) {
		grammar.push_back(it->first.first);
		grammar.push_back(it->first.second);
		grammar.push_back(it->second);	
	}

	vector<int> freq;
	for(auto it = nonterms_count.begin(); it != nonterms_count.end(); ++it) {
		freq.push_back(it->second);
	}

	sort(freq.begin(), freq.end(), std::greater<int>());
	cout<<"Most common: "<<freq[0]<<" P5: "<<freq[freq.size()*5/100]<<"\n";

	return grammar;
}