
namespace compress_benchmark{

int benchmark(string filname, string out_filename, string debug_filename,
	int chunk_size, float coeff_min, float step, int num_steps, int method, int crossing_type, bool do_repair, int limit);


}