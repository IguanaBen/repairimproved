#include "huffman.h"
#include <iostream>

void huffman_traverse(huffman_node* node, map<unsigned int, string> &res, string &path)
{
	if(node->left == NULL)
		res[node->letter] = path;
	else
	{
		path.push_back('0');
		huffman_traverse(node->left, res, path);
		path.pop_back();
		
		path.push_back('1');
		huffman_traverse(node->right, res, path);
		path.pop_back();
	}
}

void del_huffman_tree(huffman_node* node)
{	
	if(node->left != NULL)
	{
		del_huffman_tree(node->left);
		del_huffman_tree(node->right);
	}
	delete node;
}

vector<int> huffman_decode_binary(map<unsigned int, unsigned int> &frequencies, vector<bool> &compressed_text){
	priority_queue< pair<pair<unsigned int, int>, huffman_node*>,
				    vector<pair<pair<unsigned int, int>, huffman_node*>>,
					std::greater< pair<pair<unsigned int, int>, huffman_node*>>> pq;

	//DUZE DRY
	int id = 0;
	map<unsigned int, unsigned int>::iterator it;
	for(it = frequencies.begin(); it != frequencies.end(); ++it)
	{
		// cout<<it->first<<" : "<<it->second<<"\n";
		if(it->second){
			huffman_node *node = new huffman_node();
			node->letter = it->first;
			pq.push( make_pair(make_pair(it->second, id++), node) );
		}
	}

	while(pq.size() > 1)
	{
		auto n1 = pq.top(); pq.pop();
		auto n2 = pq.top(); pq.pop();
		huffman_node *new_node = new huffman_node();

		new_node->left = n1.second;
		new_node->right = n2.second;

		pq.push(make_pair(make_pair(n1.first.first + n2.first.first, id++), new_node));
	}
	map<unsigned int, string> dict;
	map<string, unsigned int> r_dict;

	string path = "";
	huffman_traverse(pq.top().second, dict, path);

	for(auto it = dict.begin(); it != dict.end(); ++it) {
		// cout<<it->first<<" "<<it->second<<" D2\n";
		r_dict[it->second] = it->first;
	}

	vector<int> result;
	int pos = 0;
	//no ale to jest slabe...
	while(pos < compressed_text.size()){

		// int current_match = 1;
		string ss;
		if(compressed_text[pos])
			ss = '1';
		else
			ss = '0';
		while(r_dict.find(ss) == r_dict.end()) {
			// current_match++;
			pos++;
			ss += char(compressed_text[pos] + '0');
		}

		result.push_back(r_dict[ss]);
		pos++;
		// pos += current_match;
	}

	del_huffman_tree(pq.top().second);
	return result;

}

void huffman_encode( vector<unsigned int> &text, map<unsigned int, string> &dict, vector<bool> &out)
{
	// vector<unsigned int> frequencies(charset_size);
	map<unsigned int, unsigned int> frequencies;
	for(int i=0; i<text.size(); ++i) {
		// cout<<text[i]<<" TT\n";
		frequencies[text[i]]++;
	}
	
	priority_queue< pair<pair<unsigned int, int>, huffman_node*>,
				    vector<pair<pair<unsigned int, int>, huffman_node*>>,
					std::greater< pair<pair<unsigned int, int>, huffman_node*>>> pq;

	int id = 0;
	map<unsigned int, unsigned int>::iterator it;
	for(it = frequencies.begin(); it != frequencies.end(); ++it)
	{
		// cout<<it->first<<" : "<<it->second<<"\n";
		huffman_node *node = new huffman_node();
		node->letter = it->first;
		pq.push( make_pair(make_pair(it->second, id++), node) );
	}

	while(pq.size() > 1)
	{
		auto n1 = pq.top(); pq.pop();
		auto n2 = pq.top(); pq.pop();
		huffman_node *new_node = new huffman_node();

		new_node->left = n1.second;
		new_node->right = n2.second;

		pq.push(make_pair(make_pair(n1.first.first + n2.first.first, id++), new_node));
	}

	if(frequencies.size() == 1)
		dict[text[0]] = "1";
	else
	{
		string path = "";
		huffman_traverse(pq.top().second, dict, path);
	}

	for(int i=0; i<text.size(); ++i)
	{
		string str = dict[text[i]];
		for(int j=0; j<str.length(); ++j)
				out.push_back(str[j]-'0');
	}

	// for(auto it = dict.begin(); it != dict.end(); ++it) {
	// 	// r_dict[it->second] = it->first;
	// 	// cout<<it->first<<" "<<it->second<<" D1\n";
	// }

	del_huffman_tree(pq.top().second);
}

void huffman_insert_key(huffman_node* node, string &key, int index, unsigned int value)
{
	if(index == key.length())
		node->letter = value;
	else
	{
		if(key[index] == '0')
		{
			if(node->left == NULL)
				node->left = new huffman_node();
			huffman_insert_key(node->left, key, index + 1, value);
		}
		else
		{
			if(node->right == NULL)
				node->right = new huffman_node();
			huffman_insert_key(node->right, key, index + 1, value);
		}
	}
}