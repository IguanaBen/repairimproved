#include "slp2.h"
#include "suffix_tree.h"
#include <map>
#include <iostream>
#include <algorithm>
#include <set>
#include <queue>

using std::pair;

// float slp2::weigth(int pair_cnt,
// 			 int pair_cross) {

// 	float pcnt = pair_cnt;
// 	float pcrs = pair_cross;
// 	// cout<<pcnt<<" x "<<pcrs<<"\n";
// 	// return pcnt/pcrs;
// 	return 2.0f*pcnt - pcrs;
// 	// return pcnt;	

// }


// void slp2::print_state(int len, vector<int> txt, vector<int> start, vector<int> end) {
// 	bool fac = 0;
// 	for(int i=0; i<len; ++i) {
// 		if(start[i] != -1) {
// 			cout<<"|_";
// 			fac = 1;
// 		}

// 		if(!fac)
// 			cout<<"|"<<txt[i]<<"|";
// 		else
// 			cout<<txt[i];
// 		if(end[i]) {
// 			fac = 0;
// 			cout<<"_|";
// 		}
// 		cout<<" ";
// 	}
// 	cout<<"\n";
// }


// bool slp2::is_single(int i, vector<int> &start, vector<int> &end) {
// 	return start[i] != -1 && end[i];
// }

// void slp2::preproc_pairs(map<pair<int, int>, int> &count_pair,
// 						 map<pair<int, int>,  int> &cross_pair,
// 						 vector<int> &start,
// 						 vector<int> &end,
// 						 int len,
// 						 vector<int> &txt) {
// 	for(int i=0; i<len-1; ++i) {
// 		count_pair[make_pair(txt[i], txt[i+1])]++;
// 		cross_pair[make_pair(txt[i], txt[i+1])] = 0;

// 		// cout<<txt[i]<<" "<<txt[i+1]<<"\n";
// 		if(end[i] && start[i+1] != -1)
// 			if(!(is_single(i, start, end) && is_single(i+1, start, end)))
// 				cross_pair[make_pair(txt[i], txt[i+1])]++;
// 	}
// }

// pair<int, int> slp2::best_pair(map<pair<int, int>, int> &count_pair,
// 				  	 map<pair<int, int>,  int> &cross_pair) {
// 	auto it = count_pair.begin();
// 	auto it2 = cross_pair.begin();
// 	float mx_weight = -1.0;
// 	pair<int, int> b_pair = make_pair(0,0);
// 	for(;it != count_pair.end(); ++it) {
// 		float w = weigth(it->second, it2->second);
// 		if(w > mx_weight) {
// 			mx_weight = w;
// 			b_pair = it->first;
// 		}
// 		++it2;
// 	}

// 	return b_pair;

// 	// cout<<mx_weight<<" "<<b_pair.first<<" "<<b_pair.second<<"\n";
// }

// void slp2::split(int i, vector<int> &start, vector<int> &end) {
// 	// start[i+1] = start[i];
// 	if(start[i] != -1 && end[i])
// 		return;
// 	if(start[i] != -1){
// 		start[i+1] = start[i];
// 		end[i] = true;
// 	} else
// 	if(end[i] == true) {
// 		end[i-1] = true;
// 		start[i] = i;
// 	}
// }

// void slp2::replacement(pair<int, int> best_pair, vector<int> &txt, int &len, vector<int> &start, vector<int> &end) {
// 	int new_len = 0;
// 	vector<int> new_text;
// 	vector<int> new_start;
// 	vector<int> new_end;
// 	int i = 0;
// 	while(i<len-1) {
// 		if (best_pair.first == txt[i] && best_pair.second == txt[i+1]) {
// 			int letter;
// 			if(nonterms.find(make_pair(txt[i], txt[i+1])) != nonterms.end()) {
// 				letter = nonterms[make_pair(txt[i], txt[i+1])];
// 			} else {
// 				letter = next_nonterm++;
// 				nonterms[make_pair(txt[i], txt[i+1])] = letter;
// 			}
// 			new_text.push_back(letter);
// 			if(end[i] && start[i+1] != -1) {
// 				split(i, start, end);
// 				split(i+1, start, end);
// 				new_start.push_back(new_start.size());
// 				new_end.push_back(true);
// 			} else {
// 				new_start.push_back(start[i]);
// 				new_end.push_back(end[i+1]);
// 			}
// 			i+=2;
// 			// cout<<"p\n;";
// 		} else {
// 			new_text.push_back(txt[i]);
// 			new_start.push_back(start[i]);
// 			new_end.push_back(end[i]);
// 			i++;
// 		}
// 	}

// 	if(i == len-1) {
// 		// cout<<"a\n";
// 		new_text.push_back(txt[i]);
// 		new_start.push_back(start[i]);
// 		new_end.push_back(end[i]);
// 	}

// 	txt = new_text;
// 	start = new_start;
// 	end = new_end;
// 	len = txt.size();
// }

int slp2::make_node(vector<txt_letter_v2> &txt_list, queue<int> &free_nodes) {
	int i = free_nodes.front();
	free_nodes.pop_front();
	return i;
}

void slp2::make_list(vector<int> &txt,
					   vector<int> &start,
					   vector<int> &end, 
					   vector<txt_letter_v2> &txt_list,
					   queue<int> &free_nodes) {
	list_begin = 0;
	// list_end = new txt_letter;

	txt_list[list_begin].letter = -1;
	txt_list[list_begin].end = 1;
	txt_list[list_begin].prev = -1;
	// list_begin->end = 1;
	// list_begin->prev = NULL;
	// list_end->letter = -1;


	int prev_node = list_begin;
	for(int i=0; i<txt.size(); ++i) {
		int node = make_node(txt_list, free_nodes);
		txt_list[node].letter = txt[i];
		txt_list[node].start = start[i];
		txt_list[node].end = end[i];
		txt_list[node].prev = prev_node;
		txt_list[prev_node].next = node;
		prev_node = node;
	}
	// delete list_end;
	// delete list_begin;
	// list_begin = list_begin->next;
	// delete list_begin->prev;/
	// list_begin->prev = NULL;
	list_end = prev_node;
	txt_list[list_end].next = -1;
	// prev_node->next = list_end;
	// list_end->prev = prev_node;
}

bool slp2_v2::is_single_list(int node, vector<txt_letter_v2> &txt_list){
	return txt_list[node].start != -1 && txt_list[node].end;
}

bool slp2_v2::is_cross_list(txt_letter* node1, txt_letter* node2, vector<txt_letter_v2> &txt_list) {
	if()
	return !(is_single_list(node1) && is_single_list(node2));
}

void slp2_v2::preproc_pairs_list(
	int begin,
	vector<txt_letter_v2> &txt_list,
	map<std::pair<int, int>, int> &count_pair,
	map<std::pair<int, int>, int> &cross_pair,
	map<std::pair<int, int>, vector<pair<txt_letter*, txt_letter*>>> &pair_positions) {

	txt_letter* node = begin;
	while(node->next != NULL) {
		count_pair[make_pair(node->letter, node->next->letter)]++;

		pair_positions[make_pair(node->letter,  node->next->letter)].push_back(make_pair(node, node->next));

		if(node->end && node->next->start != -1)
			if(!(is_single_list(node) && is_single_list(node->next))) {
				cross_pair[make_pair(node->letter, node->next->letter)]++;
			}
		node = node->next;
	}
}

// void slp2::print_state_list(txt_letter* node) {
// 	while(node != NULL) {
// 		if(node->start != -1) cout<<"|_";
// 		cout<<node->letter;
// 		if(node->end)cout<<"_|";
// 		cout<<" ";
// 		node = node->next;
// 	}
// 	cout<<"\n";
// }

// void slp2::replacement_list(
// 	pair<int, int> best_pair,
// 	set<std::pair<int, int>, cmp_pairs> &heap,
// 	set<txt_letter*> &removed_nodes,
// 	map<std::pair<int, int>, int> &count_pair,
// 	map<std::pair<int, int>, int> &cross_pair,
// 	map<std::pair<int, int>, vector<pair<txt_letter*, txt_letter*>>> &pair_positions) {

// 	vector<pair<txt_letter*, txt_letter*>> v_pos = pair_positions[best_pair];
// 	// heap.erase(heap.find(best_pair));
// 	// cout<<"Len: "<<v_pos.size()<<"\n";


// 	if(count_pair[best_pair] == 2) {

// 		pair<txt_letter*, txt_letter*> p1;
// 		pair<txt_letter*, txt_letter*> p2;
// 		int cnt;
// 		for(int i=0; i<v_pos.size(); ++i) {
// 			if((removed_nodes.find(v_pos[i].first) == removed_nodes.end()) &&
// 		  	  (removed_nodes.find(v_pos[i].second) == removed_nodes.end())) {
// 				if(cnt == 0)
// 					p1 = v_pos[i];
// 				if(cnt == 1)
// 					p1 = v_pos[i];
// 				cnt++;
// 			}
// 		}

// 		if(cnt != 2) {
// 			cout<<"Cos jest nie tak...\n";
// 		}

// 		if(p1.second == p2.first || p2.second == p1.first) {
// 			heap.erase(heap.find(best_pair));
// 			count_pair.erase(count_pair.find(best_pair));
// 			cross_pair.erase(cross_pair.find(best_pair));
// 			pair_positions.erase(pair_positions.find(best_pair));

// 			return;
// 		}
// 	}

// 	int letter;
// 	if(nonterms.find(best_pair) != nonterms.end()) {
// 		letter = nonterms[best_pair];
// 	} else {
// 		letter = next_nonterm++;
// 		nonterms[best_pair] = letter;
// 	}
// 	for(int i=0; i<v_pos.size(); ++i) {
// 		// cout<<"State bef: ";
// 		// print_state_list(list_begin);
// 		// cout<<i<<" "<<"aa\n";
// 		if((removed_nodes.find(v_pos[i].first) == removed_nodes.end()) &&
// 		   (removed_nodes.find(v_pos[i].second) == removed_nodes.end())) {
// 		   	// cout<<v_pos[i].first<<" "<<v_pos[i].second<<"\n";
// 		   	// cout<<v_pos[i].first->letter<<" "<<v_pos[i].second->letter<<"\n";
// 		   	// cout<<v_pos[i].first->prev->next->letter<<" "<<v_pos[i].second->letter<<"\n";


// 		   	//removed node
// 		   	removed_nodes.insert(v_pos[i].first);
// 		   	removed_nodes.insert(v_pos[i].second);

// 		   	txt_letter* new_node = new txt_letter;

// 		   	new_node->letter = letter;
// 		   	new_node->start = v_pos[i].first->start;
// 		   	new_node->end = v_pos[i].second->end;

// 		   	if(v_pos[i].first->prev != list_begin) {
// 		   		pair<int, int> pp = make_pair(v_pos[i].first->prev->letter, best_pair.first);
// 			   	// cout<<"P: "<<pp.first<<" "<<pp.second<<"\n";
// 			   	if(heap.find(pp) != heap.end())
// 			   		heap.erase(heap.find(pp));
// 			   	// else
// 		   			// cout<<"not found\n";

// 			   	count_pair[pp]--;

// 				if(v_pos[i].first->prev->end && v_pos[i].first->start != -1)
// 					if(!(is_single_list(v_pos[i].first->prev) && is_single_list(v_pos[i].first)))
// 			   			cross_pair[pp]--;

// 			   	if(count_pair[pp] > 0)
// 			   		heap.insert(pp);
// 			   	else {
// 			   		count_pair.erase(count_pair.find(pp));
// 			   		cross_pair.erase(cross_pair.find(pp));
// 			   	}

// 			   	// cout<<"mmamama\n";
// 		   		pair<int, int> pp2 = make_pair(v_pos[i].first->prev->letter, letter);
// 			   	// cout<<"P2: "<<pp2.first<<" "<<pp2.second<<"\n";
// 			   	if(heap.find(pp2) != heap.end())
// 		   			heap.erase(heap.find(pp2));

// 			   	count_pair[pp2]++;
// 				if(v_pos[i].first->prev->end && new_node->start != -1)
// 					if(!(is_single_list(v_pos[i].first->prev) && is_single_list(new_node)))
// 						cross_pair[pp2]++;
// 				// cout<<"Inserting\n";
// 				heap.insert(pp2);
// 				pair_positions[pp2].push_back(make_pair(v_pos[i].first->prev, new_node));
// 			   	// cout<<"XXXmmamama\n";

// 		   	}


// 		   	if(v_pos[i].second->next != NULL) {
// 			   	pair<int, int> pp = make_pair(best_pair.second, v_pos[i].second->next->letter);
// 		   			// cout<<"kk  "<<pp.first<<" "<<pp.second<<" : "<<count_pair[pp]<<" "<<cross_pair[pp]<<"\n";
// 			   	// cout<<"P3: "<<pp.first<<" "<<pp.second<<"\n";
			   	
// 			   	if(heap.find(pp) != heap.end())
// 			   		heap.erase(heap.find(pp));
// 			   	// else
// 		   			// cout<<"not found\n";
// 			   	count_pair[pp]--;

// 				if(v_pos[i].second->end && v_pos[i].second->next->start != -1)
// 					if(!(is_single_list(v_pos[i].second) && is_single_list(v_pos[i].second->next)))
// 					{
// 			   			cross_pair[pp]--;
// 			   			// if(cross_pair[pp] == -1) {
// 			   			// 	cout<<"PP: "<<letter<<" "<<pp.first<<" "<<pp.second<<" "<<count_pair[pp]<<" "<<cross_pair[pp]<<"\n";
// 			   			// }
// 					}
// 			   	if(count_pair[pp] > 0)
// 			   		heap.insert(pp);
// 			   	else {
// 			   		count_pair.erase(count_pair.find(pp));
// 			   		cross_pair.erase(cross_pair.find(pp));
// 			   	}

// 		   		pair<int, int> pp2 = make_pair(letter, v_pos[i].second->next->letter);
// 			   	// cout<<"P4: "<<pp2.first<<" "<<pp2.second<<"\n";

// 			   	if(heap.find(pp2) != heap.end())
// 		   			heap.erase(heap.find(pp2));
// 			   	count_pair[pp2]++;
// 				if(new_node->end && v_pos[i].second->next->start != -1)
// 					if(!(is_single_list(new_node) && is_single_list(v_pos[i].second->next)))
// 						cross_pair[pp2]++;

// 				heap.insert(pp2);
// 				pair_positions[pp2].push_back(make_pair(new_node, v_pos[i].second->next));
// 		   	}

// 		   	new_node->prev = v_pos[i].first->prev;
// 		   	new_node->next = v_pos[i].second->next;

// 		   	if(v_pos[i].second->next != NULL)
// 		   		v_pos[i].second->next->prev = new_node;
// 		   	// if(v_pos[i].first->prev != list_begin)
// 		   	v_pos[i].first->prev->next = new_node;
// 		}
// 	}
// 	heap.erase(heap.find(best_pair));
// 	count_pair.erase(count_pair.find(best_pair));
// 	cross_pair.erase(cross_pair.find(best_pair));
// 	pair_positions.erase(pair_positions.find(best_pair));
// 	// map.erase()

// }

void slp2::compress(vector<int> txt) {
	suffix_tree st;
	vector<unsigned int> factors;
	vector<unsigned int> factor_starts;
	st.lz77(txt, factors, factor_starts);

	vector<int> start, end, pair;
	start.resize(txt.size());
	end.resize(txt.size());
	pair.resize(txt.size());

	start[0] = 0;
	end[0] = 1;

	int len = txt.size();

	for(int i=1; i<factors.size(); ++i) {
		for(int j=factors[i-1]; j<factors[i]; ++j) {
			start[j] = -1;
			end[j] = 0;
		}
		start[factors[i-1]] = factor_starts[i];
		end[factors[i]-1] = 1;
	}


	for(int i=0; i<txt.size(); ++i) {
		next_nonterm = max(next_nonterm, txt[i] + 1);		
	}


	queue<int> free_nodes;
	vector<txt_letter_v2> txt_list;
	txt_list.resize(txt.size()*2);
	int current_txt = 0;
	for(int i=0; i<txt_list.size(); ++i) free_nodes.insert(i);
	make_list(txt, start, end, txt_list, free_nodes);

	// // cout<<"State: \n"; print_state(len, txt, start, end);

	// // preproc_pairs(count_pair, cross_pair, start, end, len, txt);
	// // cout<<count_pair[make_pair(97, 97)]<<"\n";
	// // cout<<cross_pair[make_pair(97, 97)]<<"\n";

	// // best_pair(count_pair, cross_pair);

	// txt_letter* node = list_begin;
	// // while(node != NULL) {
	// // 	cout<<node->letter<<" ";
	// // 	node = node->next;
	// // }
	// // cout<<"\n";

	// set<txt_letter*, int> removed;

	// map<std::pair<int, int>, int> count_pair;
	// map<std::pair<int, int>, int> cross_pair;
	// map<std::pair<int, int>, vector<std::pair<txt_letter*, txt_letter*>>> pair_positions;

	// preproc_pairs_list(list_begin->next, count_pair, cross_pair, pair_positions);
	// cmp_pairs cmp(&count_pair, &cross_pair);
	// set<std::pair<int, int>, cmp_pairs> heap(cmp);

	// for(auto it = count_pair.begin(); it != count_pair.end(); ++it) {
	// 	heap.insert(it->first);
	// 	// std::pair<int, int> px = it->first;
	// 	// cout<<"Inserting: "<<px.first<<" "<<px.second<<"\n";
	// }
	// // set<std::pair<std::pair<int, int>, int> 

	// // cout<<"State: \n"; print_state(len, txt, start, end);
	// // print_state_list(list_begin);
	// // cout<<cross_pair[make_pair(32,73)]<<" <<<<<\n";
	// set<txt_letter*> removed_nodes;
	// // return;
	// do {
	// 	// cout<<removed_nodes.size()<<"\n";
	// 	std::pair<int, int> pp = *heap.begin();

	// 	// print_state_list(list_begin);
	// 	if(count_pair[pp] < 2) break;
	// 	// if(count_pair[pp] < 10) break;

	// 	// cout<<pp.first<<" "<<pp.second<<" , "<<count_pair[pp]<<" "<<cross_pair[pp]<<"\n";
	// 	// cout<<"Heap: \n";
	// 	// for(auto it = heap.begin(); it != heap.end(); ++it) {
	// 	// 	cout<<(*it).first<<" "<<(*it).second<<" "<<cross_pair[*it]<<" | ";
	// 	// }
	// 	// cout<<"\n";
	// 	// cout<<"ent\n";
	// 	replacement_list(pp, heap, removed_nodes, count_pair, cross_pair, pair_positions);
	// 	// cout<<"ext\n";
	// 	// heap.erase(heap.begin());
	// 	// cout<<"repl\n";
	// 	// return;
	// } while(true);

	// return;

	// std::pair<int,int> bp;
	// do {
	// 	// cout<<"L: "<<len<<"\n";
	// 	map<std::pair<int, int>, int> count_pair;
	// 	map<std::pair<int, int>, int> cross_pair;
	// 	preproc_pairs(count_pair, cross_pair, start, end, len, txt);
	// 	bp = best_pair(count_pair, cross_pair);
	// 	// cout<<count_pair[bp]<<" | "<<bp.first<<" "<<bp.second<<"\n";
	// 	if(count_pair[bp] < 2) break;
	// 	replacement(bp, txt, len, start, end);
	// 	// cout<<"State: \n"; print_state(len, txt, start, end);

	// } while(true);
	// res_text = txt;

	// txt_letter *n = list_begin->next;
	// while(n != NULL) {
	// 	res_text.push_back(n->letter);
	// 	n = n->next;
	// }
	// cout<<"Approx2	: "<<factors.size()<<" "<<nonterms.size()*2 <<"+"<<res_text.size()<<"\n";
}

// vector<unsigned int> slp2::get_grammar() {
// 	// cout<<"Nonterms: "<<nonterms.size()<<"\n";
// 	vector<unsigned int> grammar;
// 	for(auto it = nonterms.begin(); it != nonterms.end(); ++it) {
// 		grammar.push_back(it->first.first);
// 		grammar.push_back(it->first.second);
// 		grammar.push_back(it->second);	
// 	}

// 	for(int i=0; i<res_text.size(); ++i)
// 		grammar.push_back(res_text[i]);






// 	// vector<int> freq;
// 	// for(auto it = nonterms_count.begin(); it != nonterms_count.end(); ++it) {
// 	// 	freq.push_back(it->second);
// 	// }

// 	// sort(freq.begin(), freq.end(), std::greater<int>());
// 	// cout<<"Most common: "<<freq[0]<<" P5: "<<freq[freq.size()*5/100]<<"\n";

// 	return grammar;
// }