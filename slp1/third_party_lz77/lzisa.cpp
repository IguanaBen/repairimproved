////////////////////////////////////////////////////////////////////////////////
// lzisa.cpp
//   Implementations of the main parsing functions. By choosing a suitable set
//   of parameters and auxiliary data structures, three different versions are
//   obtained:
//
//     lzisa9  - the fastest, uses 9n bytes, works for arbitrary inputs,
//     lzisa6s - space-efficient but slower version of lzisa9,
//               uses 6n bytes, works for arbitrary inputs,
//     lzisa6r - a version of lzisa6s specialized for repetitive
//               inputs, uses at most 5.628n + 16r bytes of space,
//               where r is the number of maximal runs of equal
//               characters in the BWT of the input string. The
//               total space usage is therefore at most 6n only
//               if the average run length in BWT of the input
//               string satisfies n/r > 42.
//
//   Space requirements exclude the output factorization (which is
//   produced in order, hence can be streamed to disk) but include the
//   input strings. All algorithms rely on the prior computation of
//   the suffix array, which can be done without increasing peak memory
//   usage (e.g. using https://sites.google.com/site/yuta256/sais).
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2013 Juha Karkkainen, Dominik Kempa and Simon J. Puglisi
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <algorithm>

#include "lzisa.h"
#include "sparse_lf.h"
#include "rle_lf.h"
#include "rmq_tree.h"

// Compute the phrase starting at position i in X[0..n-1]
// given psv = PSV_text[i] and nsv = NSV_text[i].
inline int parse_phrase(const unsigned char *X, int n, int i, int psv, int nsv,
    std::vector<std::pair<int, int> > *F) {
  int pos = -1, len = 0;
  if (nsv != -1 && psv != -1) {
    while (X[psv + len] == X[nsv + len]) ++len;
    if (X[i + len] == X[psv + len]) {
      ++len;
      while (X[i + len] == X[psv + len]) ++len;
      pos = psv;
    } else {
      while (i + len < n && X[i + len] == X[nsv + len]) ++len;
      pos = nsv;
    }
  } else if (psv != -1) {
    while (X[psv + len] == X[i + len]) ++len;
    pos = psv;
  } else if (nsv != -1) {
    while (i + len < n && X[nsv + len] == X[i + len]) ++len;
    pos = nsv;
  }
  if (!len) pos = X[i];
  if (F) F->push_back(std::make_pair(pos, len));
  return i + std::max(1, len);
}

int lzisa6s(const unsigned char *X, const int *SA, int n,
    std::vector<std::pair<int, int> > *F) {
  // Initialize auxiliary data structures.
  rmq_tree RMQ(SA, n);
  sparse_lf LF(X, SA, n);
  int *ISA = (int *)malloc(sizeof(int) * (n / 5 + 1));
  for (int i = 0; i < n/5; i += 37337)
    ISA[i] = 0; // Prevents cache anomalies.
  for (int i = 0; i < n; ++i)
    if (SA[i] % 5 == 0) ISA[SA[i] / 5] = i;

  int i = 0, j = 0, nphrases = 0;
  while(i < n) {
    // Locate ISA[i].
    while (j % 5) j = (j == n - 1) ? 0 : j + 1;
    int jisa = ISA[j / 5];
    while (j != i) { jisa = LF.query(jisa); j = j ? j - 1 : n - 1; }

    // Parse next phrase using PSV/NSV positions.
    int psv = RMQ.psv(jisa, i), nsv = RMQ.nsv(jisa, i);
    j = i = parse_phrase(X, n, i, psv, nsv, F);
    ++nphrases;
  }

  free(ISA);
  return nphrases;
}

int lzisa6r(const unsigned char *X, const int *SA, int n,
    std::vector<std::pair<int, int> > *F) {
  rmq_tree RMQ(SA, n);
  rle_lf LF(X, SA, n);
  int *ISA = (int *)malloc(sizeof(int) * (n / 8 + 1));
  for (int i = 0; i < n / 8; i += 37337) ISA[i] = 0;
  for (int i = 0; i < n; ++i)
    if (!(SA[i] & 7)) ISA[SA[i] >> 3] = i;

  int i = 0, j = 0, nphrases = 0;
  while(i < n) {
    while (j & 7) j = (j == n - 1) ? 0 : j + 1;
    int jisa = ISA[j >> 3];
    while (j != i) {
      jisa = LF.query(jisa);
      j = j ? j - 1 : n - 1;
    }
    int psv = RMQ.psv(jisa, i), nsv = RMQ.nsv(jisa, i);
    j = i = parse_phrase(X, n, i, psv, nsv, F);
    ++nphrases;
  }

  free(ISA);
  return nphrases;
}

int lzisa9(const unsigned char *X, const int *SA, int n,
    std::vector<std::pair<int, int> > *F) {
  rmq_tree RMQ(SA, n);
  int *ISA = (int *)malloc(sizeof(int) * n);
  for (int i = 0; i < n; i += 37337) ISA[i] = 0;
  for (int i = 0; i < n; ++i) ISA[SA[i]] = i;

  int i = 0, nphrases = 0;
  while(i < n) {
    int iisa = ISA[i];
    int psv = RMQ.psv(iisa, i), nsv = RMQ.nsv(iisa, i);
    i = parse_phrase(X, n, i, psv, nsv, F);
    ++nphrases;
  }

  free(ISA);
  return nphrases;
}
