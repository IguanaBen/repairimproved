////////////////////////////////////////////////////////////////////////////////
// sparse_lf.h
//   Space efficient representation of LF mapping that exploits the
//   presence of uncompressed suffix array. For a string of length n
//   over byte alphabet, it requires at most (8/(2^bits) + 1/64)n
//   bytes of space, where bits is a parameter defined in the class.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2013 Juha Karkkainen, Dominik Kempa and Simon J. Puglisi
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
////////////////////////////////////////////////////////////////////////////////

#ifndef __SPARSE_LF
#define __SPARSE_LF

#include <algorithm>
#include <numeric>
#include <vector>

struct sparse_lf {
  sparse_lf(const unsigned char *ax, const int *aSA, int an)
      :x(ax), SA(aSA), n(an) {
    std::fill(C, C + 257, 0);

    // Secondary lookup table.
    static const int bufsize = 1 << 12;
    unsigned char buf[bufsize];
    int i = 0;
    while (i < n) {
      int nfill = std::min(n - i, bufsize), skip = -1;
      for (int j = 0; j < nfill; ++j)
        if (SA[i + j]) buf[j] = x[SA[i + j] - 1];
        else skip = j;
      for (int j = 0; j < nfill; ++j)
        if (j != skip && !(C[buf[j] + 1]++ & mask))
          occ[buf[j]].push_back(i + j);
      i += nfill;
    }

    // Add sentinels.
    for (int j = 0; j < 256; ++j)
      occ[j].push_back(n);

    // Primary lookup table.
    std::partial_sum(C, C + 257, C);
    std::fill(lookup, lookup + 256, (int *)NULL);
    for (i = 0; i < 256; ++i) if (!occ[i].empty()) {
      lookup[i] = (int *)malloc((n / lookup_rate + 1) * sizeof(int));
      for (int j = 0, p = 0; j < n; j += lookup_rate) {
        while (occ[i][p] <= j) ++p;
        lookup[i][j >> lookup_bits] = p;
      }
    }
  }

  // Return LF[i].
  inline int query(int i) const {
    if (!SA[i]) return C[x[n - 1]];
    unsigned char c = x[SA[i] - 1];
    int pos = lookup[c][i >> lookup_bits]; // First shortcut.
    while (occ[c][pos] <= i) ++pos;
    pos = C[c] + (pos - 1) * pow;  // Second shortcut.
    while (SA[pos] != SA[i] - 1) ++pos;
    return pos;
  }

  ~sparse_lf() {
    for (int i = 0; i < 256; ++i)
      free(lookup[i]);
  }


  static const int bits = 6;
  static const int pow = 1 << bits;
  static const int mask = pow - 1;

  const unsigned char *x;
  const int *SA, n;

  int C[257];
  std::vector<int> occ[256];

  int *lookup[256];
  static const int lookup_bits = 16;
  static const int lookup_rate = 1 << lookup_bits;
};

#endif // __SPARSE_LF
