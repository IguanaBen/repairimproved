#ifndef LZ77_WRAPPER
#define LZ77_WRAPPER

#include "divsufsort.h"
#include "common.h"
#include "lzisa.h"

class lz77_wrapper {
	public:
	static void lz77(vector<int> _txt, vector<unsigned int> &factors, vector<unsigned int> &factor_starts){
  		
  		int length = _txt.size();
  		int *sa = new int[length];
  		unsigned char *txt  = new unsigned char[length];
		vector<unsigned char> uchar_txt;
		for(int i=0; i<_txt.size(); ++i) {
			// uchar_txt.push_back(txt[i]);
			if(_txt[i] > 255)
				cerr<<"possible overflow, but continue anyway, #yolo\n";
			txt[i] = _txt[i];
		}

		divsufsort(txt, sa, length);
  		std::vector<std::pair<int, int> > phrases;
		lzisa9(txt, sa, length, &phrases);

		// cout<<"AA\n";
		vector<unsigned int> _factors;
		int current_sum = 1;
		for(int i=1; i<phrases.size(); ++i) {
			// cout<<phrases[i].first<<"|"<<phrases[i].second<<" ";
			int current_factor = phrases[i].second;
			if(current_factor == 0) current_factor++;
			_factors.push_back(current_sum);
			current_sum += current_factor;
		}
		cout<<"\n";

		_factors.push_back(length);
		factors = _factors;

		delete[] txt;
		delete[] sa;

	}

};

#endif