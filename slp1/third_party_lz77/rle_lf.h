////////////////////////////////////////////////////////////////////////////////
// rle_lf.h
//   Space efficient representation of LF mapping taking advantage of
//   long runs of equal symbols occurring in BWT of repetitive strings.
//   For a string of length n over byte alphabet with r runs of equal
//   character in BWT, it requires at most n/8 + 16r bytes.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2013 Juha Karkkainen, Dominik Kempa and Simon J. Puglisi
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
////////////////////////////////////////////////////////////////////////////////

#ifndef __RLE_LF
#define __RLE_LF

#include <numeric>
#include <vector>

struct rle_lf {
  rle_lf(const unsigned char *x, const int *SA, int n) {
    int C[257]  = {0};
    for (int i = 0; i < n; ++i) ++C[x[i] + 1];
    std::partial_sum(C, C + 256, C);

    // Store runs in LF using RLE compression.
    unsigned char ch = x[n - 1];
    int beg = -1, last = C[ch], i = 0;
    static const int bufsize = (1 << 16);
    unsigned char *buf = new unsigned char[bufsize];
    while (i < n) {
      int nfill = std::min(n - i, bufsize), dollar = -1;
      
      // Fill buffer.
      for (int j = 0; j < nfill; ++j)
        if (SA[i + j]) buf[j] = x[SA[i + j] - 1];
        else dollar = j;
      
      // Process buffer.
      for (int j = 0; j < nfill; ++j) {
        if (j != dollar) {
          if (buf[j] != ch) {
            LF.push_back(std::make_pair(beg, C[ch]));
            C[ch] += i + j - beg;
            beg = i + j; ch = buf[j];
          }
        } else {
          // Add run ending just before dollar.
          LF.push_back(std::make_pair(beg, C[ch]));
          C[ch] += i + j - beg;

          // Add a single run for a dollar.
          LF.push_back(std::make_pair(i + j, last));

          // Start the next run.
          if (i + j == n - 1) beg = -2;
          else ch = x[SA[beg = i + (++j)] - 1];
        }
      }
      i += nfill;
    }
    if (beg != -2) LF.push_back(std::make_pair(beg, C[ch]));
    LF.push_back(std::make_pair(n, -1));
    delete[] buf;

    // Primary lookup table.
    lookup.reserve(n / lookup_rate + 1);
    for (int j = 0, p = 0; j < n; j += lookup_rate) {
      while (LF[p].first <= j) ++p;
      lookup[j >> lookup_bits] = p - 1;
    }
  }

  // Return LF[i].
  inline int query(int i) const {
    int j = lookup[i / lookup_rate];
    while (LF[j].first <= i) ++j;
    return LF[j - 1].second + (i - LF[j - 1].first);
  }

  static const int lookup_bits = 5;
  static const int lookup_rate = 1 << lookup_bits;
  std::vector<int> lookup;
  std::vector<std::pair<int, int> > LF;
};

#endif // __RLE_LF
