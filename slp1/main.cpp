// Michal Ganczorz 2016
/*
	This demo compress and decompress data by chunk,
	then writes statistics to files

	It does not save compressed files
*/


#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <chrono>
// #include "suffix_array.h"
#include "suffix_tree.h"
// #include "suffix_tree2.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v3.h"
// #include "compress.h"
#include "compress_benchmark.h"
#include "third_party_lz77/third_party_lz77_wrapper.h"
#include "elias_coding.h"
#include "grammar_binary_compressor.h"
#include "grammar_seq_compressor.h"
#include "huffman_compression/huffman_wrapper.h"
// #include "compress_benchmark.h"

#define MAX_THREADS 20

using namespace std;


int main()
{
	
	vector<string> files_to_test = {"bible.txt"};
	// vector<string> files_to_test = {"E.coli", "pitches.50MB", "dblp.xml.50MB", "book1", "book2", "news", "geo", 
	// 					"bible.txt", "world192.txt" ,"english.50MB", "dna.50MB", "proteins.50MB", "sources.50MB"};

	float step1 = 0.05f;
	int iterations1 = 45;
	float step2 = 0.05f;
	int iterations2 = 45;

	int block_size = 1024*1024;

	string inpref = "test_files\\";
	string outpref = "RESULTS\\";
	string debug_outpref = "RESULTS_DEBUG\\";

	vector<thread*> threads(MAX_THREADS);

	for(int i=0; i<files_to_test.size(); ++i) {
		string filename = files_to_test[i];



		// threads[2*i] = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function0", debug_outpref + filename + ".debug.function0", 
			// block_size, step1, step1, iterations1, 1, 0, 1, 90000000); } );

		//RePairImp
		threads[2*i + 1] = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function1", debug_outpref + filename + ".debug.function1", 
			block_size, step2, step2, iterations2, 2, 0, 1, 90000000); } );

	}

	for(int i=0; i<files_to_test.size(); ++i){
		// threads[2*i]->join();
		threads[2*i + 1]->join();

	}






	return 0;

}