#include "ffactor.h"

static void ffactor::factor(vector<int> &_text, vector<unsigned int> &f_positions) {

	map<vector<int>, int> freqs;

	f_positions.clear(); //just in case

	float coeff = 1.0f;

	const int max_length = 15;
	for(int i=0; i<_text.size();) {

		vector<int> current_match;
		auto next_match = current_match;
		next_match.push_back(_text[i]);

		int start = i;
		f_positions.push_back(start);
		while(current_match.size() < max_length-1 && freqs.find(next_match) != freqs.end() && i < _text.size()) {

			if( freqs[next_match]*coeff > freqs[current_match])

			current_match = next_match;
			i++;
			next_match.push_back(_text[i]);
		}
	}
	// mmkey?
}

// abcxe|abcxe|z|z|abcxeabcxe

// 2 -> 1 
// abc|abc|abca|bc
// abc|abc|abc|abc|abc
// avg size?  <- n^2 (?)


// levels?
// weights to factors?
// abc|abc|abc|abc|abc|abc|def|abc|def
// abc|abc|abc|abc|def|abc|edf||abc|def||