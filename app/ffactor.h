#ifndef FFACTOR_H
#define FFACTOR_H

#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <iostream>
#include <queue>

using namespace std;

class ffactor{

	public:
		static void factor(vector<int> &_text, vector<unsigned int> &f_positions);
};

#endif