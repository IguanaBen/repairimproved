#ifndef SLP2_H
#define SLP2_H

#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <iostream>
// #include "huffman.h"

using std::vector;
using std::pair;
using namespace std;

struct txt_letter {
	int start;
	bool end;
	int letter;
	txt_letter *next = NULL;
	txt_letter *prev = NULL;
};

struct cmp_pairs{
	
	map<std::pair<int, int>, int> *_count_pair;
	map<std::pair<int, int>, int> *_cross_pair;

	cmp_pairs(
		map<std::pair<int, int>, int> *count_pair,
		map<std::pair<int, int>, int> *cross_pair){
		_count_pair = count_pair;
		_cross_pair = cross_pair;
	}

	float weigth(int pair_cnt,
			 int pair_cross) {

		float pcnt = pair_cnt;
		float pcrs = pair_cross;
		// return pcnt;
		// cout<<pcnt<<" x "<<pcrs<<"\n";
		// return pcnt/pcrs;
		if(pair_cnt < 2) return 0.0f;
		float w = pair_cnt*pair_cnt/(pair_cross+1);
		// float w = pair_cnt - pair_cross;
		if(w > 1.0f) return w; else return 1.0f;
		return pcnt - 2.0f*pcrs;
		// return pcnt;
		// return pcnt;	

	}

	bool operator() (std::pair<int, int> p1, std::pair<int, int> p2)
    {
    	int cnt_p1 = (*_count_pair)[p1];
    	int cnt_p2 = (*_count_pair)[p2];
    	float w1 = weigth(cnt_p1, (*_cross_pair)[p1]);
    	float w2 = weigth(cnt_p2, (*_cross_pair)[p2]);
    	// cout<<p1.first<<" "<<p1.second<<" "<<(*_count_pair)[p1] << " " <<(*_cross_pair)[p1]<<" "<<w1<<" <-\n";
    	// cout<<p2.first<<" "<<p2.second<<" "<<(*_count_pair)[p2] << " " <<(*_cross_pair)[p2]<<" "<<w2<<" <<-\n";

    	// if(cnt_p1 == 1 || cnt_p2 > 1) return true;
    	// if(cnt_p2 == 1 || cnt_p1 > 1) return false;

    	if(w1 == w2) return p1 > p2;
    	return w1 > w2;
    }
};

class slp2 {
	private:
		map<pair<int, int>, int> nonterms;
		map<pair<int, int>, int> nonterms_count;

		set<pair<txt_letter*, txt_letter*>> is_crossing_pair;

		int next_nonterm = 0;

		// vector<int> iteration(vector<int> txt, vector<int> &start, vector<int> &end);
		void print_state(int len, vector<int> txt, vector<int> start, vector<int> end);
		// void preproc_pairs(map<pair<int, int>, int> &count_pair, map<pair<int, int>,  int> &cross_pair, int len, vector<int> &txt);
		void preproc_pairs(map<pair<int, int>, int> &count_pair,
						   map<pair<int, int>,  int> &cross_pair,
						   vector<int> &start,
						   vector<int> &end,
						   int len,
						   vector<int> &txt);
		pair<int, int> best_pair(map<pair<int, int>, int> &count_pair,
						  	 map<pair<int, int>,  int> &cross_pair);

		float weigth(int pair_cnt, int pair_cross);
		txt_letter* list_begin;
		txt_letter* list_end;
		// void make_list(vector<int> &txt);

		void split(int i, vector<int> &start, vector<int> &end);
		void replacement(pair<int, int> best_pair, vector<int> &txt, int &len, vector<int> &start, vector<int> &end);	
		bool is_single(int i, vector<int> &start, vector<int> &end);
		vector<int> res_text;

		void make_list(vector<int> &txt, vector<int> &start, vector<int> &end);
		bool is_single_list(txt_letter* node);
		void preproc_pairs_list(
			txt_letter* begin,
			map<std::pair<int, int>, int> &count_pair,
			map<std::pair<int, int>, int> &cross_pair,
			map<std::pair<int, int>, vector<pair<txt_letter*, txt_letter*>>> &pair_positions);
		void replacement_list(
			pair<int, int> best_pair,
			set<std::pair<int, int>, cmp_pairs> &heap,
			set<txt_letter*> &removed_nodes,
			map<std::pair<int, int>, int> &count_pair,
			map<std::pair<int, int>, int> &cross_pair,
			map<std::pair<int, int>, vector<pair<txt_letter*, txt_letter*>>> &pair_positions);
		void print_state_list(txt_letter* node);
		vector<bool> get_compressed_grammar_v2();

	public:
		vector<bool> get_compressed_grammar();
		void compress(vector<int> txt);
		vector<unsigned int> get_grammar();
};

#endif