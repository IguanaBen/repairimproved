#ifndef GRAMMAR_SEQ_COMPRESSOR
#define GRAMMAR_SEQ_COMPRESSOR

// #include "grammar_binary_compressor.h"

class grammar_seq_compressor {

	public:
		static vector<int> decompress(map<int, pair<int, int>> &grammar, vector<int> &seq) {

			vector<int> res;
			// int current = first;
			for(int i=0; i<seq.size(); ++i) {
				// cout<<"flat: "<<i<<"\n";
				flatten(seq[i], grammar, res);
			}
			return res;

		}
		static void flatten(int nonterm, map<int, pair<int, int>>  &r_grammar, vector<int> &res){

			if(r_grammar.find(nonterm) == r_grammar.end()){
				res.push_back(nonterm);
			} else {
				pair<int, int> pp = r_grammar[nonterm];
				flatten(pp.first, r_grammar, res);
				flatten(pp.second, r_grammar, res);

			}
		}


		// map<int, int> rename_symbols(map<int, pair<int, int>> &orginal_grammar, map<int, pair<int, int>> &ref_grammar) {
		// 	map<int, int> result;
		// 	vector<int> org_generation(257 + orginal_grammar.size());
		// 	vector<int> ref_generation(257 + orginal_grammar.size());

		// 	vector<vector<int>> org_gens;
		// 	vector<vector<int>> ref_gens;


		// 	//dry
		// 	int mx_gen_org = 0;
		// 	for(auto it = orginal_grammar.begin(); it != orginal_grammar.end(); ++it){

		// 		grammar_binary_compressor::calculate_generation(it->first, orginal_grammar, org_generation);
		// 		mx_gen_org = max(org_generation[it->first], mx_gen_org);

		// 		while(org_generation[it->first] >= org_gens.size());
		// 			org_gens.push_back(vector<int>());

		// 		org_gens[org_generation[it->first]-1].push_back(it->first);
		// 	}

		// 	int mx_gen_ref = 0;
		// 	for(auto it = ref_grammar.begin(); it != ref_grammar.end(); ++it){

		// 		grammar_binary_compressor::calculate_generation(it->first, ref_grammar, ref_generation);
		// 		mx_gen_ref = max(ref_generation[it->first], mx_gen_ref);

		// 		while(ref_generation[it->first] >= ref_gens.size());
		// 			ref_gens.push_back(vector<int>());

		// 		ref_gens[ref_generation[it->first]-1].push_back(it->first);
		// 	}

		// 	// map<pair<int, int>, int>
		// 	int current_pair = 0;
		// }

		// void rename_recursive(
		// 	int nonterm,
		// 	int current_index,
		// 	map<int, int> &result,
		// 	map<int, pair<int, int>> &orginal_grammar,
		// 	map<int, pair<int, int>> &source_grammar) {

		// 	if(orginal_grammar.find(nonterm) == orginal_grammar.end()
		// 		&& ref_grammar.find(nonterm) == ref_grammar.end()) {
		// 		result[nonterm] = nonterm;
		// 	} else {

		// 		auto pp 

		// 	}

		// }
};


#endif