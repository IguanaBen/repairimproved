#ifndef SLP2V3_H
#define SLP2V3_H

#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <cmath>
// #include "huffman.h"

using std::vector;
using std::pair;
using namespace std;

namespace slpv3{

//jeden nie jest potrzebny, tzn po co ci start i end
struct txt_letter {
	bool start;
	bool end;
	int letter;
	int prev, next;
};

struct tri {
	pair<int, int> p;
	float w; 
};

// struct cmp_pairs{
	
// 	unordered_map<long long, int> *_count_pair;
// 	unordered_map<long long, int> *_cross_pair;
// 	// map<std::pair<int, int>, int> *_count_pair;
// 	// map<std::pair<int, int>, int> *_cross_pair;

// 	float _coeff;
// 	cmp_pairs(
// 		unordered_map<long long, int> *count_pair,
// 		unordered_map<long long, int> *cross_pair,
// 		// map<std::pair<int, int>, int> *count_pair,
// 		// map<std::pair<int, int>, int> *cross_pair,
// 		float coeff){
// 		_count_pair = count_pair;
// 		_cross_pair = cross_pair;
// 		_coeff = coeff;
// 	}

// 	bool operator() (std::pair<int, int> p1, std::pair<int, int> p2)
//     {
//     	int cnt_p1 = (*_count_pair)[convert_pair(p1)];
//     	int cnt_p2 = (*_count_pair)[convert_pair(p2)];
//     	float w1 = weigth(cnt_p1, (*_cross_pair)[convert_pair(p1)]);
//     	float w2 = weigth(cnt_p2, (*_cross_pair)[convert_pair(p2)]);
//     	// cout<<p1.first<<" "<<p1.second<<" "<<(*_count_pair)[p1] << " " <<(*_cross_pair)[p1]<<" "<<w1<<" <-\n";
//     	// cout<<p2.first<<" "<<p2.second<<" "<<(*_count_pair)[p2] << " " <<(*_cross_pair)[p2]<<" "<<w2<<" <<-\n";

//     	// if(cnt_p1 == 1 || cnt_p2 > 1) return true;
//     	// if(cnt_p2 == 1 || cnt_p1 > 1) return false;

//     	if(w1 == w2) return p1 > p2;
//     	return w1 > w2;
//     }
// };

struct cmp_triples{
	float _coeff;
	cmp_triples(float coeff){
		_coeff = coeff;
	}

	bool operator() (tri t1, tri t2){
    	if(t1.w == t2.w)
    		 return t1.p > t2.p;
    	else
    		return t1.w > t2.w;
    }
};

class slp2_v3 {
	private:
		// bool reverse_grammar = false;

		int _limit;

		int _method;
		int _crossing_type;

		int largest_orginal_letter;

		int _lz77_factors;

		int leaf_cnt = 0;
		float _coeff;

		int first;
		int next_nonterm;
		int uncompressed_len;
		vector<txt_letter> text;
		queue<int> next_free;
		set<int> deleted;
		vector<bool> deleted_v;

		// unordered_map<long long, int> grammar;
		map<pair<int, int>, int> grammar;
		unordered_map<long long, int> pair_count;
		unordered_map<long long, int> cross_count;
		unordered_map<long long, vector<pair<int,int>>> pair_positions;
		// map<pair<int, int>, int> pair_count;
		// map<pair<int, int>, int> cross_count;
		// map<pair<int, int>, vector<int>> pair_positions;

		set<tri, cmp_triples> *pair_heap;

		pair<int, int> pair_to_replace();
		int replace_pair(pair<int, int> pp, int first);
		void replace_pair_v2(pair<int, int> pp, int first);
		void replace_pair_v3(pair<int, int> pp, int first);
		void replace_pair_v4(pair<int, int> pp, int first);
		int replace_pair_noncrossing_first(pair<int, int> pp, int first);


		void compress(vector<int> &txt, vector<int> &result, float coeff, int limit);

		inline void decrease_occurences(int fst_index, int snd_index);
		inline void increase_occurences(int fst_index, int snd_index);

		//debug
		void print_forward(int first, int print_len);
		void print_backward(int last, int print_len); // sssssss

		void replace_at(int i, int new_letter);
		int len(int first);

		void split_letter(int index);

		// vector<int> flatten(int nonterm);
		void flatten(int nonterm, map<int, pair<int, int>> &r_grammar, vector<int> &res);

		pair<int, int> check_count(int first, pair<int, int> pp);

		inline int is_crossing(int fst_index, int snd_index) {

			if(_crossing_type == 0) {
				return (text[fst_index].end && text[snd_index].start &&
						!text[fst_index].start && !text[snd_index].end);
			} else 
			if(_crossing_type == 1 || _crossing_type == 2)
			{

				if (_crossing_type == 1) {
					return (text[fst_index].end && text[snd_index].start);
				}

				if ( _crossing_type == 2 ) {
					if(text[fst_index].end && text[snd_index].start) {
					//crossing 
						if(!text[fst_index].start && !text[snd_index].end){
							return 2; // 2 dla kar podwojnych
						} else {
							if(!text[fst_index].start || !text[snd_index].end) { //
								return 1; // 1 dla kar pojedynczych
							} else {
								return 0;
							}
						}
					} else {
						return 0;
					}
				}
				
			} else {
				cerr<<"Not supported method\n";
				exit(1);
			}

			// if (!(text[fst_index].letter <= largest_orginal_letter &&
			//    text[snd_index].letter <= largest_orginal_letter))
			// 	return 0;


			// if(text[fst_index].end && text[snd_index].start) {
			// 	//crossing 
			// 	if(!text[fst_index].start)
			// 		return 1;
			// 	else
			// 		return 0;
			// } else {
			// 	return 0;
			// }

			if(text[fst_index].end && text[snd_index].start) {
				//crossing 
				if(!text[fst_index].start && !text[snd_index].end){
					return 1; // 2 dla kar podwojnych
				} else {
					if(!text[fst_index].start || !text[snd_index].end) { //
						return 1; // 1 dla kar pojedynczych
					} else {
						return 0;
					}
				}
			} else {
				return 0;
			}

			// return (text[fst_index].end && text[snd_index].start);
		}

		inline tri make_tri(int f, int s) {
			tri r;
			r.p = make_pair(f,s);
			r.w = weigth(r.p);
			return r;
		}

		inline tri make_tri(pair<int, int> p) {
			tri r;
			r.p = p;
			r.w = weigth(r.p);
			// cout << "ABC\n";
			return r;
		}

		float weigth(pair<int, int> pp) {

			int pair_cnt = pair_count[convert_pair(pp)];
			int pair_cross = cross_count[convert_pair(pp)];

			float pcnt = pair_cnt;
			float pcrs = pair_cross;

			///
			// int diff = pcnt - pcrs;
			// if(diff == 0)
			// 	return pcnt;
			// else 
			// 	return pcrs*_coeff;
			///


			// if(pcnt < 500)
			// 	return pcnt;

			//float w =  pcnt - _coeff*sqrt(pcrs); // meh
			// float w = pcnt - _coeff*pcrs;
			// float w = pcnt*log(pow(pcnt,2.0f + _coeff)/(1.0f+pcrs));
			//float w = pcnt*(log2(pcnt*(0.1+ _coeff) )-log2(pcrs));

			// float w = pow(pcnt, log2(pcnt/(pcrs + 1.0f)) );
			// float w = pcnt*(1.0f-pcrs/(pcnt*_coeff));
			// float w = pcnt + _coeff*(pcnt/(pcrs + 1.0f));  //times the logarithm of length 

			// float w = pow(pcnt, 1.0f - _coeff*pcrs/pcnt);
			// float w = pow(pcnt, 4.0f + _coeff*pcnt/(pcrs + 1.0f));

			float w = 0.0f;
			switch(_method) {

				case 0: //Repair
					w = pcnt;
					break;

				case 1: //Func1
					w = pcnt - _coeff*pcrs;
					break;

				case 2: //Func2
					//w = pow(pcnt, 2.0f + _coeff)/(pcrs + 1.0f); // 3% gain
					// cout<<"super\n";
					w = pow(pcnt, 1.0f + _coeff)/(pcrs + 1.0f); // 3% gain
					break;

				default:
					cout<<"Wrong method\n";
					exit(1);
					break;
			}

			//w = -pcrs;
			
			if(pair_cnt == 0) return std::numeric_limits<float>::lowest() + 0.5f;
			if(pair_cnt == 1) return std::numeric_limits<float>::lowest() + 1.0f;

			// cout<<"DD\n";
			// float w = pow(pcnt, 2.0f*(1.0f + _coeff) )/(pcrs + 1.0f); // 3% gain


			// if(pcnt > 10000)
			// 	w = pow(pcnt, 2.0f + _coeff);

			// float w = pow(pcnt, 2.0f*_coeff)/((pcrs + 1.0f)); //to to samo co wyzej...
			// float w = pcnt/pow(pcrs + 1.0f, _coeff);
			// float w = pcnt*(1.0f - pow(_coeff, pcnt/pcrs));
			// float w = pcnt/(pow(pcrs + 1.0f, _coeff)); // hmm

			// if(_coeff == 0.0f) w = pcnt;
			// float w =  pcnt - _coeff*pcrs*pcrs/pcnt;;

			// if(pcnt < 12)
				// w = pcnt - _coeff*pcrs*pcrs;  

			// if(pcnt == pcrs)
			// 	return pcrs*_coeff;
			// else
			// 	return pcnt-pcrs;

			// float w = pcnt*pcnt/pcrs;
			// float  w = pcnt*pcnt - pcnt*pcrs*_coeff;  // <- w miare ok, pcnt*(pcnt-pcrs)

			// float w = pcnt - pcrs; 
			// float w = pcnt*(pcnt - pcrs*_coeff);
			// float w = pcnt*pcnt - pcrs*_coeff; //
			return w;
		}

		inline long long convert_pair(pair<int, int> pp){
			int fst = pp.first;
			int snd = pp.second;
			long long res = fst;
			res <<= 31;
			res += snd;
			
			return res;
		}

	public:
		
		vector<int> decompress();
		int get_grammar_size();
		int get_remaining_length();
		vector<int> get_remaining_sequence();
		int get_max_nonterm() {
			return next_nonterm-1;
		}

		map<int, pair<int, int>> get_grammar() {
			int mx = 0;
			auto it = grammar.begin();
			map<int, pair<int, int>> r_grammar;
			for(;it != grammar.end(); ++it){
				r_grammar[it->second] = it->first;
			}

			return r_grammar;
		}

		int get_lz77_size() {
			return _lz77_factors;
		}

		slp2_v3(vector<int> txt, float coeff, int method, int crossing_type, int l);
		~slp2_v3();
};

}

#endif