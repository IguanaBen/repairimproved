#ifndef HUFFMAN_WRAPPER
#define HUFFMAN_WRAPPER

#include <iostream>
#include "huffman.h"
#include "../elias_coding.h"

class huffman_wrapper{
	public:
		static int huffman_length(vector<int> text){

			map<unsigned int, string> dict;
			vector<bool> out;
			vector<unsigned int> txt;
			for(int i=0; i<text.size(); ++i) txt.push_back(text[i]);
			huffman_encode(txt, dict, out);
			return out.size()/8;
		}

		static void huffman(vector<int> text, vector<bool> &out, map<unsigned int, string> &dict){

			// map<unsigned int, string> dict;
			// vector<bool> out;
			vector<unsigned int> txt;
			for(int i=0; i<text.size(); ++i) txt.push_back(text[i]);
			huffman_encode(txt, dict, out);
			// return out.size()/8;
		}

		static void huffman_with_dict(vector<int> text, int mx_noterm, vector<bool> &out){

			// map<unsigned int, string> dict;
			// vector<bool> out;
			map<unsigned int, string> dict;
			vector<unsigned int> txt;
			for(int i=0; i<text.size(); ++i) txt.push_back(text[i]);
			
			vector<bool> out_txt;
			huffman_encode(txt, dict, out_txt);

			vector<bool> frequencies;
			map<int, int> freqs;
			for(int i=0; i < text.size(); ++i)
				freqs[text[i]]++;
	
			// int cnt_no_freqs = 0;

			// int mxlg2 = 0;
			// int mx_n = mx_noterm;
			// while(mx_n >>= 1) mxlg2++; 

			vector<bool> out_grammar; //slaba nazwa
			for(int i=0; i<=mx_noterm; ++i) {
				auto f_vec = elias_coding::encode(freqs[i]+1);
				out_grammar.insert(out_grammar.end(), f_vec.begin(), f_vec.end());
				// cnt_no_freqs += f_vec.size();
				// cnt_no_freqs += mxlg2;
			}

			out_grammar.insert(out_grammar.end(), out_txt.begin(), out_txt.end());
			out = out_grammar;

			// cout<<int(out_grammar.size()) + cnt_no_freqs <<"  <>  "<<out_grammar.size() <<" \n";

			// return out.size()/8;
		}

		static vector<int> huffman_decode(int max_nonterm, vector<bool> &compressed_text){

			// map<unsigned int, string> dict;
			// vector<bool> out;


			map<unsigned int, unsigned int> frequencies;

			int pos = 0;
			for(int i=0; i<=max_nonterm; ++i) {
				int decoded_x = 0;
				elias_coding::decode(compressed_text, pos, decoded_x);
				decoded_x--;
				// std::cout<<i<<" "<<decoded_x<<" <-\n";
				if(decoded_x)
					frequencies[i] = decoded_x;
			}
			
			 // 1 2 3 | 4 5
			int last_cnt = compressed_text.size() - pos;
			vector<bool> txt_part(compressed_text.end() - last_cnt, compressed_text.end());
			// for(int i=0; i<txt_part.size(); ++i)
				// cout<<txt_part[i];;
			// cout<<"\n txr\n";
			// cout<<"aa\n";
			// ddd();
			return huffman_decode_binary(frequencies, txt_part);


			// map<unsigned int, string> dict;
			// vector<unsigned int> txt;
			// for(int i=0; i<text.size(); ++i) txt.push_back(text[i]);
			
			// vector<bool> out_txt;
			// huffman_encode(txt, dict, out_txt);

			// vector<bool> frequencies;
			// map<int, int> freqs;
			// for(int i=0; i < text.size(); ++i)
			// 	freqs[text[i]]++;
	
			// vector<bool> out_grammar;			
			// for(int i=0; i<=mx_noterm; ++i) {
			// 	auto f_vec = elias_coding::encode(freqs[i]+1);
			// 	out_grammar.insert(out_grammar.end(), f_vec.begin(), f_vec.end());
			// }

			// out_grammar.insert(out_grammar.end(), out_txt.begin(), out_txt.end());
			// out = out_grammar;

			// return out.size()/8;
		}

};

#endif