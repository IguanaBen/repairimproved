#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <vector>
#include <queue>
#include <string>
#include <map>

using namespace std;

struct huffman_node
{
	huffman_node *left, *right;
	unsigned int letter;
	huffman_node(){left = NULL; right = NULL; }
};

void huffman_encode( vector<unsigned int> &text, map<unsigned int, string> &dict, vector<bool> &out);
void huffman_insert_key(huffman_node* node, string &key, int index, unsigned int value);
void del_huffman_tree(huffman_node* node);

vector<int> huffman_decode_binary(map<unsigned int, unsigned int> &frequencies, vector<bool> &compressed_text);

#endif