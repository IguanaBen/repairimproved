#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <chrono>
// #include "suffix_array.h"
#include "suffix_tree.h"
// #include "suffix_tree2.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v3.h"
// #include "compress.h"
#include "compress_benchmark.h"
// #include "third_party_lz77/third_party_lz77_wrapper.h"
#include "elias_coding.h"
#include "grammar_binary_compressor.h"
#include "grammar_seq_compressor.h"
#include "huffman_compression/huffman_wrapper.h"

int limit = -1;
int block_size = 1024;
int method = 3;
float coefficient = 1.0f;


void save_bool_vector( ofstream &file, vector<bool> &vec){

	unsigned int current_val = 0;
	unsigned int mask = (1<<31);
	unsigned int cmask = mask;

	for(int i=0; i<vec.size(); ++i) {
		if (vec[i]) {
			current_val |= cmask;
		}

		cmask >>= 1;

		if(cmask == 0)
		{
			file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));
			// cout<<current_val<<" cv\n";
			// cout<<current_val<<"\n";

			// return;

			current_val = 0;
			cmask = mask;
		}
	}

	if(cmask != mask)
		file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));

}

void save_chunk(ofstream &file, int max_nonterm, vector<bool> &binary_grammar, vector<bool> &out) {

	unsigned int mx_nt = max_nonterm;
	unsigned int size_grammar = binary_grammar.size();
	unsigned int size_out = out.size();
	
	file.write(reinterpret_cast<char*>(&mx_nt), sizeof(mx_nt));
	file.write(reinterpret_cast<char*>(&size_grammar), sizeof(size_grammar));
	file.write(reinterpret_cast<char*>(&size_out), sizeof(size_out));
	

	// cout<< mx_nt <<" "<<size_grammar<<" "<<size_out<<"\n";

	save_bool_vector(file, binary_grammar);
	save_bool_vector(file, out);
}

void read_bool_vector( ifstream &file, unsigned int size, vector<bool> &vec ){

	unsigned int current_val = 0;
	unsigned int mask = (1<<31);
	unsigned int cmask = mask;

	vec.clear();

	file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
	// cout<<current_val<<"\n";
	// return;
	// cout<<current_val<<" cv\n";

	for(int i=0; i<size; ++i) {

		vec.push_back(cmask & current_val);
		// cout<<"B: "<<cmask && current_val<<"\n";
		cmask >>= 1;
		if(cmask == 0 ) {
			if(i + 1 < size) {

				file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
				// cout<<current_val<<"\n";
				// return;
				cmask = mask;
			}

		}

	}

}

void read_chunk(ifstream &file, int &max_nonterm, vector<bool> &binary_grammar, vector<bool> &out) {
	unsigned int mx_nt;
	unsigned int size_grammar;
	unsigned int size_out;
	
	file.read(reinterpret_cast<char*>(&mx_nt), sizeof(mx_nt));
	file.read(reinterpret_cast<char*>(&size_grammar), sizeof(size_grammar));
	file.read(reinterpret_cast<char*>(&size_out), sizeof(size_out));


	// unsigned int ss;
	// file.read(reinterpret_cast<char*>(&ss), sizeof(ss));

	cout<< mx_nt <<" "<<size_grammar<<" "<<size_out<<"\n";


	binary_grammar.clear();
	out.clear();
	read_bool_vector(file, size_grammar, binary_grammar);
	read_bool_vector(file, size_out, out);


	max_nonterm = mx_nt;
}

void print_help() {
	cout<<"Usage: compress [infile] [outfile]\n";
	cout<<"Parameters:\n";
	cout<<"-m [0-3] : compression method\n";
	cout<<"-l [INT] : grammar size limit per chunk\n";
	cout<<"-c [FLOAT] : f function coefficient\n";
	cout<<"-s [INT] : chunk size in KB\n";
}

int main(int argc, char** argv) {

	vector<string> arguments;

	if (argc == 2) {
		if( argv[1][0] == '-') {
			if( argv[1][1] == 'h') {
				print_help();
				return 0;
			}
		}
	}
	
	if(argc < 3) {
		cout<<"wrong number of arguments, type -h for help\n";
		return 1;
	}

	string infile = string(argv[1]);
	string outfile = string(argv[2]);


	try{
	for(int i=3; i<argc; ++i){
		// cout<<argv[i]<<"\n";
		if( argv[i][0] == '-') {
			if (argv[i][1] == 'h') {
				print_help();
				return 0;
				// cout<<"Usage: compress [infile] [outfile]\n";
			} else
			if (argv[i][1] == 'm') {
				string a = string(argv[++i]);
				method = stoi(a);
			} else 
			if (argv[i][1] == 'l') {
				string a = string(argv[++i]);
				limit = stoi(a);
			} else 
			if (argv[i][1] == 'c') {
				string a = string(argv[++i]);
				coefficient = stof(a);
			} else 
			if (argv[i][1] == 's') {
				string a = string(argv[++i]);
				block_size = stoi(a);
			} else {
				cout<<"bad parameter, type -h for help\n";
				return 1;
			}
		} else {
			cout<<"bad parameter, type -h for help\n";
			return 1;
		}
	}
	}catch(...){
		cout<<"bad argument\n";
		return 1;
	}

	block_size *= 1024;

	if(limit == -1)
		limit = block_size*5;

	// cout<<infile<<" "<<outfile<<" "<<method<<" "<<limit<<" "<<coefficient<<"\n";


	ifstream fl(infile, ios::in | ios::binary);
	if (!fl.is_open()) {
		cout<<"Cannot open file: "<<infile<<"\n";
		return 1;
	}

	ofstream ofl(outfile, ios::out | ios::binary);
	if (!ofl.is_open()) {
		cout<<"Cannont create file: "<<outfile<<"\n";
		return 1;
	}
	// ofstream out_file(out_filename, ios::out | ios::binary);

	cout<<"Compressing...\n";

    fl.seekg( 0, ios::end );  

    size_t len = fl.tellg();
    char *ret = new char[block_size];  
    fl.seekg(0, ios::beg);
    // cout<<len<<"\n";

    int read = 0;

    int k = 0;

    int chunk_count = 0;

    // int cut = 100000; // wsadz to do argumentu

    int total_chunks = len/block_size + (len % block_size > 1);

    do {

     int to_read = min(block_size, int(len-read));
     read += to_read;

     vector<int> vec;
     fl.read(ret, to_read);

     for(int i=0; i<to_read; ++i) {
     	unsigned char temp = ret[i];
    	vec.push_back(temp);
    }
    	// compress_chunk_slp2_v3_bench(vec, coeff, method, crossing_type,
    	// lz77_size, grammar_size, grammar_size2,
    	// remaining_length, binary_seq_length, binary_grammar_length);
    	int crossing_type = 0;
    	if (method == 0 ) {
    		crossing_type = 1;
    	} else
    	if (method == 1) {
    		crossing_type = 2;
    	}

    	int compression_method = 2;
    	if(method < 3) {
    		compression_method = 1;
    	}

    	if(method == 4)
    		compression_method = 0;

		slpv3::slp2_v3 slp_compress(vec, coefficient, compression_method, crossing_type, limit);
		auto seq = slp_compress.get_remaining_sequence();
		int max_nonterm = slp_compress.get_max_nonterm();
		auto grammar = slp_compress.get_grammar();
		auto binary_grammar = grammar_binary_compressor::compress_grammar(grammar, seq);
		vector<bool> out;
		huffman_wrapper::huffman_with_dict(seq, max_nonterm, out);


		save_chunk(ofl, max_nonterm, binary_grammar, out);

		// ifstream check_file(outfile, ios::in | ios::binary);

		// int mx;
		// vector<bool> bg;
		// vector<bool> ot;

		// ofl.close();
		// read_chunk(check_file, mx, bg, ot);

		// if(mx == max_nonterm) {
		// 	cout<< mx << " " << max_nonterm<<"\n"; 
		// // }
		// cout<<bg[0]<<bg[1]<<bg[2]<<bg[3]<<"\n";
		// cout<<binary_grammar[0]<<binary_grammar[1]<<binary_grammar[2]<<binary_grammar[3]<<"\n";
		
		// if(bg == binary_grammar) {
		// 	cout<<"kkk\n";
		// }

		// if(out == ot){
		// 	cout<< "kkkk\n";
		// }



		// return 0;
     	cout << "Completed: " << ++chunk_count <<"/"<< total_chunks << endl;

	} while(read != len);
    delete[] ret;
    fl.close();
    ofl.close();

	return 0;
}