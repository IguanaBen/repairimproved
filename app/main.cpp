/*
Code for generating tests
*/

#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <chrono>
// #include "suffix_array.h"
#include "suffix_tree.h"
// #include "suffix_tree2.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v3.h"
// #include "compress.h"
#include "compress_benchmark.h"
// #include "third_party_lz77/third_party_lz77_wrapper.h"
#include "elias_coding.h"
#include "grammar_binary_compressor.h"
#include "grammar_seq_compressor.h"
#include "huffman_compression/huffman_wrapper.h"
// #include "compress_benchmark.h"

#define MAX_THREADS 20

using namespace std;

vector<int> to_vec(string str)
{
	vector<int> res;
	for(int i=0; i<str.size(); ++i)
		res.push_back(str[i]);
	return res;
}
  
vector<int> readFileToVector(const char *name)  
{  
    ifstream fl(name);
    fl.seekg( 0, ios::end );  
    size_t len = fl.tellg();  
    char *ret = new char[len];  
    fl.seekg(0, ios::beg);   
    fl.read(ret, len);  
    fl.close();

    vector<int> vec;
    for(int i=0; i<len; ++i)
    	vec.push_back(ret[i]);

    delete[] ret;
    cout<<vec.size()<<"\n";
    return vec;
}

void compressByChunk(const char *name) {
	// unsigned char x;
	// file.read(&x, 1);
	// cout<<static_cast<int>(x)<<endl;

	unsigned char x;
	ifstream file(name);
	// while(file.read(&x, 1)) {
		
	// }
}


inline pair<thread*, thread*> test_all(string filename, float step1, float iterations1, float step2, float iterations2) {

	string inpref = "test_files\\";
	string outpref = "RESULTS\\";
	string debug_outpref = "RESULTS_DEBUG\\";

	int block_size = 1024*1024;

	thread* t1 = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function0", debug_outpref + filename + ".debug.function0", 
		block_size, step1, step1, iterations1, 1, 0, 1); } );

	thread* t2 = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function1", debug_outpref + filename + ".debug.function1", 
		block_size, step2, step2, iterations2, 2, 0, 1); } );

	make_pair(t1, t2);
}

int main()
{

	// vector<string> files_to_test = {"bible.txt", "world192.txt" ,"english.50MB", "dna.50MB"};
	// vector<string> files_to_test = {"bible.txt", "world192.txt"};
	// vector<string> files_to_test = {"world192.txt"};
	// vector<string> files_to_test = {"special_chars_txt"};
	// vector<string> files_to_test = {"E.coli", "pitches.50MB", "dblp.xml.50MB", "book1", "book2", "news", "geo", 
						// "bible.txt", "world192.txt" ,"english.50MB", "dna.50MB", "proteins.50MB", "sources.50MB"};

	// vector<string> files_to_test = {"english.50MB"};
	// vector<string> files_to_test = {"web-Google.txt", "web-BerkStan.txt"};
	// vector<string> files_to_test = {"web-Google.txt", "english.50MB"};
	// vector<string> files_to_test = {"web-Google.txt"};
	// vector<string> files_to_test = {"osdb"};

	// vector<string> files_to_test = {"uk-2007"};
	// vector<string> files_to_test = {"dblp_graph", "dblp_graph_edges"};
	// vector<string> files_to_test = {"com-dblp.ungraph.txt"};

	// vector<string> files_to_test = {"small_graph_edges"};
	
	// vector<string> files_to_test = {"bible.txt", "world192.txt" ,"english.50MB", "dna.50MB", "proteins.50MB", "sources.50MB"};

	vector<string> files_to_test = {"english.50MB"};

	float start1 = 0.5f;
	float step1 = 0.5f;
	int iterations1 = 6;
	float start2 = 1.0f;
	float step2 = 0.5f;
	int iterations2 = 6;

	int block_size = 1024*1024;

	// auto thread_ptrs = test_all(files_to_test[0], step1, iterations1, step2, iterations2);

	string inpref = "test_files\\";
	string outpref = "RESULTS2\\";
	string debug_outpref = "RESULTS_DEBUG\\";

	// vector<thread*> threads(MAX_THREADS);

	// for(int i=0; i<files_to_test.size(); ++i) {
	// 	string filename = files_to_test[i];
	// 	threads[i] = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function1", debug_outpref + filename + ".debug.function1", 
	// 		block_size, -1.0f, step1, iterations1, 2, 0, 1); } );
	// }

	// for(int i=0; i<files_to_test.size(); ++i){
	// 	threads[i]->join();
	// }


	// int arr[] = {1, 2, 5};
	int arr[] = {10};

	for(int j = 0; j < 1; ++j) {


		string suf = "." + to_string(arr[j]) + "MB";

		vector<thread*> threads(MAX_THREADS);

		int bs = block_size*arr[j];

		for(int i=0; i<files_to_test.size(); ++i) {
			string filename = files_to_test[i];
			threads[2*i] = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function0" + suf, debug_outpref + filename + ".debug.function0" + suf, 
				bs, start1, step1, iterations1, 1, 0, 1); } );

			threads[2*i + 1] = new thread([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function1" + suf, debug_outpref + filename + ".debug.function1" + suf, 
				bs, start2, step2, iterations2, 2, 0, 1); } );

		}

		for(int i=0; i<files_to_test.size(); ++i){
			threads[2*i]->join();
			threads[2*i + 1]->join();

		}

	}




	return 0;

	// vector<unsigned int> txt;
	// for(int i=0; i<1000000; ++i)
		// txt.push_back(i%30);

	// vector<int> txt = readFileToVector("english.50MB");

	// vector<unsigned int> sa = build_suffix_arr
	// string str = "ababc";ay(txt);
	// string str = "ababcabcabca";
	// string str = "abcabcabcabca";
	// string str = "ababcabxabcazabvabcazabv";
	// string str = "abcaabcabca";
	// string str = "ababcabxabx";
	// string str = "abazab";
	// string str = "abazabaaba";
	// string str = "aaaabbccdd";



	// string str = "aaaaaaaaaaaabb";
	// string str = "abcd";
	// str += str;
	// str += str;
	// str += str;
	// str += str;
	// str += str;
	// str += str;
	// str += str;
	// str += str;
	// str += str;

	// 114 101 32 97 108 108 32 97
	// string str = "aaaaaaaaabaab";
	// // str += char(114);
	// // str += char(101);
	// // str += char(32);
	// // str += char(97);
	// // str += char(108); 
	// // str += char(108);
	// // str += char(32);
	// // str += char(97);
	// // cout<<str<<"\n";
	// // string str = "abbbabbbbb";
	// // str = "aaaaaaaaaaaabb";
	// vector<int> str_vec = to_vec(str);
	

	// slp1 sl1;
	// sl1.compress(str_vec, 1);
	// cout<<sl1.get_remaining_length()<<" < \n";
	// cout<<sl1.get_grammar_size()<<" < \n";

	// // cout<<"aa"<<endl;
	// vector<int> dec = sl1.decompress();

	// if(dec == str_vec) {
	// 	cout<<"KK\n";
	// } else {
	// 	cout<<"NO\n";
	// }

	// return 0;

	// return 0;
	// // string str = "abcabcabc";
	// // for(int i=0; i<2; ++i) {

		
	// 	vector<int> str_vec = to_vec(str);
	// 	slpv3::slp2_v3 slp_grammarv3(str_vec, 2.0f);

	// 	vector<int> dec_vec = slp_grammarv3.decompress();

	// 	for(int i=0; i < str_vec.size(); ++i)
	// 		cout<<str_vec[i]<<" ";
	// 	cout<<"\n";

	// 	for(int i=0; i < dec_vec.size(); ++i)
	// 		cout<<dec_vec[i]<<" ";
	// 	cout<<"\n";

	// int dd = 0;

	// cout<<(dd++) + 1<<"\n";
	vector<bool> arithm = {0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1};
	vector<bool> ott;
	int p,d;
	grammar_binary_compressor::compress_arithmethic(arithm, ott, p, d);
	cout<<arithm.size()<<" _ "<<ott.size()<<"\n";

	// return 0;

	vector<int> sq = {5, 6, 7, 7, 8, 10};
	int max_nonterm = 10;
	vector<bool> out;
	huffman_wrapper::huffman_with_dict(sq, max_nonterm, out);
	vector<int> new_sequence = huffman_wrapper::huffman_decode(max_nonterm, out);
	for(int i=0; i<new_sequence.size(); ++i){
		cout<<new_sequence[i]<<" ";
	}
	cout<<"\n";
	// return 0;

	map<int, pair<int, int>> grammar;

	grammar[10] = make_pair(1, 9);
	grammar[11] = make_pair(0, 4);
	grammar[12] = make_pair(11,10);
	grammar[13] = make_pair(12, 12);
	grammar[14] = make_pair(10, 12);

	vector<int> seq = {13};

	// grammar[10] = make_pair(1, 9);
	// grammar[11] = make_pair(0, 4);
	// grammar[12] = make_pair(10, 11);
	// // grammar[13] = make_pair(0, 12);
	// // grammar[14] = make_pair(13, 13);
	// // grammar[15] = make_pair(0,0);
	// grammar[13] = make_pair(0,10);

	// vector<int> seq = {13};
	// grammar[make_pair(1, 1)] = 4;
	// grammar[make_pair(0, 0)] = 5;
	// grammar[make_pair(0, 5)] = 6;
	// grammar[make_pair(0, 6)] = 7;
	// grammar[make_pair(6, 7)] = 8;


	// cout<<grammar_binary_compressor::log2ceil(0)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(1)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(2)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(3)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(4)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(7)<<"\n";
	// cout<<grammar_binary_compressor::log2ceil(8)<<"\n";

	// auto vec = grammar_binary_compressor::to_binary(4, 8);

	// for(int i=0; i<vec.size(); ++i)
	// 	cout<<vec[i];
	// cout<<"\n";


	// return 0;

	//binary code dla prawych, nie elias, wiemy ile potrzebujemy miejsca

	//KODUJ ROZNICE W HUFFMANIE !!!!!
	//moze bedzie lepiej

	// for(int i=0; i<seq.size(); ++i){
	// 	cout<<seq[i]<<" ";
	// }
	// cout<<"\n";


	auto res = grammar_binary_compressor::compress_grammar(grammar, seq);
	// for(int i=0; i<10; ++i) {
	// 	// cout<<res[i];
	// }
	// cout<<"\n";

	for(int i=0; i<seq.size(); ++i){
		cout<<seq[i]<<" ";
	}
	cout<<"\n";

	auto dec_grammar = grammar_binary_compressor::decompress(res);

	auto dec_txt = grammar_seq_compressor::decompress(dec_grammar, seq);
	for(int i=0; i<dec_txt.size(); ++i){
		cout<<dec_txt[i]<<" ";
	}
	cout<<"\n";

	// cout<<grammar_binary_compressor::grammar_eq(grammar, dec_grammar)<<" TSSS\n";
	
	// return 0;

	// 	// cout<<"AA\n";
	// }

	// for(int i=1; i<10; ++i){
	// 	// int i = 1;
	// 	auto bb = elias_coding::encode(i);
	// 	for(int j=0; j<bb.size(); ++j){
	// 		cout<<bb[j];
	// 	}
	// 	cout<<"\n";
	// }
	// return 0;

	// compress_benchmark::benchmark("english.50MB", 10, 1.0f, 5.0f, 20);	
	// compress_benchmark::benchmark("english.50MB", 1024*1024, 1.0f, 5.0f, 20);
	// compress_benchmark::benchmark("dna.50MB", 10, 2.0f, 2.0f, 1);	
	// compress_benchmark::benchmark("dna.50MB", "dna.results.1", "dna.debug.1", 1024*1024, 0.0f, 0.2f, 50);
	// compress_benchmark::benchmark("english.50MB", 1024*1024, 0.0f, 0.2f, 50);
	
	// compress_benchmark::benchmark("dna.50MB", 1024*1024, 3.0f, 3.0f, 1);
	// compress_benchmark::benchmark("dna.50MB", 1024*1024, 1.0f + 4.0f + 4.0f, 5.0f + 4.0f + 4.0f, 20);
	// compress_benchmark::benchmark("dna.50MB", 1024*1024, 2.8f, 3.4f, 3);

	// compress_benchmark::benchmark("dna.50MB", "res\\dna.results.1", "res\\dna.debug.1", 1024*1024, 0.0f, 0.2f, 50);

	// compress_benchmark::benchmark("dna.50MB", "res\\temp_out1", "res\\temp_debug1", 1024*1024, 2.0f, 0.2f, 1);
	// return 0;

	// thread tdna1([] () {compress_benchmark::benchmark("english.50MB", "resv2\\test_rse", "resv2\\test_debug", 1024*1024, 0.5f, 0.2f, 1); } );

		// string str = "aaaaaaaaaaaa";
	// string str = "abcabc";

	// string str = "abxzzzzzbxvvzzz";
	// string str = "xbxxxvxxxb";

	// // string str = "abcdabcd";
	// auto vec1 = to_vec(str);

	// vector<unsigned int> factors, factor_starts;

	// lz77_wrapper::lz77(vec1, factors, factor_starts);

	// return 0;
	// for(int i=0; i < factors.size(); ++i)
	// 	cout<<factors[i]<<"| ";
	// cout<<"\n";
	// suffix_tree st;
	// vector<unsigned int> factors;
	// vector<unsigned int> factor_starts;
	// st.lz77(vec1, factors, factor_starts);
	// st.super_factor_log(vec1, factors, factor_starts, 50);

	// // return 0;

	// // cout<<"WW\n";
	// for(int i=0; i < factors.size(); ++i)
	// 	cout<<factors[i]<<"| ";
	// // cout<<"\n";
	// cout<<factors[factors.size()-1];
	// cout<<str[0]<<"|";
	// for(int i=1; i<factors.size(); ++i)
	// {	
	// 	// cout<<"n";
	// 	for(int j=factors[i-1]; j<factors[i]; ++j)
	// 		cout<<str[j];
	// 	cout<<"|";
	// }
	// // cout<<"\n";

	// return 0;


// 2 + 2 + 2
// 3 + 3 = 6 


	//check:


	//<================XcacXzazX
	// string test_str = "abcacabzazab";
 	
	// auto data = to_vec(test_str);
	// slpv3::slp2_v3 slp_compress(data, 0.2f);

	// return 0;


	/// TAKZE dlz77

	// i zobacz co sie dzieje dla mniejszych blokow
	// return 0;


	//////

	//average length < 
	// cout<<"like, really?\n";

	// string pref = "rresults\\res_9.04_mac\\";
	// string inpref = "test_files\\";
	// string outpref = "rresults\\res_test\\";
	// string pref2 = "rresults\\res_07.04_of\\";

	// string filename = "dblp.xml.50MB";
	// string filename = "sources.50MB";

	// string filename = "Escherichia_Coli";
	string filename = "bible.txt";

	int iters = 10;
	// int block_size = 1024*1024;
	// string filename = "world192.txt";

	//zamienia

	float step = 0.05;

	thread tdna1([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function0", outpref + filename + ".debug.function0", 
		block_size, 0.2f, 0.2f, iters, 1, 0, 1); } );
	// thread tdna1([=] () {compress_benchmark::benchmark(inpref + filename, outpref + filename + ".results.function0", outpref + filename + ".debug.function0", 
	// 	block_size, 0.2f, 0.2f, iters, 1, 0, 1); } );

	// thread tdna2([=] () {compress_benchmark::benchmark(filename, pref + filename + ".results.02", pref + filename + ".debug.02", block_size, 2.0f, 0.2f, iters); } );

	// thread t3([=] () {compress_benchmark::benchmark(filename, pref + filename + ".results.02", pref + filename + ".debug.02", block_size, 1.0f, 0.1f, iters); } );
	// thread t4([=] () {compress_benchmark::benchmark(filename, pref + filename + ".results.02", pref + filename + ".debug.02", block_size, 1.0f, 0.1f, iters); } );


	// thread tdna1([=] () {compress_benchmark::benchmark("proteins.50MB", pref + "proteins.results.01", pref + "proteins.debug.01", block_size, 1.0f, 0.1f, iters); } );
	// thread tdna2([=] () {compress_benchmark::benchmark("proteins.50MB", pref + "proteins.results.02", pref + "proteins.debug.02", block_size, 2.00f, 0.1f, iters); });

	// thread teng1([=] () {compress_benchmark::benchmark("dna.50MB", pref + "dna.results.01", pref + "dna.debug.01", block_size, 0.0f, 0.1f, iters); } );
	// thread teng2([=] () {compress_benchmark::benchmark("dna.50MB", pref + "dna.results.02", pref + "dna.debug.02", block_size, 2.00f, 0.1f, iters); });

	// thread teng1([] () {compress_benchmark::benchmark("english.50MB", "res\\english.results.1.2", "res\\english.debug.1.2", block_size, 0.0f, 0.2f, 10); });
	// thread teng2([] () {compress_benchmark::benchmark("english.50MB", "res\\english.results.2.2", "res\\english.debug.2.2", block_size, 2.0f, 0.2f, 10); });

	tdna1.join();
	// tdna2.join();
	// teng1.join();
	// teng2.join();

	return 0;

	// string str = "abcabcda";
	// compress_slp1("english.50MB", "english.50MB.compressed", 5*1024*1024);
	// compress("english.50MB", "english.50MB.compressed", 1024*1024, 1);
	// compress("english.50MB", "english.50MB.compressed", 1024*1024, 2);


	// return 0;
	// compress("english.50MB", "english.50MB.compressed", 100*1024, 0);
	// compress_slp1("pitches.50MB", "pitches.50MB.compressed", 1024*1024);
	// compress_slp1("retext", "retext.compressed", 1024*1024);
	
	// vector<int> txt = to_vec(str);
	// vector<int> txt = readFileToVector("slp1.cpp");
	// vector<int> txt = readFileToVector("english.50MB");


	// slp1 sl;
	// sl.compress(txt);

	// slp2 slp2_compress;
	// slp2_compress.compress(txt);
	// slp1_compress.compress2(data);
	// vector<unsigned int> compressed_grammar_uint = slp2_compress.get_grammar();

	// return 0;

	// // suffix_tree st(vec1);
	// // suffix_tree st2;
	// // st2.build_tree(vec1);

	// suffix_tree st;
	// // suffix_tree st2;
	// // st2.build_tree(vec1);
	// vector<unsigned int> factors;
	// vector<unsigned int> factor_starts;
	// st.lz77(vec1, factors, factor_starts);
	// // return 0;
	// for(int i=0; i < factors.size(); ++i)
	// 	cout<<factors[i]<<"|"<<factor_starts[i]<<" ";
	// cout<<"\n";

	// cout<<str[0]<<"|";
	// for(int i=1; i<factors.size(); ++i)
	// {	
	// 	// cout<<"n";
	// 	for(int j=factors[i-1]; j<factors[i]; ++j)
	// 		cout<<str[j];
	// 	cout<<"|";
	// }
	// cout<<"\n";
	
	// slp1 sl;
	// sl.compress(vec1);

	// return 0;
}