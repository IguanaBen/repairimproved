#include "suffix_tree.h"

#include <iostream>

suffix_tree::suffix_tree()
{
	// text = _text;
	// root = new suffix_tree_node;
	// root->slink = root;

	// suffix_tree_node* active_node = root;
	// unsigned int active_len = 0;
	// int active_char = 0;
	// int t_index = 0;

	// suffix_tree_node* last_added = NULL;

	// for(unsigned int i=0; i<text.size(); ++i)
	// {
	// 	// cout<<"Adding: "<<i<<" "<<char(text[i])<<"\n";
	// 	add_letter(active_node, last_added, active_len, active_char, t_index, text[i], i);
	// }
}

void suffix_tree::build_tree(vector<int> &_text)
{
	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;

	for(unsigned int i=0; i<text.size(); ++i)
	{
		// cout<<"Adding: "<<i<<" "<<char(text[i])<<"\n";
		add_letter(active_node, last_added, active_len, active_char, t_index, i);
	}
	// cout<<"T2: "<<root->children['a']->children['b']->children['c']->children['z']->edge_length<<"\n";
	// cout<<"T: "<<root->children['a']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->text_ind<<"\n";
	// cout<<"T: "<<root->children['a']->children['b']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->children['b']->text_ind<<"\n";
	// cout<<"T: "<<root->children['a']->children['b']->children['x']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->children['b']->children['x']->text_ind<<"\n";
}

void suffix_tree::lz77(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts)
{
	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;

	unsigned int i = 0;
	vector<unsigned int> factor_positions;
	vector<unsigned int> factor_starts;
	// cout<<"FACTORS:\n";
	while(i < text.size())
	{
		// cout<<"enter\n";
		// if(i % 100000 == 0) cout<<i<<" nodes added \n";
		pair<int, int> mf = max_factor(i, _text);
		// cout<<mf.second<<"\n";

		if(mf.second == 0)
		{
			add_letter(active_node, last_added, active_len, active_char, t_index, i);
			++i;
			// cout<<1<<"\n";
		}
		else
		{

			// int pw2 = 1;
			// while(pw2*2 <= mf.second)
			// 	pw2*=2;

			// if(i < _text.size()/4)
			// mf.second = 1;

			for(int j=0; j < mf.second; ++j)
			{
				// cout<<"adding: "<<i+j<<"\n";
				add_letter(active_node, last_added, active_len, active_char, t_index, i+j);
			}
			i += mf.second;
			// cout<<mf.second<<"\n";
		}
		// std::cout<<i<<" "<<mf.second<<"\n";
		factor_positions.push_back(i);
		factor_starts.push_back(mf.first);
	}
	// factor_starts.push_back(_text.size());
	// cout<<"end\n";
	f_positions = factor_positions;
	f_starts = factor_starts;
	// cout<<"T: "<<root->children['a']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->text_ind<<"\n";

	// suffix_tree_node* node = root->children['a'];
	// map<int, suffix_tree_node*>::iterator it;
	// for(it = node->children.begin(); it != node->children.end(); ++it)
		// del_tree(it->second);
		// cout<<it->first<<"|\n";
	// cout<<"T: "<<root->children['a']->children['x']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->children['x']->text_ind<<"\n";
	// cout<<"T: "<<root->children['a']->children['x']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->children['x']->text_ind<<"\n";

	// cout<<"T: "<<root->children['a']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->text_ind<<"\n";
	// cout<<"T: "<<root->children['a']->children['x']->edge_length<<"\n";
	// cout<<"I: "<<root->children['a']->children['x']->text_ind<<"\n";

	// cout<<"T: "<<root->children['a']->children['b']->edge_length<<"\n";

	// cout<<"T: "<<root->children['a']->children['b']->children['a']->text_ind<<"\n";
}

void suffix_tree::super_factor_log(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts, int lookback)
{

	vector<float> cost;
	for(int i=0; i < _text.size(); ++i) {
		// cost.push_back(std::numeric_limits<float>::lowest() + 0.5f);
		// cost.push_back(-float(_text.size())-5.0f);
		cost.push_back(float(_text.size())*float(_text.size()) + 5.0f);
	}

	vector<int> last_length;

	for(int i=0; i < _text.size(); ++i) {
		last_length.push_back(1);
	}

	vector<float> lengths_costs;

	lengths_costs.push_back(300.0f);
	lengths_costs.push_back(300.0f);
	lengths_costs[0] = 1.0f;
	lengths_costs[1] = 1.0f;

	// int special = 5;
	// for(int i=2; i < special; ++i) {
	// 	lengths_costs.push_back(log2(i));
	// 	// lengths_costs.push_back(float(i));
	// }

	float avg = 5.0f;

	for(int i=2; i < _text.size(); ++i) {
		unsigned int x = i;
		// if((x != 0) && ((x & (~x + 1)) == x))
			// lengths_costs.push_back(-i*i);
		// else
		// lengths_costs.push_back( -(log2(i) + 2) );
		// lengths_costs.push_back(abs(avg - i));
		// lengths_costs.push_back(1.0f);
		if(i % 2 )
			lengths_costs.push_back(1.0f + 0.5f);
		else
			lengths_costs.push_back(1.0f);

	}

	// lengths_costs[2] = 100.0f;
	// lengths_costs[3] = 50.0f;
	// lengths_costs[4] = 25.0f;
	// lengths_costs[5] = 12.5f;




	cost[0] = lengths_costs[1];
	// last_length[0] = 1;

	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;

	// unsigned int i = 0;
	vector<unsigned int> factor_positions;
	vector<unsigned int> factor_starts;


	add_letter(active_node, last_added, active_len, active_char, t_index, 0);

	int k = 0;
	auto mf = max_factor(k, _text);
	for(int i=1; i < _text.size(); ++i) {
		// auto mf = max_factor(i, _text);
		// cout<<mf.second<<" | xxx\n";
		int mxl = max(1,i-lookback-1);
		int max_match = 0;
		for(int j=mxl; j < i; ++j) {
			auto mf = max_factor(j, _text);
			// cout<<i<<" "<<j<<" "<<mf.second<<"\n";
			if( mf.second >= (i-j+1) ){
				max_match = i-j+1;
				break;
			}
		}
		
		cost[i] = cost[i-1] + lengths_costs[1];
		// int best_start = i;
		for(int l = 0; l<max_match; ++l) {
			// cout<<cost[i]<<" "<<cost[i-l-1]<<" "<<i-l-1<<" check\n";
			// if ((cost[i] <= lengths_costs[l+1] + cost[i-l-1])) {
			if ((cost[i] > lengths_costs[l+1] + cost[i-l-1])) {
				cost[i] =  lengths_costs[l+1] + cost[i-l-1];
				last_length[i] = l+1;
			}
		}
		// cout<<i<<" "<<max_match<<"\n";
		// cout<<cost[i]<<" "<<last_length[i]<<" "<<"_\n\n";
		add_letter(active_node, last_added, active_len, active_char, t_index, i);
	}
	// vector<int> f_positions;
	int prev = _text.size() - last_length[_text.size()-1];


	//3.6????

	// 0 1 2 3 4 5 6 7
	// return;
	// cout<<"XX\n";
	//check corner cases
	// cout<<prev<<" <<P \n";
	while(prev != 0) {
		factor_positions.push_back(prev);
		// cout<<prev<<" "<<last_length[prev]<<"\n";	
		prev -= last_length[prev-1];
	}
	// factor_positions.push_back(0);
	reverse(factor_positions.begin(), factor_positions.end());

	factor_positions.push_back(_text.size());

	// for(int i=0; i<factor_positions.size(); ++i)
	// 	cout<<factor_positions[i]<<" _ ";
	// cout<<"\n";

	//reverse vector r_f
	// factor_start
	// for(int i = 0; i< r_f)

	// while(i < text.size())
	// {
	// 	pair<int, int> mf = max_factor(i, _text);

	// 	if(mf.second == 0)`
	// 	{
	// 		add_letter(active_node, last_added, active_len, active_char, t_index, i);
	// 		++i;
	// 	}
	// 	else
	// 	{
	// 		for(int j=0; j < mf.second; ++j)
	// 		{
	// 			add_letter(active_node, last_added, active_len, active_char, t_index, i+j);
	// 		}
	// 		i += mf.second;
	// 	}
	// 	factor_positions.push_back(i);
	// 	factor_starts.push_back(mf.first);
	// }
	f_positions = factor_positions;
	f_starts = factor_starts;
}

int suffix_tree::match_factor(int j, int i){

// 	suffix_tree_node* current_node = root;
// 	int matched_length = 0;

// 	suffix_tree_node* current_node = root;
// 	int current_edge_length = 0;

// 	while(j != i){
// 		if(current_node->children.find(_text[j + matched_length]) == current_node->children.end()) {
// 			// current_node = current_node->slink;

// 			j++;
// 		} else {
// 			suffix_tree_node* next_node = current_node->children[_text[j+matched_length]];
// 			unsigned int e_len = current_node->children[_text[i+factor_length]]->edge_length;
// 			unsigned int s_pos = current_node->children[_text[i+factor_length]]->text_ind;
// 			int dif = 0;
// 			while(j != i && dif < e_len) {

// 			}
// 		}
// 	}

	return -1;//todo
}

pair<int, int> suffix_tree::max_factor(int i, vector<int> &_text)
{
	// cout<<".."<<i<<"\n";
	suffix_tree_node* current_node = root;
	int factor_length = 0;
	int factor_start = i;
	int offset = 0;
	int j = 0;

	// if (current_node->children[_text[i]] == 0) {
	// 	return make_pair(factor_start, factor_length);
	// }

	do
	{
		if (current_node->children.find(_text[i+factor_length]) == current_node->children.end()) {
			// cout<<_text[i+factor_length]<<" "<<j<<" "<<"br\n";
			// cout<<"Breaking\n";
			break;
		}
		else
		{	
			// cout<<"enter\n";
			suffix_tree_node* next_node = current_node->children[_text[i+factor_length]];
			unsigned int e_len = current_node->children[_text[i+factor_length]]->edge_length;
			unsigned int s_pos = current_node->children[_text[i+factor_length]]->text_ind;
			int mxl = 0;
			bool do_break = false;
			factor_start = s_pos - factor_length;
			// cout<<"w: "<< min(min(e_len, i), int(_text.size()))<<" "<<s_pos<<" "<<e_len<<"\n";
		 	unsigned int text_len = _text.size();
			// for(int j=0; j < min(min(e_len, (unsigned int)i),  text_len) && s_pos+j < i; ++j)
		 	j = factor_length;
		 	// cout<<s_pos<<" "<<i<<" "<<j<<"_\n";
		 	// cout<<i+j<<" "<<text_len<<" ; ; \n";
		 	int new_factor_length = factor_length;
			// while( s_pos + j < i && i+j < text_len && j < s_pos + e_len )
			while( s_pos + j-factor_length < i && i+j < text_len && j < s_pos + e_len && j-factor_length < e_len )
			{
				// cout<<s_pos+j-factor_length<<" + "<<i+j<<" + "<<e_len<<"\n";
				if(_text[s_pos+j-factor_length] == _text[i+j])
				{
					// cout<<"pr "<<s_pos+j<<" "<<i+j<<"\n";	
					// factor_length++;
					new_factor_length++;
				}
				else
				{
					do_break = true;
					break;
				}
				// cout<<_text[s_pos+j]<<" | "<<" "<<"\n";
				++j;
			}
			// cout<<"tLen: "<<i+j<<" "<<text_len<<"\n";
			factor_length = new_factor_length;
			// cout<<"br"<<" "<<do_break<<"\n";
			// cout<<factor_length<<" \n";

			if(do_break || e_len == inf || i+j >= text_len)
			{
				// cout<<"br\n";
				break;
			}
			else
			{
				// cout<<_text[i+factor_length]<<"\n";
				// if (current_node->children[_text[i+factor_length]] == 0) {
				// 	break;
				// }

				current_node = next_node;
				// cout<<"NN: "<<next_node->children[i+factor_length]->text_ind<<"\n";
				// j = factor_length;
			}
		}

	}while(true);
	// cout<<factor_length<<" Br\n";
	return make_pair(factor_start, factor_length);
}

void suffix_tree::lz77_p2(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts)
{
	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;

	unsigned int i = 0;
	vector<unsigned int> factor_positions;
	vector<unsigned int> factor_starts;
	while(i < text.size())
	{
		// cout<<"enter\n";
		// if(i % 100000 == 0) cout<<i<<" nodes added \n";
		pair<int, int> mf = max_factor_p2(i, _text);
		// cout<<mf.second<<"\n";

		if(mf.second == 0)
		{
			add_letter(active_node, last_added, active_len, active_char, t_index, i);
			++i;
		}
		else
		{
			for(int j=0; j < mf.second; ++j)
			{
				// cout<<"adding: "<<i+j<<"\n";
				add_letter(active_node, last_added, active_len, active_char, t_index, i+j);
			}
			i += mf.second;
		}
		// std::cout<<i<<" "<<mf.second<<"\n";
		factor_positions.push_back(i);
		factor_starts.push_back(mf.first);
	}
	// cout<<"end\n";
	f_positions = factor_positions;
	f_starts = factor_starts;
}

pair<int, int> suffix_tree::max_factor_p2(int i, vector<int> &_text)
{
	// cout<<".."<<i<<"\n";
	suffix_tree_node* current_node = root;
	int factor_length = 0;
	int factor_start = i;
	int offset = 0;
	int j = 0;

	// if (current_node->children[_text[i]] == 0) {
	// 	return make_pair(factor_start, factor_length);
	// }

	do
	{
		if (current_node->children.find(_text[i+factor_length]) == current_node->children.end()) {
			// cout<<_text[i+factor_length]<<" "<<j<<" "<<"br\n";
			break;
		}
		else
		{	
			// cout<<"enter\n";
			suffix_tree_node* next_node = current_node->children[_text[i+factor_length]];
			unsigned int e_len = current_node->children[_text[i+factor_length]]->edge_length;
			unsigned int s_pos = current_node->children[_text[i+factor_length]]->text_ind;
			int mxl = 0;
			bool do_break = false;
			factor_start = s_pos - factor_length;
			// cout<<"w: "<< min(min(e_len, i), int(_text.size()))<<" "<<s_pos<<" "<<e_len<<"\n";
		 	unsigned int text_len = _text.size();
			// for(int j=0; j < min(min(e_len, (unsigned int)i),  text_len) && s_pos+j < i; ++j)
		 	j = factor_length;
		 	// cout<<s_pos<<" "<<i<<" "<<j<<"_\n";
		 	// cout<<i+j<<" "<<text_len<<" ; ; \n";
		 	int new_factor_length = factor_length;
			// while( s_pos + j < i && i+j < text_len && j < s_pos + e_len )
			while( s_pos + j-factor_length < i && i+j < text_len && j < s_pos + e_len && j-factor_length < e_len )
			{
				// cout<<s_pos+j-factor_length<<" + "<<i+j<<" + "<<e_len<<"\n";
				if(_text[s_pos+j-factor_length] == _text[i+j])
				{
					// cout<<"pr "<<s_pos+j<<" "<<i+j<<"\n";	
					// factor_length++;
					new_factor_length++;
				}
				else
				{
					do_break = true;
					break;
				}
				// cout<<_text[s_pos+j]<<" | "<<" "<<"\n";
				++j;
			}
			// cout<<"tLen: "<<i+j<<" "<<text_len<<"\n";
			factor_length = new_factor_length;
			// cout<<"br"<<" "<<do_break<<"\n";
			if(do_break || e_len == inf || i+j >= text_len)
			{
				break;
			}
			else
			{
				current_node = next_node;
			}
		}

	}while(true);

	if(factor_length == 0)
		return make_pair(factor_start, factor_length);

	int pow2 = 1;
	while(pow2*2 <= factor_length)
		pow2*=2;

	return make_pair(factor_start, pow2);
}

void suffix_tree::add_letter(
	suffix_tree_node *&active_node,
	suffix_tree_node *&last_added,
	unsigned int &active_len,
	int &active_char,
	int &t_index,
	unsigned int i)
{

		bool cont = true;
		do
		{	
			if(active_len == 0)
			{
				if(active_node->children[text[i]] == 0)
				{
					suffix_tree_node* new_leaf = new suffix_tree_node;
					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;
					active_node->children[text[i]] = new_leaf;

					active_char = text[i];

					if(last_added != NULL)
						last_added->slink = active_node;

					if(active_node == root)
						cont = false;
					else
						active_node = active_node->slink;

					last_added = NULL;
				}
				else
				{
					active_len++;

					if(last_added != NULL)
						last_added->slink = active_node;

					active_char = text[i];

					if(active_node->children[text[i]]->edge_length == 1)
					{
						active_node = active_node->children[text[i]];
						active_len = 0;
					}

					last_added = NULL;
					cont = false;
				}
			}
			else
			{
				unsigned int index = active_node->children[active_char]->text_ind + active_len;
				if( text[i] == text[index])
				{
					active_len++;

					if(active_node->children[active_char]->edge_length == active_len)
					{
						active_node = active_node->children[active_char];
						active_len = 0;
						active_char = text[i];

						if(last_added != NULL)
							last_added->slink = active_node;	//innego przypadku nie moze byc gdy text[i] == text[index]
					}

					last_added = NULL;
					cont = false;
				}
				else
				{

					suffix_tree_node* child = active_node->children[active_char];
					// cout<<active_len<<" "<<active_node->children.size()<<" "<<child<<"|\n";
					suffix_tree_node* new_leaf = new suffix_tree_node;
					suffix_tree_node* new_node = new suffix_tree_node;

					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;

					new_node->text_ind = child->text_ind;
					new_node->edge_length = active_len;

					child->text_ind = child->text_ind + active_len;
					if(child->edge_length != inf)
						child->edge_length -= active_len;

					active_node->children[active_char] = new_node;

					new_node->children[text[i]] =  new_leaf;
					new_node->children[text[index]] = child;

					if(last_added != NULL)
						last_added->slink = new_node;


					last_added = new_node;

					unsigned int t_index;
					if(active_node == root)
					{
						if(active_len == 1)
						{
							new_node->slink = root;
							last_added = NULL;
						}

						active_len--;

						active_char = text[ new_node->text_ind + 1];
						t_index = new_node->text_ind + 1;
					}
					else
					{
						last_added = new_node;
						t_index = active_node->children[active_char]->text_ind;
					}

					active_node = active_node->slink;
					// cout<<active_len<<" "<<active_char<<" "<<active_node->children[active_char]<<"\n";
					// cout<<"a\n";
					while(active_len > 0 && active_node->children[active_char]->edge_length <= active_len)
					{
						// cout<<active_len<<" "<<active_node->children[active_char]->edge_length<<"\n";
						// cout<<"changing\n";
						unsigned int e_length = active_node->children[active_char]->edge_length;
						active_len -= e_length;
						active_node = active_node->children[active_char];
						// cout<<active_node->children.size()<<" "<<active_char<<"\n";

						// map<int, suffix_tree_node*>::iterator it;
						// for(it = active_node->children.begin(); it != active_node->children.end(); ++it)
						// 	cout<<it->first<<" ";
						// cout<<"\n";

						// cout<<"b\n";
						// cout<<char(active_char)<<"\n";

						t_index += e_length;
						if(active_len != 0)
							active_char = text[t_index];
						// cout<<char(active_char)<<"\n";
						// cout<<"e\n";
					}
					// cout<<"Overdiri\n";
					
					// cout<<active_len<<"\n";

					// if(active_node == )

				}
			}

		}while(cont);

}

vector<unsigned int> suffix_tree::make_array()
{
	vector<unsigned int> result;
	result.resize(text.size());

	unsigned int cnt = 0;
	traverse(root, result, cnt, 0);

	return result;
}

void suffix_tree::del_tree(suffix_tree_node* node)
{
	map<int, suffix_tree_node*>::iterator it;
	for(it = node->children.begin(); it != node->children.end(); ++it)
		del_tree(it->second);

	delete node;
}

suffix_tree::~suffix_tree()
{
	// cout<<"deleting tree...\n";
	del_tree(root);
	// cout<<"deleted tree\n";
}

void suffix_tree::traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth)
{
	if(node->children.size() == 0)
		vec[cnt++] = text.size() - depth;
	else
	{
		  map<int, suffix_tree_node*>::iterator it;
		  for(it = node->children.begin(); it != node->children.end(); ++it)
		  {
		  	if(it->second->edge_length != inf)
		  	{
				// cout<<"depth: "<<depth
				// 	<<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + it->second->edge_length);
		  	}
		  	else
		  	{
				// cout<<"depth: "<<depth
					// <<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + (text.size() - it->second->text_ind));
		  	}
		  }
	}

	// cout<<"UP\n";
}