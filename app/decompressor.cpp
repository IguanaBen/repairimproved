#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <chrono>
// #include "suffix_array.h"
#include "suffix_tree.h"
// #include "suffix_tree2.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v3.h"
// #include "compress.h"
#include "compress_benchmark.h"
// #include "third_party_lz77/third_party_lz77_wrapper.h"
#include "elias_coding.h"
#include "grammar_binary_compressor.h"
#include "grammar_seq_compressor.h"
#include "huffman_compression/huffman_wrapper.h"

void read_bool_vector( ifstream &file, unsigned int size, vector<bool> &vec ){

	unsigned int current_val = 0;
	unsigned int mask = (1<<31);
	unsigned int cmask = mask;

	vec.clear();

	file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
	// cout<<current_val<<"\n";
	// return;
	// cout<<current_val<<" cv\n";

	for(int i=0; i<size; ++i) {

		vec.push_back(cmask & current_val);
		// cout<<"B: "<<cmask && current_val<<"\n";
		cmask >>= 1;
		if(cmask == 0 ) {
			if(i + 1 < size) {

				file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
				// cout<<current_val<<"\n";
				// return;
				cmask = mask;
			}

		}

	}

}

void read_chunk(ifstream &file, int &max_nonterm, vector<bool> &binary_grammar, vector<bool> &out) {
	unsigned int mx_nt;
	unsigned int size_grammar;
	unsigned int size_out;
	
	file.read(reinterpret_cast<char*>(&mx_nt), sizeof(mx_nt));
	file.read(reinterpret_cast<char*>(&size_grammar), sizeof(size_grammar));
	file.read(reinterpret_cast<char*>(&size_out), sizeof(size_out));


	// unsigned int ss;
	// file.read(reinterpret_cast<char*>(&ss), sizeof(ss));

	// cout<< mx_nt <<" "<<size_grammar<<" "<<size_out<<"\n";


	binary_grammar.clear();
	out.clear();
	read_bool_vector(file, size_grammar, binary_grammar);
	read_bool_vector(file, size_out, out);


	max_nonterm = mx_nt;
}

int main(int argc, char** argv) {



	if(argc != 3) {
		cout<<"Usage: decompress [infile] [outfile]\n";
		return 1;
	}

	string infile = string(argv[1]);
	string outfile = string(argv[2]);

	ifstream fl(infile, ios::in | ios::binary);
	ofstream ofl(outfile, ios::out | ios::binary);

	if (!fl.is_open()) {
		cout<<"Cannot open file: "<<infile<<"\n";
		return 1;
	}

	if (!ofl.is_open()) {
		cout<<"Cannont create file: "<<outfile<<"\n";
		return 1;
	}
	cout<<"Decompressing...\n";
	while(fl.peek() != EOF) {
		int max_nonterm;
		vector<bool> grammar;
		vector<bool> str;
		read_chunk(fl, max_nonterm, grammar, str);

		auto dec_grammar = grammar_binary_compressor::decompress(grammar);
		vector<int> new_sequence = huffman_wrapper::huffman_decode(max_nonterm, str);
		auto data = grammar_seq_compressor::decompress(dec_grammar, new_sequence);

		for (int i=0; i<data.size(); ++i) {
			char c = data[i];
			ofl.write(reinterpret_cast<char*>(&c), sizeof(c));
		}

	}
	cout<<"Done\n";
	fl.close();
	ofl.close();

	return 0;
}