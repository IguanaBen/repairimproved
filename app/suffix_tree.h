#ifndef SUFFIXTREE_H
#define SUFFIXTREE_H

#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <unordered_map>

using namespace std;


struct suffix_tree_node
{
	// list< pair<int, suffix_tree_node*> > children;
	map<int, suffix_tree_node*> children;
	suffix_tree_node* slink;	//leafs may not have slinks
	unsigned int text_ind; //starting position of substring represented
						   // by this node
	unsigned int edge_length;   // length of the edge=(parent, this_node)
};

class suffix_tree
{
	public:
		const unsigned int inf = 2147483647;
		vector<int> text;
		suffix_tree_node* root;
		void traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth);
		void del_tree(suffix_tree_node* node);
		void add_letter(
			suffix_tree_node *&active_node,
			suffix_tree_node *&last_added,			
			unsigned int &active_len,
			int &active_char,
			int &t_index,
			unsigned int i);
		pair<int, int> max_factor(int i, vector<int> &_text);
		pair<int, int> max_factor_p2(int i, vector<int> &_text);


		// suffix_tree_node* find_active_node(suffix_tree_node *active_node, 
		// 					unsigned int active_len, int active_char);
		// pair<suffix_tree_node<charType>*, int> find_slink(pair<suffix_tree_node<charType>*, int> node);

	public:
		suffix_tree();
		void build_tree(vector<int> &_text);
		void lz77(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts); //non self-referencing
		void lz77_p2(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts); //non self-referencing
		void super_factor_log(vector<int> &_text, vector<unsigned int> &f_positions, vector<unsigned int> &f_starts, int lookback); //non self-referencing
		int match_factor(int j, int i);

		~suffix_tree();
		vector<unsigned int> make_array();
};

#endif
