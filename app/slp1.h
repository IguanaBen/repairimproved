#ifndef SLP1_H
#define SLP1_H

#include <vector>
#include <map>
#include <algorithm>
// #include "huffman.h"
using std::vector;
using namespace std;
class slp1
{

	private:
		int lz77_size;
		int len;
		vector<int> result_txt;
		map<pair<int, int>, int> nonterms;
		map<pair<int, int>, int> nonterms_count;
		map<int, pair<int, int> > r_nonterms;
		vector<pair<int, int>> nonterm_list;
		int next_nonterm = 0;
		void split(int i, vector<int> &start, vector<int> &end);
		void preproc(int len, vector<int> &start, vector<int> &end);
		void pairing(int len, vector<int> &start, vector<int> &end, vector<int> &pair);
		void pairing2(int len, vector<int> &txt, vector<int> &start, vector<int> &end, vector<int> &pair);
		void replacement(int &len, vector<int> &txt, vector<int> &start, vector<int> &end, vector<int> &pair);
		void print_state(int len, vector<int> &txt, vector<int> &start, vector<int> &end);
		void flatten(int nonterm, map<int, pair<int, int>>  &r_grammar, vector<int> &res);
		
		// vector<int> iteration(vector<int> txt, vector<int> &start, vector<int> &end);
	public:
		void compress(vector<int> txt, int compress_mode);
		void compress2(vector<int> txt);
		vector<int> decompress();
		vector<unsigned int> get_grammar();

		int get_lz77_size(){ return lz77_size; }

		int get_remaining_length() { return len; }
		int get_grammar_size() {return nonterms.size(); }

};


#endif
