#include "slp2_v3.h"
#include "suffix_tree.h"
// #include "third_party_lz77/third_party_lz77_wrapper.h"

namespace slpv3{

slp2_v3::slp2_v3(vector<int> txt, float coeff, int method, int crossing_type, int l) {

	pair_heap = NULL;

	_limit = l;
	_method = method;
	_crossing_type = crossing_type;

	vector<int> result;
	// _limit = txt.size()*5;
	// int limit = 100000;
	compress(txt, result, coeff, _limit);
}

slp2_v3::~slp2_v3(){
	// cout<<"deeted\n";
	// pair_heap->clear();
	// while(pair_heap->size()) pair_heap->erase(pair_heap->begin());
	delete pair_heap;
	// cout<<"EE \n";
	// exit(0);
}

void slp2_v3::compress(vector<int> &txt, vector<int> &result, float coeff, int limit) {

	float next_coeff = coeff;
	next_coeff = 0.0f;
	// next_coeff = 0.0f;

	// if(limit == 0)
		// coeff = 0.0f;

	_coeff = coeff;


	text.clear();
	deleted_v.clear();
	// next_free.clear();
	while(next_free.size())
		next_free.pop();
	
	pair_count.clear();
	cross_count.clear();
	pair_positions.clear();

	text.resize(txt.size()*2 + 50); //because we set 2x we can in each iteration have new deleted vector <-not really
	deleted_v.resize(txt.size()*2 + 50, 0);

	for(int i=txt.size(); i < txt.size()*2 + 50; ++i){
		next_free.push(i);
	}

	for(int i=0; i<txt.size(); ++i){
		text[i].letter = txt[i];
		text[i].next = i+1;
		text[i].prev = i-1;
	}

	text[txt.size()-1].next = -1;

	// obtain lz77[factorization]

	vector<unsigned int> factors;
	vector<unsigned int> factor_starts;
	suffix_tree st;
	st.lz77(txt, factors, factor_starts);

	// lz77_wrapper::lz77(txt, factors, factor_starts);
	
 	// st.super_factor_log(txt, factors, factor_starts, 64);
 	// cout<<"wohooo\n";
 	
 	_lz77_factors = factors.size();
 	// cout<<limit<<" "<<_lz77_factors<<"\n";

	// cout<<"start\n";

	//copying lz77 factorization
	text[0].start = true;
	text[0].end = true;
	for(int i=1; i<factors.size(); ++i) {
		for(int j=factors[i-1]; j<factors[i]; ++j) {
			// text[i-1].start = false;
			// text[i-1].end = false;
		}
		// cout << factors[i-1]<<"\n";
		text[factors[i-1]].start = true;
		text[factors[i]-1].end = true;
	}

	// vector<int> txt2;
	// for(int i=0; i<txt.size(); ++i){
	// 	txt2.push_back(txt[txt.size()-i-1]);
	// }

	// suffix_tree st2;
	// factors.clear();
	// factor_starts.clear();
	// st.lz77(txt2, factors, factor_starts);

	// 0 1 2 3
	// 3 2 1 0

	// 8 -7 - 1
	// ABCDEFGH
	// HGFEDCBA

	// for(int i=1; i<factors.size(); ++i) {
		
	// 	if(factors[i-1] >= txt2.size()/2 ) {
	// 		for(int j=factors[i-1]; j<factors[i]; ++j) {
	// 			text[txt2.size() -1 - i-1].start = false;
	// 			text[txt2si-1].end = false;
	// 		}
	// 		// cout << factors[i-1]<<"\n";
	// 		text[factors[i-1]].start = true;
	// 		text[factors[i]-1].end = true;

	// 	}
	// }




	//next new letter
	next_nonterm = txt[0] + 1;
	for(int i=0; i<txt.size(); ++i) {
		next_nonterm = max(next_nonterm, txt[i] + 1);		
	}

	largest_orginal_letter = next_nonterm-1;

	for(int i=0; i<txt.size()-1; ++i){
		pair_positions[ convert_pair(make_pair(txt[i], txt[i+1])) ].push_back(make_pair(i, i+1));
		pair_count[ convert_pair(make_pair(txt[i], txt[i+1])) ]++;
		
		// if(text[i].end && text[i+1].start)
		int is_cross = is_crossing(i, i+1); 
		if(is_cross)
			cross_count[ convert_pair(make_pair(txt[i], txt[i+1]))] += is_cross;
	}

	uncompressed_len = txt.size();
	first = 0;
	int last = uncompressed_len-1;

	// print_forward(first, uncompressed_len);
	// print_backward(last, uncompressed_len);

	// cmp_pairs cmp(&pair_count, &cross_count, coeff);
	cmp_triples cmp(coeff);

	if(pair_heap != NULL)
		delete pair_heap;

	pair_heap = new set<tri, cmp_triples>(cmp); 

	// cout<<pair_count[make_pair(97,98)]<<"\n";
	// cout<<cross_count[make_pair(97,98)]<<"\n";

	for(int i=0; i<txt.size()-1; ++i){
		pair_heap->insert(make_tri(txt[i], txt[i+1]));
	}

	// return;

	// print_forward(first, uncompressed_len);

	int current_len = txt.size();
	int removed_count = 0;

	// int threshold = current_len/2.0f;
	int threshold = current_len*3.0f/4.0f;

	bool no_pairs = false;

	// cout<<"START\n";
	int replaced_count = 0;
	while( (text[first].next != -1) && pair_heap->size()){

		replaced_count++;
		if(replaced_count > limit) {
			break;
		}

		pair<int, int> pp = pair_to_replace();
		// cout<<next_free.front()<<"|\n";
		// auto pcnt = check_count(first, pp);
		// cerr<<" Size: " << pair_heap->size() <<" " << pair_count[convert_pair(pp)] <<"\n";

		if(pair_count[convert_pair(pp)] < 2){
			no_pairs = true;
			break;
		}
		// cerr<<pp.first<<" "<<pp.second<<" << Replacing\n";

		int pc = pair_count[convert_pair(pp)];
		int cc = cross_count[convert_pair(pp)];
		// cout<<pair_count[convert_pair(pp)]<<"\n";
		// cout << " << " << pair_count[convert_pair(pp)]<<" << \n";`


		// cout << " << " << pair_count[convert_pair(pp)] << " |  " <<
		// 	pcnt.first << " :: " << 
		// 	cross_count[convert_pair(pp)]<<" | " << 
		// 	pcnt.second << " << \n";
		// cout<<pp.first<<" "<<pp.second<<" "<<pair_count[convert_pair(pp)]<<endl;;

		// if(pc < 100) break;

		removed_count += replace_pair(pp, first);
		// removed_count += replace_pair_noncrossing_first(pp, first);

		// cout<< pc <<" "<< cc <<" "<<current_len-removed_count<<" "<<pp.first<<" "<<pp.second<<"\n";

		// if( current_len - removed_count  < threshold && limit)
			// break;

		// if(pc < 350 && limit)
		// 	break;

		// replace_pair_v2(pp, first);
		// replace_pair(pp, first);

		// cerr<<"repl\n";



		// auto phit = pair_heap->begin();

		// while(phit != pair_heap->end()) {
		// 	pp = (*phit);

		// 	pcnt = check_count(first, pp);

		// 	// cout << " << " << pair_count[convert_pair(pp)] << " |  " <<
		// 	// pcnt.first << " :: " << 
		// 	// cross_count[convert_pair(pp)]<<" | " << 
		// 	// pcnt.second << " << \n";

		// 	if(pair_count[convert_pair(pp)] == pcnt.first && cross_count[convert_pair(pp)] == pcnt.second) {
		// 		cout<<"OK\n";
		// 	}
		// 	else
		// 		cout<<"FFFAIL\n";

		// 	phit++;
		// }

		// cout<<"______\n";
		// return;
		// cout<<pair_heap->size()<<"\n";
		// if(pair_count[ convert_pair(pp) ] < 2) {
		// 	// break;
		// 	cout<<"End\n";
		// } else 
		// 	cout<<"XEnd\n";
			// print_forward(first, uncompressed_len);
			// print_backward(9, uncompressed_len);

		// cout<<"iter: "<< pair_count[convert_pair(pp)] << " " << pair_heap->size() <<" " << len(first) << "\n";
		// auto it = pair_heap->begin();
		// while(it != pair_heap->end()){

		// 	cout<<(*it).first<<" "<<(*it).second<<" "<<pair_count[(*it)]<<" "<<cross_count[(*it)];
		// 	cout<<" | ";
		// 	for(int i=0; i<pair_positions[(*it)].size(); ++i){
		// 		cout<<pair_positions[(*it)][i]<<" ";
		// 	}

		// 	cout<<"\n";
		// 	it++;
		// }
		// cout<<"\n";
		// if(pair_heap->size() == 0)
			// print_forward(first, uncompressed_len);
	}
	// cout<<replaced_count<<" "<<removed_count <<" rc\n";
	// cout<<"END\n";
	// cout<<"End\n";
	// cout<<grammar.size()<<"\n";
	// cout<<"End??\n";
	pair_count.clear();
	cross_count.clear();

	/*
	if(!no_pairs && limit){
		// cout<<"Calling\n";

		// vector<int> new_txt;
		txt.clear();
		int current = first;
		while(current != -1) {
			txt.push_back(text[current].letter);
			current = text[current].next;
			// if(current == -1)
				// break;
		}
		compress(txt, result, next_coeff, limit-1);
	}*/
}

int slp2_v3::len(int first) {

	int current = first;
	// for(int i=0; i<print_len; ++i)
	int res = 1;
	while(true)
	{
		// if(text[current].start)
		// 	cout<<" | "<<text[current].letter;
		// else
		// 	cout<<" "<<text[current].letter;

		// if(text[current].end)
		// 	cout<<" ] ";
		// else
		// 	cout<<" ";

		current = text[current].next;

		if(current == -1)
			break;
		res++;
	}

	return res;

}

pair<int, int> slp2_v3::check_count(int first, pair<int, int> pp) {

	int current = first;
	int cross = 0, total = 0;
	while(text[current].next != -1) {
		if(text[current].letter == pp.first && text[text[current].next].letter == pp.second) {
			total++;
			if(text[current].end && text[text[current].next].start)
				cross++;
		}

		current = text[current].next;
	}

	return make_pair(total, cross);
}


void slp2_v3::print_forward(int first, int print_len){
	int current = first;
	for(int i=0; i<print_len; ++i)
	{
		if(text[current].start)
			cout<<" | "<<text[current].letter;
		else
			cout<<" "<<text[current].letter;

		if(text[current].end)
			cout<<" ] ";
		else
			cout<<" ";

		current = text[current].next;

		if(current == -1)
			break;
	}
	cout<<"\n";

	current = first;
	for(int i=0; i<print_len; ++i)
	{
		cout<<current<<" :: ";

		current = text[current].next;

		if(current == -1)
			break;
	}
	cout<<"\n";
}

void slp2_v3::print_backward(int last, int print_len){
	int current = last;
	for(int i=0; i<print_len; ++i)
	{
		if(text[current].end)
			cout<<" [ ";
		else
			cout<<" ";

		if(text[current].start)
			cout<<text[current].letter<<" | ";
		else
			cout<<text[current].letter<<" ";

		current = text[current].prev;
	}
	cout<<"\n";
}

pair<int, int> slp2_v3::pair_to_replace(){
	auto res = *(pair_heap->begin());
	// pair_heap->erase(pair_heap->begin());
	// cross_count->erase(cross_scount->find(convert_pair(res)));
	return res.p;
}

void slp2_v3::decrease_occurences(int fst_index, int snd_index) {
	if(fst_index != -1 && snd_index != -1){
		//decrease key
		pair<int, int> neigh_pair = make_pair(text[fst_index].letter, text[snd_index].letter);

		auto it = pair_heap->find(make_tri(neigh_pair));
		if(it != pair_heap->end()){
			pair_heap->erase(it);
		}
		// pair_heap->erase(pair_heap->find(neigh_pair));

		pair_count[convert_pair(neigh_pair)]--;
		
		// if(text[fst_index].end && text[snd_index].start)
		int is_cross = is_crossing(fst_index, snd_index);
		if(is_cross)
			cross_count[convert_pair(neigh_pair)] -= is_cross;


		if(pair_count[convert_pair(neigh_pair)] > 1)
			pair_heap->insert(make_tri(neigh_pair));
	}
}

void slp2_v3::increase_occurences(int fst_index, int snd_index) {
	if(fst_index != -1 && snd_index != -1){

		pair<int, int> neigh_pair = make_pair(text[fst_index].letter, text[snd_index].letter);
		auto it = pair_heap->find(make_tri(neigh_pair));
		if(it != pair_heap->end()){
			pair_heap->erase(it);
		}

		pair_count[convert_pair(neigh_pair)]++;

		// if(text[fst_index].end && text[snd_index].start) {
		int is_cross = is_crossing(fst_index, snd_index);
		if(is_cross)
			cross_count[convert_pair(neigh_pair)] += is_cross;
		// }


		if(pair_count[convert_pair(neigh_pair)] > 1)
			pair_heap->insert(make_tri(neigh_pair));

	}
}

void slp2_v3::split_letter(int index){

	// not entirely correct

	if(text[index].start && text[index].end)
		return;

	if(text[index].start) {
		text[index].end = true;
		text[text[index].next].start = true;
		pair<int, int> neigh_pair = make_pair(text[index].letter, text[text[index].next].letter);
		
		// auto it = pair_heap->find(neigh_pair);
		// if(it != pair_heap->end()){
		// 	pair_heap->erase(it);
		// }
		// cross_count[convert_pair(neigh_pair)]++;
		// pair_heap->insert(neigh_pair);
		// increase_occurences(index, text[index].next);
	} else if(text[index].end) {
		text[index].start = true;
		text[text[index].prev].end = true;
		pair<int, int> neigh_pair = make_pair(text[text[index].prev].letter, text[index].letter);

		// auto it = pair_heap->find(neigh_pair);
		// if(it != pair_heap->end()){
		// 	pair_heap->erase(it);
		// }
		// cross_count[convert_pair(neigh_pair)]++;
		// pair_heap->insert(neigh_pair);
		// increase_occurences(text[index].prev, index);
	}

}

void slp2_v3::replace_at(int i, int new_letter){

	int prev_index = text[i].prev;
	int index_fst = i;
	int index_snd = text[i].next;
	int next_index = text[index_snd].next;

	decrease_occurences(index_fst, index_snd); //trzeba jesli nie usuwamy wszystkich

	decrease_occurences(prev_index, index_fst);
	decrease_occurences(index_snd, next_index);

	// nie przejmujemy sie pp, i tak wyleci po tej iteracji
	// pair<int, int> pp = make_pair(text[index_fst].letter, text[index_snd].letter);
	// auto it = pair_heap->find(pp);

	// if(it != pair_heap.end())
		// pair_heap.erase(it);

	// cout<<next_free.size()<<" SS \n";

	int new_index = next_free.front();
	next_free.pop();

	if(text[index_fst].end && text[index_snd].start) { //crossing pair
		// if(prev_index != -1)
		split_letter(index_fst);
		split_letter(index_snd);
	}


	text[new_index].letter = new_letter;
	text[new_index].prev = prev_index;
	text[new_index].next = next_index;

	text[new_index].start = text[index_fst].start;
	text[new_index].end = text[index_snd].end;

	if(prev_index != -1)
		text[prev_index].next = new_index;
	
	if(next_index != -1)
		text[next_index].prev = new_index;

	increase_occurences(prev_index, new_index);
	increase_occurences(new_index, next_index);

	if(prev_index != -1)
		pair_positions[ convert_pair(make_pair(text[prev_index].letter, text[new_index].letter) ) ].push_back(make_pair(prev_index, new_index));
	
	if(next_index != -1){
		pair_positions[ convert_pair(make_pair(text[new_index].letter, text[next_index].letter) ) ].push_back(make_pair(new_index,next_index));
	}

	// next_free.push(index_fst);
	// next_free.push(index_snd);

	// deleted.insert(index_fst);
	// deleted.insert(index_snd);

	deleted_v[index_fst] = true;
	deleted_v[index_snd] = true;

	if(first == index_fst) first = new_index;
}

int slp2_v3::replace_pair(pair<int, int> pp, int first){

	int removed_cnt = 0;

	int new_letter = next_nonterm;
	// set<int> deleted;

	// if(grammar.find(convert_pair(pp)) == grammar.end()) {
	if(grammar.find(pp) == grammar.end()) {
		// grammar[convert_pair(pp)] = new_letter;
		grammar[pp] = new_letter;
		next_nonterm++;
	}
	else
		new_letter = grammar[pp];
		// new_letter = grammar[convert_pair(pp)];

	for(auto it = pair_positions[convert_pair(pp)].begin(); it != pair_positions[convert_pair(pp)].end(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		// if(next_index != -1)
			// if(text[index].letter == pp.first && text[next_index].letter == pp.second)
				// replace_at(index, new_letter);

		if(next_index != -1){
			// if(deleted.find(index) == deleted.end() && deleted.find(next_index) == deleted.end()){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				// cout<<"replacing: "<<index<<" "<<next_index<<"\n";
				// cout<<"letters: "<<text[index].letter<<" "<<text[next_index].letter<<"\n";
				removed_cnt++;
				replace_at(index, new_letter);
	// return;
			}
		}
	}

	// auto it = pair_heap->find(pp);
	// if(it == pair_heap->end()) cout<<"AA\n";
	// cout<<"d8dasaaasasdasd\n";
	// pair_heap->erase(pair_heap->find(pp)); // <- juz to usunales

	auto pair_heap_it = pair_heap->find(make_tri(pp));
	if(pair_heap_it != pair_heap->end())
		pair_heap->erase(pair_heap_it);

	// pair_heap->erase(pair_heap->find(pp));

	auto cross_count_it = cross_count.find(convert_pair(pp));
	if(cross_count_it != cross_count.end())
		cross_count.erase(cross_count_it);

	pair_count.erase(pair_count.find(convert_pair(pp)));

	pair_positions.erase(pair_positions.find(convert_pair(pp)));

	return removed_cnt;
	// cout<<"replacing end\n";
}

void slp2_v3::replace_pair_v2(pair<int, int> pp, int first){

	//idea - first replace non-crossing

	int new_letter = next_nonterm;
	// set<int> deleted;

	// if(grammar.find(convert_pair(pp)) == grammar.end()) {
	if(grammar.find(pp) == grammar.end()) {
		// grammar[convert_pair(pp)] = new_letter;
		grammar[pp] = new_letter;
		next_nonterm++;
	}
	else
		new_letter = grammar[pp];
		// new_letter = grammar[convert_pair(pp)];


	// first replace all non crossing pairs
	for(auto it = pair_positions[convert_pair(pp)].begin(); it != pair_positions[convert_pair(pp)].end(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && !deleted_v[next_index]) {
				// if(!(text[index].end && text[next_index].start))
				if(!is_crossing(index, next_index))
					replace_at(index, new_letter);
			}
		}
	}

	for(auto it = pair_positions[convert_pair(pp)].begin(); it != pair_positions[convert_pair(pp)].end(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				replace_at(index, new_letter);
			}
		}
	}

	auto pair_heap_it = pair_heap->find(make_tri(pp));
	if(pair_heap_it != pair_heap->end())
		pair_heap->erase(pair_heap_it);

	auto cross_count_it = cross_count.find(convert_pair(pp));
	if(cross_count_it != cross_count.end())
		cross_count.erase(cross_count_it);

	pair_count.erase(pair_count.find(convert_pair(pp)));

	pair_positions.erase(pair_positions.find(convert_pair(pp)));
}


//replace ONE pair preferably non-crossing
void slp2_v3::replace_pair_v3(pair<int, int> pp, int first){

	//idea - first replace non-crossing

	int new_letter = next_nonterm;
	// set<int> deleted;

	// if(grammar.find(convert_pair(pp)) == grammar.end()) {
	if(grammar.find(pp) == grammar.end()) {
		// grammar[convert_pair(pp)] = new_letter;
		grammar[pp] = new_letter;
		next_nonterm++;
	}
	else
		new_letter = grammar[pp];
		// new_letter = grammar[convert_pair(pp)];

	bool replaced = 0;

	for(auto it = pair_positions[convert_pair(pp)].rbegin(); !replaced && it != pair_positions[convert_pair(pp)].rend(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				if(!is_crossing(index, next_index)){
					replace_at(index, new_letter);
					replaced = 1;
					pair_positions[convert_pair(pp)].erase(std::next(it).base());
				}
			}
		}
	}

	for(auto it = pair_positions[convert_pair(pp)].rbegin(); !replaced && it != pair_positions[convert_pair(pp)].rend(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				replace_at(index, new_letter);
				replaced = 1;
				pair_positions[convert_pair(pp)].erase(std::next(it).base());

			}
		}
	}

	if(pair_count[convert_pair(pp)] == 0) {
		auto pair_heap_it = pair_heap->find(make_tri(pp));
		if(pair_heap_it != pair_heap->end())
			pair_heap->erase(pair_heap_it);

		auto cross_count_it = cross_count.find(convert_pair(pp));
		if(cross_count_it != cross_count.end())
			cross_count.erase(cross_count_it);

		pair_count.erase(pair_count.find(convert_pair(pp)));

		pair_positions.erase(pair_positions.find(convert_pair(pp)));
	}
}

void slp2_v3::replace_pair_v4(pair<int, int> pp, int first){

	//idea - first replace non-crossing

	int new_letter = next_nonterm;
	// set<int> deleted;

	// if(grammar.find(convert_pair(pp)) == grammar.end()) {
	if(grammar.find(pp) == grammar.end()) {
		// grammar[convert_pair(pp)] = new_letter;
		grammar[pp] = new_letter;
		next_nonterm++;
	}
	else
		new_letter = grammar[pp];
		// new_letter = grammar[convert_pair(pp)];

	bool replaced = 0;

	for(auto it = pair_positions[convert_pair(pp)].rbegin(); it != pair_positions[convert_pair(pp)].rend(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				if(!is_crossing(index, next_index)){
					replace_at(index, new_letter);
					replaced = 1;
					pair_positions[convert_pair(pp)].erase(std::next(it).base());
				}
			}
		}
	}

	for(auto it = pair_positions[convert_pair(pp)].rbegin(); !replaced && it != pair_positions[convert_pair(pp)].rend(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				replace_at(index, new_letter);
				pair_positions[convert_pair(pp)].erase(std::next(it).base());

			}
		}
	}

	if(pair_count[convert_pair(pp)] == 0) {
		auto pair_heap_it = pair_heap->find(make_tri(pp));
		if(pair_heap_it != pair_heap->end())
			pair_heap->erase(pair_heap_it);

		auto cross_count_it = cross_count.find(convert_pair(pp));
		if(cross_count_it != cross_count.end())
			cross_count.erase(cross_count_it);

		pair_count.erase(pair_count.find(convert_pair(pp)));

		pair_positions.erase(pair_positions.find(convert_pair(pp)));
	}
}

int slp2_v3::replace_pair_noncrossing_first(pair<int, int> pp, int first){

	int new_letter = next_nonterm;
	// set<int> deleted;

	// if(grammar.find(convert_pair(pp)) == grammar.end()) {
	if(grammar.find(pp) == grammar.end()) {
		// grammar[convert_pair(pp)] = new_letter;
		grammar[pp] = new_letter;
		next_nonterm++;
	}
	else
		new_letter = grammar[pp];
		// new_letter = grammar[convert_pair(pp)];

	bool replaced = 0;

	// first replace all non crossing pairs
	for(auto it = pair_positions[convert_pair(pp)].begin(); it != pair_positions[convert_pair(pp)].end(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && !deleted_v[next_index]) {
				// if(!(text[index].end && text[next_index].start))
				if(!is_crossing(index, next_index)) {
					replaced = 1;
					replace_at(index, new_letter);
				}
			}
		}
	}

	// bool replaced_cross = 0;
	for(auto it = pair_positions[convert_pair(pp)].begin(); !replaced && it != pair_positions[convert_pair(pp)].end(); ++it) {
		int index = (*it).first;
		int next_index = (*it).second;

		if(next_index != -1){
			if(!deleted_v[index] && ! deleted_v[next_index]) {
				replace_at(index, new_letter);
				// replaced_cross = 1;
			}
		}
	}


	if(pair_count[convert_pair(pp)] == 0) {
		auto pair_heap_it = pair_heap->find(make_tri(pp));
		if(pair_heap_it != pair_heap->end())
			pair_heap->erase(pair_heap_it);

		auto cross_count_it = cross_count.find(convert_pair(pp));
		if(cross_count_it != cross_count.end())
			cross_count.erase(cross_count_it);

		pair_count.erase(pair_count.find(convert_pair(pp)));

		pair_positions.erase(pair_positions.find(convert_pair(pp)));
	}

	return 0;

}

int slp2_v3::get_grammar_size(){
	return grammar.size();
}

int slp2_v3::get_remaining_length() {
	return len(first);
}

vector<int> slp2_v3::get_remaining_sequence() {
	int current = first;
	// int res = 1;
	vector<int> res;
	res.push_back(text[current].letter);
	while(true)
	{
		current = text[current].next;

		if(current == -1)
			break;

		res.push_back(text[current].letter);

		// res++;
	}

	return res;

}

vector<int> slp2_v3::decompress() {
	int mx = 0;
	auto it = grammar.begin();
	map<int, pair<int, int>> r_grammar;
	for(;it != grammar.end(); ++it){
		mx = max(mx, it->second);
		r_grammar[it->second] = it->first;
		// cout<<it->second<<" "<<it->first.first<<" "<<it->first.second<<" |\n";
	}

	vector<int> res;
	int current = first;
	while(current != -1) {
		flatten(text[current].letter, r_grammar, res);
		current = text[current].next;
	}
	return res;
}

void slp2_v3::flatten(int nonterm, map<int, pair<int, int>>  &r_grammar, vector<int> &res){

	if(r_grammar.find(nonterm) == r_grammar.end()){
		// res.push_back(nonterm);
		// vector<int> res;
		// res.push_back(nonterm);
		// cout<<"leaf: "<<nonterm<<"\n";
		// return res;
		res.push_back(nonterm);
		// leaf_cnt++;
		// if(leaf_cnt % 1000 == 0)
			// cout<<"leaf: "<<leaf_cnt<<"\n";
	} else {
		pair<int, int> pp = r_grammar[nonterm];
		// vector<int> left = flatten(pp.first, r_grammar);
		// vector<int> right = flatten(pp.second, r_grammar);

		// left.insert(left.end(), right.begin(), right.end());
		// return left;
		flatten(pp.first, r_grammar, res);
		flatten(pp.second, r_grammar, res);

	}

}

}