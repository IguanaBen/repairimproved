
import os
# name1 = "rrr/book2.results.function0"

def tranform(indir, outdir, name):
	op = [0, 1, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2,  1, 1, 1]
	round2 = [4, 6, 8, 10, 12]
	round3 = [13, 14, 15]
	best_ind = []
	best_val = []
	res_lines = []
	for i in range(len(op)):
		best_ind.append(-1)
		if op[i] == 0:
			best_val.append(0)
		if op[i] == 1:
			best_val.append(10**9)
		if(op[i] == 2):
			best_val.append(-10**9)

	with open(indir + "/" +  name) as f:
		 content = f.readlines()
		 
		 for i in range(len(content)):
		 	if content[i][0] != 'B':
		 		if (content[i][0] == 'L' or content[i][0] == 'F'):
		 			# print(content[i], end="")
		 			res_lines.append([content[i][:-1]])
		 		else:
		 			sl = content[i][:-3].split('&')

		 			# if sl[0][0] != 'R':
		 			# 	sl[0] = str(round(float(sl[0]) + 1.0, 2) )
		 				
		 			sl[1], sl[3] = sl[3], sl[1]
		 			sl[2], sl[4] = sl[4], sl[2]
		 			res = []
		 			for j in range(len(sl)):
		 				if op[j] == 0:
		 					res.append(sl[j].strip())
		 				if op[j] == 1:
		 					res.append(sl)
		 					if float(sl[j]) < float(best_val[j]):
		 						best_val[j] = sl[j]
		 						best_ind[j] = i
		 				if op[j] == 2 :
		 					if float(sl[j]) > float(best_val[j]):
		 						best_val[j] = sl[j]
		 						best_ind[j] = i
		 			# print(sl)
		 			res_lines.append(sl)
		 			# print(" & ".join(sl))


		 for i in range(len(res_lines)):
		 	if len(res_lines[i]) > 1:
		 		res_lines[i][0] = res_lines[i][0].strip()

		 		for j in range(len(res_lines[i])-1):
		 			# print(j)
		 			res_lines[i][j+1] = res_lines[i][j+1].strip() 
		 			if (j+1) in round2:
		 				res_lines[i][j+1] = '{:.2f}'.format(round(float(res_lines[i][j+1].strip()), 2))
		 			if (j+1) in round3:
		 				res_lines[i][j+1] = '{:.3f}'.format(round(float(res_lines[i][j+1].strip()), 3))

		 for i in range(len(op)):
		 	if best_ind[i] != -1 :
		 		# print(best_ind[i])
		 		res_lines[best_ind[i]][i] = "\\textbf{" + str(res_lines[best_ind[i]][i]) + "}"

		 f = open(outdir + "/" + name, 'w')
		 for line in res_lines:
		 	f.write(" & ".join(line) + "\\\\" + "\n")
		 # print(" & ".join(res_lines[3]))
		 # print(res_lines[3])



# tranform(name1)

indir = "RESULTS_S3"
outdir = "RESULTS_NEW_2"

filenames =os.listdir(indir)
for name in filenames:
	tranform(indir, outdir, name)

