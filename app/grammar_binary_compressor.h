#ifndef GRAMMAR_BINARY_COMPRESSOR
#define GRAMMAR_BINARY_COMPRESSOR

#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>

#include "elias_coding.h"

using std::map;

/*
TODO: możesz flipować  zamiast całej gramatyki generację, wtedy powinno być jeszcze lepiej(może...)

*/



//chyba coder a nie compressor
class grammar_binary_compressor {

		static void prepare_numbers(map<int, pair<int, int>> &grammar,
				int alphabet_size,
				map<int, int> &_nonterm_positions, 
				vector<int> &_compressed_grammar,
				vector<int> &_generation_sizes,
				vector<int> &_generation_first_number,
				vector<int> &_necessary_bits) {

			vector<int> compressed_grammar;

			// map<int, int> nonterm_perm;
			// map<pair<int, int>, int> old_pos;

			//flip and get the smaller :P

			// map<int, pair<int, int>> r_grammar;
			// for(auto it = grammar.begin(); it != grammar.end(); ++it) {
			// 	// r_grammar[it->second] = it->first;
			// 	auto pp = it->second;
			// 	auto pp2 = pp;
			// 	pp.first = pp2.second;
			// 	pp.second = pp2.first;
			// 	r_grammar[it->first] = pp;

			// }
			// grammar = r_grammar;

			int min_nonterm = (*grammar.begin()).first;

			vector<int> generation(grammar.size() + 256, -1);

			map<int, int> positions;
			vector<vector<int>> gens;

			int mx_gen = 0;

			int nonterminal_position = 0;
			for(auto it = grammar.begin(); it != grammar.end(); ++it){

				// old_pos[it->second] = nonterminal_position++;

				calculate_generation(it->first, grammar, generation);
				mx_gen = max(generation[it->first], mx_gen);

				while(generation[it->first] >= gens.size())
					gens.push_back(vector<int>());

				gens[generation[it->first]-1].push_back(it->first);

			}

			// set<int> alphabet;
			// for(int i=0; i<256;++i){
			// 	if(generation[i] == 0)
			// 		alphabet.insert(i);
			// }

			// int alphabet_size = alphabet.size();
			// for(int i=0; i<gens[0].size(); ++i) {
			// 	auto pp = r_grammar[gens[0][i]]; 
			// 	positions[pp] = current_position++;
			// 	result.push_back()
			// }

			vector<int> generation_size;
			vector<int> generation_first_number;
			vector<int> necessary_bits;
			// cout<<min_nonterm<<" mnt\n";
			int current_position = 0;
			int last_generation_size = 0;
			int current_sum = 0;

			// 4 - 2 = 
			// 0      1        2     3       4
			// <a,b> <b,a> | <0,1> <1,0> | <1,0>
			// 4 <0,1>
			// 5 <2,3>
			//---
			// 6 <,5>
			int cnt = 0;
			for(int i=0; i<mx_gen; ++i) {
				vector<pair< pair<int, int>, pair<int, int> >> sorted_generation;
				for(int j=0; j<gens[i].size(); ++j) {
					auto pp = grammar[gens[i][j]];
					auto pp2 = pp;
					
					if(pp.first >= min_nonterm)
						pp2.first = positions[pp.first] + alphabet_size;

					// cout<<"processing: "<<gens[i][j]<<"\n";

					int nb = 0;

					if( (pp.first < min_nonterm || positions[pp.first] < current_sum - last_generation_size)){
						if(i > 0){
							pp2.second = positions[pp.second] - current_sum + last_generation_size;
							// necessary_bits.push_back(log2ceil(last_generation_size-1));
							nb = log2ceil(last_generation_size-1);
							// cout<<"pp2.second is: "<<pp2.second<<" "<<log2ceil(last_generation_size-1)<<"\n"; 
						}
						else{
							pp2.second = pp.second;
							nb =  log2ceil(alphabet_size-1);
							// necessary_bits.push_back(log2ceil(alphabet_size-1));
						}
					} else {
						// necessary_bits.push_back(log2ceil(current_sum + alphabet_size-1));
						nb = log2ceil(current_sum + alphabet_size-1);

						// necessary_bits.push_back(log2ceil(current_sum + alphabet_size));
						if(pp.second >= min_nonterm)
							pp2.second = positions[pp.second] + alphabet_size;
					}

					// pp2.second++;

					sorted_generation.push_back(make_pair(pp2, make_pair(gens[i][j],nb) ));
				}
				sort(sorted_generation.begin(), sorted_generation.end());
				int pos = current_position;
				for(int j=0; j<sorted_generation.size(); ++j) {
					auto pp = sorted_generation[j];
					// result.push_back(make_pair(pp.first.first, pp.first.second));
					compressed_grammar.push_back(pp.first.first);
					int snd = pp.first.second;
					compressed_grammar.push_back(snd);			
					positions[pp.second.first] = current_position++;
					necessary_bits.push_back(pp.second.second);
					// auto ps = pp.second;

					// nonterm_perm[pp.second] = cnt++; 

				}
				
				current_sum += sorted_generation.size();
				last_generation_size = sorted_generation.size();

				generation_size.push_back(sorted_generation.size());
				generation_first_number.push_back(sorted_generation[0].first.first);
				// positions[grammar] = current_position++;
			}

			// cout<<"Generation: "<<mx_gen<<" rsize: "<<compressed_grammar.size()<<" "<<grammar.size()*2<<" alph: "<<alphabet.size()<<" cnt: "<<cnt<<"\n";

			_nonterm_positions = positions;

			_compressed_grammar = compressed_grammar;
			_generation_sizes = generation_size;
			_generation_first_number = generation_first_number;
			// _alphabet = alphabet;
			_necessary_bits = necessary_bits; 

			// return result;
		}


		static map<pair<int, int>, int> grammar_from_binary(vector<bool> vec){

		}

		static void normalize_grammar(map<int, pair<int, int>> &in_grammar, map<int, pair<int, int>> &_grammar, set<int> &_alphabet){
			
			set<int> alphabet;
			for(auto it = in_grammar.begin(); it != in_grammar.end(); ++it) {
				int f = it->second.first;
				int s = it->second.second;
				if(in_grammar.find(f) == in_grammar.end())
					alphabet.insert(f);
				if(in_grammar.find(s) == in_grammar.end())
					alphabet.insert(s);

				// if(f < 0 || s < 0)
					// cout<<f<<" ... "<<s<<"\n";
			}

			map<int, int> alphabet_index;
			int ind = 0;
			for(auto it = alphabet.begin(); it != alphabet.end(); ++it) {
				alphabet_index[*it] = ind++;
			}
			// 0 1 2 3 5
			int min_nonterm = in_grammar.begin()->first;

			int alphabet_size = alphabet.size();
			int dif = min_nonterm - alphabet_size;
			map<int, pair<int, int>> grammar;
			for(auto it = in_grammar.begin(); it != in_grammar.end(); ++it) {
				// r_grammar[it->second] = it->first;
				int key = it->first;
				int fst = it->second.first;
				int snd = it->second.second;

				if(key < min_nonterm){
					key =  alphabet_index[key];
				} else {
					key -= dif;
				}

				if(fst < min_nonterm){
					fst =  alphabet_index[fst];
				} else {
					fst -= dif;
				}

				if(snd < min_nonterm){
					snd =  alphabet_index[snd];
				} else {
					snd -= dif;
				}

				grammar[key] = make_pair(fst, snd);

			}


			// return grammar;
			_grammar = grammar;
			_alphabet = alphabet;
			// map<pair<int, int>, int> result_grammar;
			// for(auto it = r_grammar.begin(); it != r_grammar.end(); ++it) {

			// }
		}

		static map<int, pair<int, int>> denormalize_grammar(map<int, pair<int, int>> &grammar, set<int> &alphabet, int min_nonterm) {
			
			map<int, int> alphabet_rindex;
			int ind = 0;
			for(auto it = alphabet.begin(); it != alphabet.end(); ++it) {
				alphabet_rindex[ind++] = *it;
				// cout<<*it<<" alph\n";
			}

			int alphabet_size = alphabet.size();
			int mx_letter = (*alphabet.rbegin());
			int dif = min_nonterm - alphabet_size;

			map<int, pair<int, int>> result_grammar;
			int current_nonterm_index = min_nonterm;
			// map<int, int> 
			for(auto it = grammar.begin(); it != grammar.end(); ++it) {
				int key = it->first;
				int fst = it->second.first;
				int snd = it->second.second;

				if(key < alphabet.size()){

					cout<<"like, really?\n";
					key =  alphabet_rindex[key];
				} else {
					key += dif;
				}

				if(fst < alphabet.size()){
					fst =  alphabet_rindex[fst];
				} else {
					fst += dif;
				}

				if(snd < alphabet.size()){
					snd =  alphabet_rindex[snd];
				} else {
					snd += dif;
				}

				result_grammar[current_nonterm_index++] = make_pair(fst, snd);
			}

			return result_grammar;
		}

	public:

		//DEBUG
		static bool grammar_eq(map<int, pair<int, int>> &grammar1, map<int, pair<int, int>> &grammar2)
		{
			// return equal(grammar1.begin(), grammar1.end(), grammar2.begin(), grammar2.end());
			if(grammar1.size() != grammar2.size()) return false;

			// bool ok = true;
			int ind = 0;
			for(auto it = grammar1.begin(); it != grammar1.end(); ++it) {
				int fst = it->first;
				if(it->second != grammar2[fst]){
					cout<<ind<<"  fail here\n";
					return false; 
				}
				ind++;
			}

			return true;

		}

		static void print_grammar(map<int, pair<int, int>> &grammar) {

			for(auto it = grammar.begin(); it != grammar.end(); ++it) {
				cout<<it->first<<"->"<<it->second.first<<"|"<<it->second.second<<"\n";
			}
			cout<<"\n";

		}

		//end



		static void calculate_generation(int node, map<int, pair<int, int>> &grammar, vector<int> &generation) {
			if(grammar.find(node) == grammar.end()){
				generation[node] = 0;
			} else {
				auto pp = grammar[node];
				if(generation[pp.first] == -1)
					calculate_generation(pp.first, grammar, generation);
				if(generation[pp.second] == -1)
					calculate_generation(pp.second, grammar, generation);
				generation[node] = max(generation[pp.first], generation[pp.second]) + 1;
			}
		}


		static int log2ceil(int x){
			if(x == 0) return 1;
			int cnt = 0;
			while(x){
				x >>= 1;
				cnt++;
			}

			return cnt;
		}

		static vector<bool> to_binary(int x, int len) {
			vector<bool> result;
			while(len--) {
				result.push_back(x % 2);
				x >>= 1;
			}
			reverse(result.begin(), result.end());
			return result;

		}

		static void from_binary(vector<bool> &vec, int &x, int &pos, int len) {
			// reverse(result.begin(), result.end());
			int p2 = 1;
			int res = 0;
			for(int i=0; i<len; ++i){
				if(vec[pos + len-1-i])
					res += p2;
				p2 <<= 1;
			}
			pos += len;
			x = res;
			// return result;

		}

		static vector<bool> compress_grammar(map<int, pair<int, int>> &grammar, vector<int> &seq) {

			map<int, int> rm1, rm2;
			auto v1 = compress_grammar_flip(grammar, 0, rm1);
			int min_nonterm = (grammar.begin()->first);

			

			// return v1;
			auto v2 = compress_grammar_flip(grammar, 1, rm2);
			// cout<<v1.size()<<" "<<v2.size()<<"\n";
			if(v2.size() < v1.size()) {
				// cout<<"2!\n";
				for(int i=0; i<seq.size(); ++i){
				if(seq[i] >= min_nonterm)
					seq[i] = rm2[seq[i]];
				}
				return v2;
			}
			else {
				// cout<<"1!\n";
				for(int i=0; i<seq.size(); ++i){
					if(seq[i] >= min_nonterm)
						seq[i] = rm1[seq[i]];
				}
				return v1;
			}

		}

		static vector<bool> compress_grammar_flip(map<int, pair<int, int>> grammar, bool flip, map<int, int> &rename_map) {
			// map<pair<int, int>> pair_positions;


			// auto norm_grammar = normalize_grammar

			map<int, pair<int, int>> n_grammar;
			set<int> alphabet;

			if(flip) {
				for(auto it = grammar.begin(); it != grammar.end(); ++it) {
					auto pp = it->second;
					swap(pp.first, pp.second);
					it->second = pp;
				}
			}

			normalize_grammar(grammar, n_grammar, alphabet);

			// auto debug_gram = denormalize_grammar(n_grammar, alphabet, grammar.begin()->first);
			// cout<<"Is there\n";
			// print_grammar(grammar);
			// print_grammar(n_grammar);
			// print_grammar(debug_gram);


			vector<int> compressed_grammar;
			vector<int> generation_size;
			vector<int> generation_first_number;
			vector<int> necessary_bits;

			map<int, int> nonterm_positions, new_nonterm_name;

			prepare_numbers(n_grammar, alphabet.size(),
				nonterm_positions,
				compressed_grammar, generation_size,
				generation_first_number, necessary_bits);

			// cout<<"yep\n";
			rename_map = map<int, int>();
			int min_nonterm = grammar.begin()->first;
			// cout<<"D: "<<min_nonterm - alphabet.size()<<"\n";
			// cout<<" \n";
			for(auto it = nonterm_positions.begin(); it != nonterm_positions.end(); ++it) {
				rename_map[it->first + min_nonterm - alphabet.size()] = it->second + min_nonterm;
				// rename_map[it->first + ]
				// cout<<it->first<<" ; "<<it->second<<"\n";
				// cout<<it->first + min_nonterm - alphabet.size()<<" ;; "<<it->second + min_nonterm<<"\n";
			}
			// cout<<"\n";

			// rename_map = new_nonterm_name;


			vector<bool> result;
			result.push_back(flip); // flip
			
			auto min_nonterm_count = elias_coding::encode(min_nonterm);
			result.insert(result.end(), min_nonterm_count.begin(), min_nonterm_count.end());
			auto alphabet_count = elias_coding::encode(alphabet.size());
			// cout<<"hmmm "<<alphabet.size()<<"\n";
			result.insert(result.end(), alphabet_count.begin(), alphabet_count.end());
			for(auto it = alphabet.begin(); it != alphabet.end(); ++it){
				// cout<<"Encoding: "<<*it<<"\n";
				auto num = elias_coding::encode((*it) + 1);
				result.insert(result.end(), num.begin(), num.end());

			}
			// cout<<"OK\n";


			auto gen_count = elias_coding::encode(generation_size.size() + 1);
			result.insert(result.end(), gen_count.begin(), gen_count.end());

			for(int i=0; i<generation_size.size(); ++i) {
				// cout<<generation_size[i]<<" "<<generation_first_number[i]<<"\n"; 
				auto num_v = elias_coding::encode(generation_size[i]);
				result.insert(result.end(), num_v.begin(), num_v.end());
				num_v = elias_coding::encode(generation_first_number[i]+1);
				result.insert(result.end(), num_v.begin(), num_v.end());

				// cout<<generation_size[i]<<" gs\n";
			}
			// cout<<"Still ok\n";

			int current_gen_ind = 0;
			int gen = 0;

			int last_generation_size = 0;
			int current_sum = 0;

			// cout<<"| ";
			for(int i=0; i<compressed_grammar.size(); i+=2){
				// cout<<compressed_grammar[i]<<" "<<compressed_grammar[i+1]<<" ";
				if(current_gen_ind == 0) {
					auto num_v = elias_coding::encode(1);
					result.insert(result.end(), num_v.begin(), num_v.end());
				} else {
					auto num_v = elias_coding::encode( compressed_grammar[i] - compressed_grammar[i-2] + 1);
					result.insert(result.end(), num_v.begin(), num_v.end());
				}
				// cout<<necessary_bits[i/2]<<" <-:9\n";
				auto num_v = to_binary(compressed_grammar[i+1], necessary_bits[i/2]);
				// auto num_v = elias_coding::encode(compressed_grammar[i+1] + 1);
				result.insert(result.end(), num_v.begin(), num_v.end());

				current_gen_ind++;
				if(current_gen_ind == generation_size[gen]){
					current_gen_ind = 0;
					gen++;
				}
			}
			// cout<<" |\n";

			// cout<<result.size()<<"\n";

			// for(int i=0; i<numbers.size(); ++i)
			// 	cout<<numbers[i]<<" ";
			// cout<<"\n";

			return result;

		}


		static map<int, pair<int, int>> decompress(vector<bool> &vec){

			map<int, pair<int, int>> grammar;


			bool flip = vec[0];
			set<int> alphabet;
			int alphabet_size = 0;
			int pos = 1;
			int min_nonterm = 0;
			elias_coding::decode(vec, pos, min_nonterm);
			elias_coding::decode(vec, pos, alphabet_size);
			// cout<<alphabet_size<<" dupa\n";
			for(int i=0; i<alphabet_size; ++i) {
				int t = 0;
				elias_coding::decode(vec, pos, t);
				alphabet.insert(t-1);
				// cout<<t-1<<"\n";
			}

			int gen_count = 0;
			elias_coding::decode(vec, pos, gen_count);
			gen_count--;
			// cout<<gen_count<<" cc\n";

			vector<int> generation_size;
			vector<int> generation_first_number;
			for(int i=0; i<gen_count; ++i) {
				int gs = 0, gfn = 0;
				elias_coding::decode(vec, pos, gs);
				elias_coding::decode(vec, pos, gfn);
				gfn--;
				// cout<<gs<<" "<<gfn<<"\n";
				generation_size.push_back(gs);
				generation_first_number.push_back(gfn);
			}

			int gen_sum = 0;

			for(int i=0; i<gen_count; ++i)
				gen_sum += generation_size[i];

			int current_generation = 0;
			int index_gen = 0;
			int processed = 0;

			int current_sum = 0;

			int prev_generation_sum = 0;
			int last_generation_size = 0;

			int nonterm_index = alphabet_size;
			// int alphabet_size = alphabet.size();

			int prev_fst = 0;
			while(processed < gen_sum) {

				int fst = 0, snd = 0;
				int diff = 0;
				elias_coding::decode(vec, pos, diff);
				// cout<<"DD: "<<diff<<" d\n";
				diff--;
				if(index_gen == 0) {
					fst = generation_first_number[current_generation];
				} else {
					fst = prev_fst + diff;
				}

				prev_fst = fst;

				// if(fst >= alphabet_size)
					// fst -= alphabet_size;
				// cout<<fst<<" \n";
				// 4 - 2 = 
				// 4      5        6     7       4
				// <a,b> <b,a> | <0,1> <1,0> | <6,>
				// 5 < 2 

				// cout<<"A "<<current_generation<<" "<<fst<<" "<<last_generation_size<<"\n";

				int necessary_bits = 0;
				// cout<<prev_generation_sum + alphabet_size<<" <-prev_sum \n";
				if( (fst < alphabet_size || fst < prev_generation_sum + alphabet_size)){
					// cout<<"me\n";
					if(current_generation > 0){
						// cout<<"case\n";
						necessary_bits = log2ceil(last_generation_size-1);
						// cout<<vec[pos]<<" "<<snd<<" ";
						from_binary(vec, snd, pos, necessary_bits);
						// cout<<necessary_bits<<" nn \n";
						snd += prev_generation_sum + alphabet_size;
						// snd = alphabet_size + 
						// pp2.second = positions[pp.second] - current_sum + last_generation_size;
					}
					else{
						necessary_bits = log2ceil(alphabet_size-1);
						// cout<<necessary_bits<<"\n";
						from_binary(vec, snd, pos, necessary_bits);
					}
				} else {
					necessary_bits = log2ceil(current_sum + alphabet_size-1);
					// cout<<necessary_bits<<"\n";
					// necessary_bits.push_back(log2ceil(current_sum + alphabet_size));
					from_binary(vec, snd, pos, necessary_bits);

					// if(pp.second >= min_nonterm)
						// pp2.second = positions[pp.second] + alphabet_size;
				}

				// cout<<"necessary_bits: "<<necessary_bits<<"\n";
				// cout<<vec[pos-3]<<vec[pos-2]<<vec[pos-1]<<"\n";
				// int snd = 

				

				// current_generation++;
				processed++;

				// cout<<nonterm_index<<" : "<<fst<<" "<<snd<<"\n";
				grammar[nonterm_index++] = make_pair(fst, snd);

				index_gen++;
				if(index_gen == generation_size[current_generation]){
					index_gen = 0;

					if(current_generation) {
						prev_generation_sum += generation_size[current_generation-1];
					}
					last_generation_size = generation_size[current_generation];
					current_sum += generation_size[current_generation];		
					// last_generation_size = gen
					current_generation++;
				}

			}

			if(flip) {
				for(auto it = grammar.begin(); it != grammar.end(); ++it) {
					auto pp = it->second;
					swap(pp.first, pp.second);
					it->second = pp;
				}
			}
			// auto tempg = denormalize_grammar(grammar, alphabet);
			// print_grammar(tempg);
			return denormalize_grammar(grammar, alphabet, min_nonterm);
		}

		static void compress_arithmethic(vector<bool> &in, vector<bool> &result, int &pm, int &pd) {
			// vector<bool> res;

			long long ns = 0;

			for(int i=0; i<in.size(); ++i)
				ns += in[i];

			long long all = in.size();

			// float p = ns/all; //hmmmmmmmmmm

		 //    unsigned long long  high = 0xFFFFFFFFU;
		 //    unsigned long long  low = 0;  

		 //    int pending_bits = 0;

		 //    vector<bool> res;
			// for(int i=0; i< in.size(); ++i) {
			// 		long long  range = high - low + 1;
			// 		unsigned long long  lower = 0;
			// 		unsigned  long long upper = 0;
			// 		bool is_set = in[i];

			// 		if(!is_set){
			// 			lower = 0;
			// 			upper = all-ns;
			// 		} else {
			// 			lower = all-ns+1;
			// 			upper = all;
			// 		}

			// 	  high = low + (range * upper)/all;
			// 	  low = low + (range * lower)/all;

			// 	  // long long temp = (long long)(range)*(long long)(upper)/all;
			// 	  // high = low + temp;
			// 	  // temp = (long long)(range)*(long long)(lower)/all;
			// 	  // low = low + temp;

			// 	  // exit(0);
			// 	  for ( ; ; ) {
			// 	  cout<<low<<" "<<high<<" "<<0xFFFFFFFFU<<"\n";
			// 	    if ( high < 0x80000000U ) {
			// 	    	// cout<<"c1\n";
			// 	      output_bit_plus_pending( 0, pending_bits, res );
			// 	      low <<= 1;
			// 	      high <<= 1;
			// 	      high |= 1;
			// 	    } else if ( low >= 0x80000000U ) {
			// 	    	// cout<<"c2\n";

			// 	      output_bit_plus_pending( 1, pending_bits, res );
			// 	      low <<= 1;
			// 	      high <<= 1;
			// 	      high |= 1;
			// 	    } else if ( low >= 0x40000000 && high < 0xC0000000U ) {
			// 	    	// cout<<"c3\n";

			// 	      pending_bits++;
			// 	      low <<= 1;
			// 	      low -= 0x7FFFFFFF;
			// 	      high <<= 1;
			// 	      high -= 0x80000001;
			// 	    } else
			// 	      break;
			// 	  }
			// }
					// cout<<i<<" "<<res.size()<<" <-res\n";
			// std::cout<<res.size()<<" <-res\n";

			  vector<bool> res;
			  int pending_bits = 0;
			  unsigned long long low = 0;
			  unsigned long long high = 0xFFFFFFFFU;
			  int i=0;
			  for ( i=0; i<in.size(); ++i ) {
			    // int c = m_input.getByte();
			    // if ( c == -1 )
			    //   c = 256;
			    // prob p = m_model.getProbability( c );

			  	unsigned long long  lower = 0;
				unsigned  long long upper = 0;

			    	bool is_set = in[i];

					if(!is_set){
						lower = 0;
						upper = all-ns;
					} else {
						lower = all-ns+1;
						upper = all;
					}

				// high = low + (range * upper)/all;
				// low = low + (range * lower)/all;


			    unsigned long long range = high - low + 1;
			    high = low + (range * upper / all) - 1;
			    low = low + (range * lower / all);
			    //
			    // On each pass there are six possible configurations of high/low,
			    // each of which has its own set of actions. When high or low
			    // is converging, we output their MSB and upshift high and low.
			    // When they are in a near-convergent state, we upshift over the
			    // next-to-MSB, increment the pending count, leave the MSB intact,
			    // and don't output anything. If we are not converging, we do
			    // no shifting and no output.
			    // high: 0xxx, low anything : converging (output 0)
			    // low: 1xxx, high anything : converging (output 1)
			    // high: 10xxx, low: 01xxx : near converging
			    // high: 11xxx, low: 01xxx : not converging
			    // high: 11xxx, low: 00xxx : not converging
			    // high: 10xxx, low: 00xxx : not converging
			    //
			    for ( ; ; ) {
			      if ( high < 0x80000000U )
			        put_bit_plus_pending(0, pending_bits, res);
			      else if ( low >= 0x80000000U )
			        put_bit_plus_pending(1, pending_bits, res);
			      else if ( low >= 0x40000000 && high <  0xC0000000U ) {
			        pending_bits++;
			        low -= 0x40000000;  
			        high -= 0x40000000;  
			      } else
			        break;
			      high <<= 1;
			      high++;
			      low <<= 1;
			      high &= 0xFFFFFFFFU;
			      low &= 0xFFFFFFFFU;
			    }
			    // if ( c == 256 ) //256 is the special EOF code
			    //   break;
			  }
			  pending_bits++;
			  if ( low < 0x40000000 )
			    put_bit_plus_pending(0, pending_bits, res);
			  else
			    put_bit_plus_pending(1, pending_bits, res);

			result = res;


		  // return 0;
		}

		static inline void put_bit_plus_pending(bool bit, int &pending_bits, vector<bool> &res)
		{
		  // m_output.put_bit(bit);
			res.push_back(bit);
		  for ( int i = 0 ; i < pending_bits ; i++ ){
		    // m_output.put_bit(!bit);
		  	res.push_back(bit);
		  }
		  pending_bits = 0;
		}


		static void output_bit_plus_pending(bool bit, int &pending_bits, vector<bool> &res)
		{
		  // output_bit( bit );
			res.push_back(bit);
		  	while ( pending_bits ){
		  		// cout<<"add\n";
		  		res.push_back(!bit);
		  		pending_bits--;
		  	}
		    // output_bit( !bit );
		}
		// static map<int, int> translation(map<int, pair<int, int>> grammar)




};


#endif