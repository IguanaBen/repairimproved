#include <iostream>
#include <fstream>
#include <cmath>
#include "suffix_tree.h"
#include "slp1.h"
#include "slp2.h"
#include "slp2_v2.h"
#include "slp2_v3.h"
// #include "compress.h"
// #include "huffman.h"
#include "huffman_compression/huffman_wrapper.h"
#include "grammar_binary_compressor.h"
#include "grammar_seq_compressor.h"

#define CHECK 0

using namespace std;

namespace compress_benchmark {

int compress_chunk_slp1(vector<int> &data, ofstream &out_file);
int compress_chunk_slp2(vector<int> &data, ofstream &out_file);
void compress_chunk_slp1_bench(vector<int> &data, float coeff,
	int &lz77_size, int &grammar_size, int &grammar_size2, int &remaining_length);

void compress_chunk_slp2_v3_bench(vector<int> &data, float coeff, int method, int crossing_type,
	int &lz77_size, int &grammar_size, int &grammar_size2, int &remaining_length,
	int &binary_seq_length, int &binary_grammar_length);

void compress_bench(string in_filename, ofstream &debug_file,
	int chunk_size, float coeff, int mehtod, int crossing_type,
	int &lz77_size, int &grammar_size, int &grammar_size2, int &remaining_length,
	int &binary_seq_length, int &binary_grammar_length);

int benchmark(string filname, string out_filename, string debug_filename,
	int chunk_size, float coeff_min, float step, int num_steps, int method, int crossing_type, bool do_repair) {

	// float step = (coeff_max-coeff_min)/(float(num_steps));
	// cout<<"RePair: "<<compress_bench(filname, chunk_size, 0.0f)<<"\n";
	// auto repair_result = compress_bench(filname, chunk_size, 0.0f);
	// cout<<"RePair: " << repair_result.first << " " << repair_result.second.first << " " << repair_result.second.second<<"\n";

	ifstream fl(filname);
    fl.seekg( 0, ios::end );  
    size_t len = fl.tellg();
    fl.close();

	int lz77_s = 0;

	ofstream out_file(out_filename);
	ofstream debug_file(debug_filename);
	
	int lz77_size_repair = 0;
	int sum_of_grammars_repair = 0;
	int sum_of_grammars2_repair = 0;
	int rem_seq_len_repair = 0;
	int binary_seq_length_repair = 0;
	int binary_grammar_length_repair = 0;

	float multiplier = 100.0f;

	// out_file.precision(3);
	// int precision = 3;
	// cout << fixed << setprecision(2) << total;

	if(do_repair) {
		compress_bench(filname, debug_file, chunk_size, 0.0f, 0, 0,
			lz77_size_repair, sum_of_grammars_repair, sum_of_grammars2_repair,
			rem_seq_len_repair, binary_seq_length_repair, binary_grammar_length_repair);

		out_file
							<< "RePair" << " & " << sum_of_grammars_repair << " & 0" << " & "
					        << sum_of_grammars2_repair << " & " << rem_seq_len_repair << " & "
					        << sum_of_grammars2_repair*2 + rem_seq_len_repair <<" & " << " 0" <<" & "
							<< binary_grammar_length_repair<< " & 0 & "
							<< binary_seq_length_repair<< " & 0 & "
							<< binary_grammar_length_repair + binary_seq_length_repair << " & 0 & "
							<< float(binary_grammar_length_repair*8)/float(len) << " & "
							<< float(binary_seq_length_repair*8)/float(len) << " & "
							<< float((binary_grammar_length_repair + binary_seq_length_repair)*8)/float(len) <<
							" \\\\"<< endl;
	}

	lz77_s = lz77_size_repair;

	float mx_percent_sum_of_grammars = -100.0f;
	float mx_percent_sum_2 = -100.0f;
	float mx_percent_bgrammarlen = -100.0f;
	float mx_percent_bseqlen = -100.0f;
	float mx_percent_blen = -100.0f;

	float bestbpsgrammar = 100.0f;
	float bestbpsseq = 100.0f;
	float bestbpstotal = 100.0f;

	float ccoeff = coeff_min;
	for(int i=0; i<num_steps; ++i) {

		int lz77_size = 0;
		int sum_of_grammars = 0;
	    int sum_of_grammars2 = 0;
	    int rem_seq_len = 0;
	    int binary_seq_length = 0;
	    int binary_grammar_length = 0;
		compress_bench(filname, debug_file, chunk_size, ccoeff, method, crossing_type,
			lz77_size, sum_of_grammars, sum_of_grammars2,
			rem_seq_len, binary_seq_length, binary_grammar_length);

		// float diff_sum = float(sum_of_grammars);
		// cout<<"Coeff: "<<ccoeff<<" "<<compress_bench(filname, chunk_size, ccoeff)<<"\n";
		// out_file<< ccoeff << " " << sum_of_grammars << " " << sum_of_grammars2
		// 		<< " " << rem_seq_len << " " << binary_seq_length<< " " << binary_grammar_length<< endl;
				 // << " " << lz77_size <<endl;
		int sum2_repair = sum_of_grammars2_repair*2 + rem_seq_len_repair;
		int sum2 = sum_of_grammars2*2 + rem_seq_len;
		int total_binary_len_repair = binary_grammar_length_repair + binary_seq_length_repair; 
		int total_binary_len = binary_grammar_length + binary_seq_length;
		
		out_file
							<< ccoeff << " & " << sum_of_grammars <<" & "<< float(sum_of_grammars_repair-sum_of_grammars)/float(sum_of_grammars_repair)*multiplier <<" & " 
					        << sum_of_grammars2  << " & " << rem_seq_len << " & "
					        << sum_of_grammars2*2 + rem_seq_len <<" & " << float(sum2_repair - sum2)/float(sum2_repair)*multiplier <<" & "
							<< binary_grammar_length << " & " <<   float(binary_grammar_length_repair-binary_grammar_length)/float(binary_grammar_length_repair)*multiplier<< " & "
							<< binary_seq_length << " & " <<   float(binary_seq_length_repair-binary_seq_length)/float(binary_seq_length_repair)*multiplier << " & "
							<< total_binary_len << " & " << float( total_binary_len_repair- total_binary_len)/float(total_binary_len_repair)*multiplier << " & "
							<< float(binary_grammar_length*8)/float(len) << " & "
							<< float(binary_seq_length*8)/float(len) << " & "
							<< float(total_binary_len*8)/float(len) <<
							" \\\\"<< endl;

		mx_percent_sum_of_grammars = max(mx_percent_sum_of_grammars, float(sum_of_grammars_repair-sum_of_grammars)/float(sum_of_grammars_repair)*multiplier );
		mx_percent_sum_2 = max(mx_percent_sum_2, float(sum2_repair - sum2)/float(sum2_repair)*multiplier  );
		mx_percent_bgrammarlen = max(mx_percent_bgrammarlen, float(binary_grammar_length_repair-binary_grammar_length)/float(binary_grammar_length_repair)*multiplier);
		mx_percent_bseqlen = max(mx_percent_bseqlen, float(binary_seq_length_repair-binary_seq_length)/float(binary_seq_length_repair)*multiplier);
		mx_percent_blen = max(mx_percent_blen, float( total_binary_len_repair- total_binary_len)/float(total_binary_len_repair)*multiplier);

		bestbpsgrammar = min(bestbpsgrammar, float(binary_grammar_length*8)/float(len));
		bestbpsseq = min(bestbpsseq, float(binary_seq_length*8)/float(len));
		bestbpstotal = min(bestbpstotal, float(total_binary_len*8)/float(len) );


		ccoeff += step;
		// lz77_s = lz77_size;
	}


	out_file
							<< "Best:" << " & " << "-" <<" & "<< mx_percent_sum_of_grammars <<" & " 
					        << "-"  << " & " << "-" << " & "
					        << "-" <<" & " << mx_percent_sum_2 <<" & "
							<< "-" << " & " << mx_percent_bgrammarlen<< " & "
							<< "-" << " & " <<   mx_percent_bseqlen << " & "
							<< "-" << " & " << mx_percent_blen << " & "
							<< bestbpsgrammar << " & "
							<< bestbpsseq << " & "
							<< bestbpstotal <<
							" \\\\"<< endl;

	out_file<<"LZ77: "<<lz77_s<<"\n";
	out_file<<"Filesize: "<<len<<"\n";
	out_file.close();
	debug_file.close();
}

void compress_bench(string in_filename, ofstream &debug_file, int chunk_size, float coeff, int method, int crossing_type,
	int &lz77_size, int &grammar_size, int &grammar_size2, int &remaining_length,
	int &binary_seq_length, int &binary_grammar_length) {
    
    ifstream fl(in_filename);
	// ofstream out_file(out_filename, ios::out | ios::binary);

    fl.seekg( 0, ios::end );  

    size_t len = fl.tellg();
    char *ret = new char[chunk_size];  
    fl.seekg(0, ios::beg);
    // cout<<len<<"\n";

    int read = 0;

    int k = 0;

    int chunk_count = 0;

    int cut = 100000; // wsadz to do argumentu

    do {

     int to_read = min(chunk_size, int(len-read));
     read += to_read;

     vector<int> vec;
     fl.read(ret, to_read);
     // cout<<ret[0]<<" "<<ret[1]<<" "<<ret[2]<<" "<<ret[3]<<" "<<ret[4]<<"\n";
     // for(int i=0; i<40; ++i)cout<<ret[i];
     	// cout<<"\n";
     for(int i=0; i<to_read; ++i) {
     	unsigned char temp = ret[i];
    	vec.push_back(temp);

    	// cout<<int(ret[i])<<" ret\n";
    	// if(vec[i] < 0) {
    	// 	unsigned char dd = ret[i];
    	// 	cout<<vec[i]<<" "<<ret[i]<<" "<<int(dd)<<" ret\n";
    	// }

    }
    // cout<<"\n";

     // if(method == 0)
     	// compress_chunk_slp1(vec, out_file);
     // else if(method == 1)
     	// compress_chunk_slp2(vec, out_file);
     // else

    	// TODO: daj to do argumentu

    	// compress_chunk_slp1_bench(vec, coeff, lz77_size, grammar_size, grammar_size2, remaining_length);
    	compress_chunk_slp2_v3_bench(vec, coeff, method, crossing_type,
    		lz77_size, grammar_size, grammar_size2,
    		remaining_length, binary_seq_length, binary_grammar_length);
    	// sum_of_grammars += res_p.first + res_p.second - 1;
    	// sum_of_grammars2 += res_p.first;
    	// rem_seq_len += res_p.second;

     	// sum_of_grammars +=
     	// cerr << "Completed: " << coeff <<"/"<< chunk_count++ << endl;
     	debug_file << "Completed: " << coeff <<"/"<< chunk_count++ << endl;
     // cout<<"Read: "<<read<<"\n"; 
     	//break;
	} while(read != len);
    delete[] ret;
    fl.close();

    // return sum_of_grammars;

    //return make_pair(sum_of_grammars, make_pair(sum_of_grammars2, rem_seq_len));

    // cout<<vec.size()<<"\n";
    // return 0;
}

int compress_chunk_slp1(vector<int> &data, ofstream &out_file) {
	slp1 slp1_compress;
	slp1_compress.compress(data, 0);
	// slp1_compress.compress2(data);
	vector<unsigned int> compressed_grammar_uint = slp1_compress.get_grammar();
	// vector<unsigned int> compressed_grammar_uint;
	// cout<<compressed_grammar.size()<<"\n";
	// compressed_grammar_uint.resize(compressed_grammar.size());
	// for(int i=0; i<compressed_grammar.size(); ++i) {
	// 	compressed_grammar_uint[i] = compressed_grammar[i];
	// 	// compressed_grammar_uint.push_back(compressed_grammar[i]);
	// }
	// cout<<"kk\n";
	map<unsigned int, string> dict;
	vector<bool> out;
	huffman_encode(compressed_grammar_uint, dict, out);

	// cout<<"Dict:\n";
	// for(auto it = dict.begin(); it != dict.end(); ++it) {
	// 	cout<<it->first<<" "<<it->second<<"\n";
	// }
	// cout<<"\n";
	cout<<compressed_grammar_uint.size()<<" "<<out.size()/8<<"\n";
}

int compress_chunk_slp2(vector<int> &data, ofstream &out_file) {
	slp2 slp2_compress;
	slp2_compress.compress(data);
	// slp1_compress.compress2(data);
	vector<unsigned int> compressed_grammar_uint = slp2_compress.get_grammar();
	// slp2_compress.get_compressed_grammar();
	// vector<unsigned int> compressed_grammar_uint;
	// cout<<compressed_grammar.size()<<"\n";
	// compressed_grammar_uint.resize(compressed_grammar.size());
	// for(int i=0; i<compressed_grammar.size(); ++i) {
	// 	compressed_grammar_uint[i] = compressed_grammar[i];
	// 	// compressed_grammar_uint.push_back(compressed_grammar[i]);
	// }
	// cout<<"kk\n";
	map<unsigned int, string> dict;
	vector<bool> out;
	huffman_encode(compressed_grammar_uint, dict, out);

	// cout<<"Dict:\n";
	// for(auto it = dict.begin(); it != dict.end(); ++it) {
	// 	cout<<it->first<<" "<<it->second<<"\n";
	// }
	// cout<<"\n";
	cout<<compressed_grammar_uint.size()<<" "<<out.size()/8<<"\n";
	exit(0);
}


void compress_chunk_slp1_bench(vector<int> &data, float coeff, int &lz77_size, int &grammar_size, int &grammar_size2, int &remaining_length) {
	// slpv3::slp2_v3 slp_compress(data, coeff);
	slp1 sl1, sl2;
	sl1.compress(data, 0);
	sl2.compress(data, 1);
	// slpv3::slp2_v3 slp_compress(data, coeff);

	lz77_size += sl1.get_lz77_size();
	grammar_size += sl1.get_grammar_size();
	grammar_size2 += sl2.get_grammar_size();
	remaining_length += sl2.get_remaining_length();

	if(1) {
		vector<int> decompressed_data = sl1.decompress();
		vector<int> decompressed_data2 = sl2.decompress();

		// cout<<data.size()<<" "<<decompressed_data.size()<<" |\n";

		if(data == decompressed_data && data == decompressed_data2){
			cout<<"Chunk: OK\n";
		} else {
			cout<<"Chunk: FAIL\n";

			for(int i=0; i<data.size(); ++i)
				cout<<data[i]<<" ";
			cout<<"\n";

			exit(0);
		// 	// while(1){}
		}
		// exit(0);
	}
	// cout<<"Chunk compleet\n";
	// lz77_size += slp_compress.get_lz77_size();
	// return make_pair(slp_compress.get_grammar_size(), slp_compress.get_remaining_length());
	// slp_compress.compress(data);
}


void compress_chunk_slp2_v3_bench(vector<int> &data, float coeff, int method, int crossing_type,
		int &lz77_size, int &grammar_size,int &grammar_size2, int &remaining_length,
		int &binary_seq_length, int &binary_grammar_length) {



	slpv3::slp2_v3 slp_compress(data, coeff, method, crossing_type, data.size()*5);

	auto seq = slp_compress.get_remaining_sequence();


	if(0) {
		vector<int> decompressed_data = slp_compress.decompress();
		// cout<<data.size()<<" "<<decompressed_data.size()<<" |\n";

		if(data == decompressed_data){
			cout<<"Chunk: OK\n";
		} else {
			cout<<"Chunk: FAIL\n";

		// 	for(int i=0; i<data.size(); ++i)
		// 		cout<<data[i]<<" ";
		// 	cout<<"\n";

			exit(0);
		// 	// while(1){}
		}
	}
	// cout<<"Chunk compleet\n";
	lz77_size += slp_compress.get_lz77_size();
	grammar_size += slp_compress.get_grammar_size() + slp_compress.get_remaining_length() -1;
	grammar_size2 += slp_compress.get_grammar_size();
	remaining_length += slp_compress.get_remaining_length();

	// if(grammar_seq_compressor::decompress(grammar, seq) == data){
	// 	cout<<"OKI\n";
	// }

	// cout<<"This\n";
	/*
	map<int, int> freqs;
	for(int i=0; i < seq.size(); ++i)
		freqs[seq[i]]++;
	int dict_size = 0;
	int zeros = 0;

	int last_nonzero = -1;
	for(int i=0; i<=max_nonterm; ++i) {
		if(freqs[i] == 0) zeros++ ;
		// dict_size += 2*ceil(log2(freqs[i] + 1)) + 1;
		int x = freqs[i] + 1;
		// dict_size += floor(log2(x)) + 2*floor(log2(floor(log2(x)) + 1 )) + 1;
		// if(freqs[i]){
		// 	int diff = i-last_nonzero;
		// 	last_nonzero = i;
		// 	dict_size += 2*int(log2(diff)) + 1;
		// 	dict_size += 2*int(log2(x-1)) + 1;
		// }
		dict_size += 2*int(log2(x)) + 1;
	}
	cout<<"zeroes: "<<zeros<<" \n";
	// cout<<ceil(log2(1)) + 1<<"\n";
	// cout<<max_nonterm<<" | "<<dict_size<<" : "<<zeros<<"\n";
	

	vector<bool> out;
	map<unsigned int, string> dict;
	// cout<<dict[32332].length()<< " Ddddddd\n"; exit(0);

	// int dict_size = 0;
	huffman_wrapper::huffman(seq, out, dict);
	// for(int i=0; i<=max_nonterm; ++i) {
	// 	// if(freqs[i] == 0) zeros++ ;
	// 	dict_size += 2*ceil(log2(dict[i].length() + 1)) + 1;
	// }

	// for(int i=0; i<)
	binary_seq_length += (out.size()/8 + dict_size/8);

	*/

	int max_nonterm = slp_compress.get_max_nonterm();
	// cout<<max_nonterm<<" max_nonterm\n";
	auto grammar = slp_compress.get_grammar();
	// for(auto it = grammar.begin(); it != grammar.end(); ++it){
	// 	if(it->second.first < 0 || it->second.second < 0)
	// 	cout<<it->second.first<<" "<<it->second.second<<"\n"; 
	// }

	// if(grammar_seq_compressor::decompress(grammar, seq) == data){
	// 	cout<<"OKI\n";
	// }
	// for(int i=0; i<seq.size(); ++i)
	// 	cout<<seq[i]<<" ";
	// cout<<"\n";
	auto binary_grammar = grammar_binary_compressor::compress_grammar(grammar, seq);
	// cout<<"compressed\n";
	// for(auto it = grammar.begin(); it != grammar.end(); ++it) {
	// 	cout<<"G: "<<it->first<<" "<<it->second.first<<"|"<<it->second.second<<"\n";
	// }
	// for(int i=0; i<seq.size(); ++i)
	// 	cout<<seq[i]<<" ";
	// cout<<"\n";

	vector<bool> out;
	huffman_wrapper::huffman_with_dict(seq, max_nonterm, out);

	//vector<bool> out_temp;
	//huffman_wrapper::huffman_with_dict(seq, max_nonterm, out_temp);

	binary_seq_length += out.size()/8;

	// for(auto it = dec_grammar.begin(); it != dec_grammar.end(); ++it) {
	// 	cout<<"DG: "<<it->first<<" "<<it->second.first<<"|"<<it->second.second<<"\n";
	// }


	// vector<bool> binary_grammar_arithmethic;
	// int p1=0, p2=0;
	// grammar_binary_compressor::compress_arithmethic(binary_grammar, binary_grammar_arithmethic, p1, p2);
	// cout<<binary_grammar.size()/8<<" "<<binary_grammar_arithmethic.size()/8<<" <cc \n";

	// vector<int> huffman_grammar_txt; 
	// map<int, int> context_freqs;
	// int cl = 5;
	// for(int i=0; i<binary_grammar.size()-cl; i+=cl){
	// 	int p2 = 1;
	// 	int nmbr =0 ;
	// 	for(int j=0; j<cl; ++j){
	// 		nmbr += p2*(binary_grammar[i+j]);
	// 		p2 *= 2;
	// 	}

	// 	huffman_grammar_txt.push_back(nmbr);
	// 	context_freqs[nmbr]++;

	// }
	// int mm = 1;
	// for(int i=0; i<cl; ++i){
	// 	mm*=2;
	// }
	// mm--;
	// cout<<mm<<"\n";
	// vector<bool> huffman_grammar_txt_out;
	// huffman_wrapper::huffman_with_dict(huffman_grammar_txt, mm, huffman_grammar_txt_out);

	// cout<<binary_grammar.size() << " __ "<<huffman_grammar_txt_out.size()<<"\n";
	// cout<<"\n";
	// for(auto it = context_freqs.begin(); it != context_freqs.end(); ++it) {
	// 	// cout<<" > "<<it->first<<" "<<it->second<<"\n";
	// }
	// cout<<"\n";

	// auto ohnh = slp_compress.get_remaining_sequence();


	if(CHECK){

	auto dec_grammar = grammar_binary_compressor::decompress(binary_grammar);
	vector<int> new_sequence = huffman_wrapper::huffman_decode(max_nonterm, out);

	cout<<grammar.size()<<" | "<<dec_grammar.size()<<"\n";
	cout<<seq.size()<<" | "<<new_sequence.size()<<"\n";


	// if(grammar_seq_compressor::decompress(dec_grammar, seq) == data)
	// 	cout<<"okk\n";
	// else {
	// 	cout<<"FFF\n";
	// }

	// if(new_sequence == seq){
	// 	cout<<"somewhat ok\n";
	// }

	if(grammar_seq_compressor::decompress(dec_grammar, new_sequence) == data)
		cout<<"okk2\n\n";
	else{

		// auto dec_seq = grammar_seq_compressor::decompress(dec_grammar, new_sequence);
		// cout<<dec_seq.size()<<" "<<data.size()<<"\n";
		// for(int i=0; i<min(dec_seq.size(), data.size()); ++i){
		// 	// if(dec_seq[i] != data[i]){
		// 		cout<<dec_seq[i]<<" "<<data[i]<<"\n";
		// 		// break;
		// 	// }
		// }

		cout<<"FFF2\n";
		
		exit(1);
	}
	}

	// auto cmp_ref = grammar_seq_compressor::decompress(dec_grammar, seq);
	// // if(grammar.size() ==  dec_grammar.size())
	// if(grammar_seq_compressor::decompress(grammar, ohnh)
	// 	==
	// 	grammar_seq_compressor::decompress(dec_grammar, seq))
	// 	cout<<"OKI\n";
	// else
	// 	cout<<"FAIL:(\n";

	binary_grammar_length += binary_grammar.size()/8;
	// return make_pair(slp_compress.get_grammar_size(), slp_compress.get_remaining_length());
	// slp_compress.compress(data);
}

}