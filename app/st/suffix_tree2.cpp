#include "suffix_tree2.h"

#include <iostream>

suffix_tree::suffix_tree(vector<int> &_text)
{
	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;

	for(unsigned int i=0; i<text.size(); ++i)
	{
		// cout<<"Adding: "<<i<<" "<<char(text[i])<<"\n";
		bool cont = true;
		do
		{	
			if(active_len == 0)
			{
				if(active_node->children[text[i]] == 0)
				{
					suffix_tree_node* new_leaf = new suffix_tree_node;
					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;
					active_node->children[text[i]] = new_leaf;

					active_char = text[i];

					if(last_added != NULL)
						last_added->slink = active_node;

					if(active_node == root)
						cont = false;
					else
						active_node = active_node->slink;

					last_added = NULL;
				}
				else
				{
					active_len++;

					if(last_added != NULL)
						last_added->slink = active_node;

					active_char = text[i];

					if(active_node->children[text[i]]->edge_length == 1)
					{
						active_node = active_node->children[text[i]];
						active_len = 0;
					}

					last_added = NULL;
					cont = false;
				}
			}
			else
			{
				unsigned int index = active_node->children[active_char]->text_ind + active_len;
				if( text[i] == text[index])
				{
					active_len++;

					if(active_node->children[active_char]->edge_length == active_len)
					{
						active_node = active_node->children[active_char];
						active_len = 0;
						active_char = text[i];

						if(last_added != NULL)
							last_added->slink = active_node;	//innego przypadku nie moze byc gdy text[i] == text[index]
					}

					last_added = NULL;
					cont = false;
				}
				else
				{

					suffix_tree_node* child = active_node->children[active_char];
					// cout<<active_len<<" "<<active_node->children.size()<<" "<<child<<"|\n";
					suffix_tree_node* new_leaf = new suffix_tree_node;
					suffix_tree_node* new_node = new suffix_tree_node;

					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;

					new_node->text_ind = child->text_ind;
					new_node->edge_length = active_len;

					child->text_ind = child->text_ind + active_len;
					if(child->edge_length != inf)
						child->edge_length -= active_len;

					active_node->children[active_char] = new_node;

					new_node->children[text[i]] =  new_leaf;
					new_node->children[text[index]] = child;

					if(last_added != NULL)
						last_added->slink = new_node;


					last_added = new_node;

					unsigned int t_index;
					if(active_node == root)
					{
						if(active_len == 1)
						{
							new_node->slink = root;
							last_added = NULL;
						}

						active_len--;

						active_char = text[ new_node->text_ind + 1];
						t_index = new_node->text_ind + 1;
					}
					else
					{
						last_added = new_node;
						t_index = active_node->children[active_char]->text_ind;
					}

					active_node = active_node->slink;
					// cout<<active_len<<" "<<active_char<<" "<<active_node->children[active_char]<<"\n";
					// cout<<"a\n";
					while(active_len > 0 && active_node->children[active_char]->edge_length <= active_len)
					{
						// cout<<active_len<<" "<<active_node->children[active_char]->edge_length<<"\n";
						// cout<<"changing\n";
						unsigned int e_length = active_node->children[active_char]->edge_length;
						active_len -= e_length;
						active_node = active_node->children[active_char];
						// cout<<active_node->children.size()<<" "<<active_char<<"\n";

						// map<int, suffix_tree_node*>::iterator it;
						// for(it = active_node->children.begin(); it != active_node->children.end(); ++it)
						// 	cout<<it->first<<" ";
						// cout<<"\n";

						// cout<<"b\n";
						// cout<<char(active_char)<<"\n";

						t_index += e_length;
						if(active_len != 0)
							active_char = text[t_index];
						// cout<<char(active_char)<<"\n";
						// cout<<"e\n";
					}
					// cout<<"Overdiri\n";
					
					// cout<<active_len<<"\n";

					// if(active_node == )

				}
			}

		}while(cont);
	}
}

vector<unsigned int> suffix_tree::make_array()
{
	vector<unsigned int> result;
	result.resize(text.size());

	unsigned int cnt = 0;
	traverse(root, result, cnt, 0);

	return result;
}

void suffix_tree::del_tree(suffix_tree_node* node)
{
	map<int, suffix_tree_node*>::iterator it;
	for(it = node->children.begin(); it != node->children.end(); ++it)
		del_tree(it->second);

	delete node;
}

suffix_tree::~suffix_tree()
{
	del_tree(root);
}

void suffix_tree::traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth)
{
	if(node->children.size() == 0)
		vec[cnt++] = text.size() - depth;
	else
	{
		  map<int, suffix_tree_node*>::iterator it;
		  for(it = node->children.begin(); it != node->children.end(); ++it)
		  {
		  	if(it->second->edge_length != inf)
		  	{
				// cout<<"depth: "<<depth
				// 	<<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + it->second->edge_length);
		  	}
		  	else
		  	{
				// cout<<"depth: "<<depth
					// <<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + (text.size() - it->second->text_ind));
		  	}
		  }
	}

	// cout<<"UP\n";
}